// ==UserScript==
// @name        PlayerSaver2.0
// @author      Kraxnor
// @namespace   kraxnor
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasTextRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasAxisLabelRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.pointLabels.min.js
// @include     http://trophymanager.com/*
// @include     https://trophymanager.com/*
// @exclude     http://trophymanager.com/players
// @exclude     https://trophymanager.com/players
// @version     4.1
// @grant        none
// ==/UserScript==

//Avoid conflicts
this.$ = this.jQuery = jQuery.noConflict(true);

(function() {

$(document).ready(function() {
    /*
    * GLOBALS
    *
    */
    var debug = true;
    var logLevel = 1;
    function log(msg,level) {
        if (level === null || level === undefined)
            level = 1;
        if (debug && level <= logLevel) {
            if (typeof msg === 'object') {
                console.log(msg);
            } else {
                if (level === 0)
                    console.log("["+level+"]!!!! " + msg);
                else
                    console.log("["+level+"]#### " + msg);
            }
        }
    }




    var availableGraphTypes = ['r2','rr','sk','r2+sk',];
    var rversion = 4;
    var skillNames = function() {
        if (rversion == 4) {
            return ["strength","passing","stamina","crossing","pace","technique","marking","heading","tackling","finishing","workrate","longshots","positioning","set pieces","rr_dc","rr_dlr","rr_dmc","rr_dmlr","rr_mc","rr_mlr","rr_omc","rr_omlr","rr_fc","r2_dc","r2_dlr","r2_dmc","r2_dmlr","r2_mc","r2_mlr","r2_omc","r2_omlr","r2_fc"];
        }
        return ["strength","passing","stamina","crossing","pace","technique","marking","heading","tackling","finishing","workrate","longshots","positioning","set pieces","sk1","sk2","rr_dc","rr_dlr","rr_dmc","rr_dmlr","rr_mc","rr_mlr","rr_omc","rr_omlr","rr_fc","r2_dc","r2_dlr","r2_dmc","r2_dmlr","r2_mc","r2_mlr","r2_omc","r2_omlr","r2_fc"];
    }
    var skillNamesGoalKeeper = ["strength","handling","stamina","one on ones","pace","reflexes","","aerial ability","","jumping","","communication","","kicking","","throwing","sk1","sk2","rr_dc","rr_dlr","rr_dmc","rr_dmlr","rr_mc","rr_mlr","rr_omc","rr_omlr","rr_fc","r2_dc","r2_dlr","r2_dmc","r2_dmlr","r2_mc","r2_mlr","r2_omc","r2_omlr","r2_fc"];
    var infoNames = ["","club","age","height/weight","wage","recommandation","skill index", "status", "routine"];
    var positionNames = ["D C", "D L", "D R", "DM C", "DM L", "DM R", "M C", "M L", "M R", "OM C", "OM L", "OM R", "F", "GK"];
    var scoutNames = ["scout","date","age","potential","bloom status","development status","speciality","should develop"];

    /*
    * TM
    *
    */
    function playersaver() {
        var self = this;
        self.api = new tmapi();
        self.seller = null;
        self.player = null;
        self.tactics = null;

        self.init = function() {
            log("####### INIT PlayerSaver2.0 #######");
            log(SESSION,2);
            self.api.init(function(authenticated) {
                if (authenticated) {
                    self.seller = new playerseller(self.api);
                    self.tactics = new tmtactics(self.api);
                    self.route();
                }
            });
        };

        self.route = function() {
            if (location.href.indexOf("/players/") != -1) {
                var playerTable = document.getElementsByClassName("skill_table zebra")[0];
                var infoTable = document.getElementsByClassName("float_left info_table zebra")[0];
                tmutils.delay(function() {
                    self.player = new tmplayer(self.api);
                    self.player.init(playerTable, infoTable);
                    self.player.loadPlayerData();

                    self.seller.init(self.player);
                    self.seller.loaded(function() {
                        self.seller.initSellerDongle();
                    });
                    self.seller.loadPlayerMenu();
                },200);

                tmutils.addCss(".playerSaverGraphMenu ul {list-style:none} .playerSaverGraphMenu ul li {float:left;padding:5px;}");
            }
            if (location.href.indexOf("/tactics/") != -1) {
                tmutils.delay(function() {
                    self.tactics.init();
                    tmutils.initHoverListener(null,self.api);
                },300);
            }

            if (location.href.indexOf("/transfer/") != -1) {
                tmutils.delay(function() {
                    tmutils.waitForTransferlist(function() {
                        tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                        $( window ).on("hashchange",function() {
                            tmutils.waitForTransferlist(function() {
                                tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                            });
                        });
                    });
                },1000);
            }

            if (location.href.indexOf("/matches/") != -1) {
                tmutils.waitForMatch(function() {
                     tmutils.delay(function() {
                         tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                     },200);
                });
            }
            if (location.href.indexOf("/club") != -1) {
                self.seller.init();
                self.seller.loaded(function() {
                    self.seller.initSellerDongle();
                    tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                });
            }
            if (location.href.indexOf("/home") != -1) {
                self.seller.init();
                self.seller.loaded(function() {
                    self.seller.initSellerDongle();
                    tmutils.waitForTabContent(function() {
                        tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                        $(".tabs_new > div").click(function() {
                            tmutils.waitForTabContent(function() {
                                tmutils.initHoverListener('a[player_link],span[player_link],a.no_hover,.player_field > div.pog',self.api);
                            });
                        });
                    });
                });
            }
        };
    }

    function tmcache() {
        var self = this;
        var sday = 24 * 60 * 60;
        self.expires = {
            "lastRecord": 300,
            "currentRecord": 300,
            "tactics": 300
        };
        self.getCacheDate = function(cacheName) {
            var dname = cacheName+".date";
            var timestamp = window.localStorage.getItem(dname);
            if (timestamp !== null && timestamp !== undefined)
                return timestamp;
            else
                return 0;
        };
        self.setCacheDate = function(cacheName, date) {
            var dname = cacheName+".date";
            var unix = Math.round(+date/1000);
            window.localStorage.setItem(dname,unix);
        };
        self.getCache = function(cacheName, cache) {
            //check expire
            var empty = {};
            var cacheSlug = cacheName.split(".")[1];
            var cacheExpire = self.expires[cacheSlug];
            var cacheDate = self.getCacheDate(cacheName);
            var now = new Date();
            var nowUnix = Math.round(now/1000);

            if (cacheDate !== null && Math.abs(nowUnix-cacheDate) > cacheExpire) {
                log("clearing expired cache: " + cacheName,1);
                window.localStorage.setItem(cacheName,JSON.stringify(empty));
                self.setCacheDate(cacheName,now);
                return empty;
            }

            log("cache: "+ cacheName+" expires in " + (cacheExpire-Math.abs(nowUnix-cacheDate)) ,2);

            if (cache !== null && cache !== undefined) {
                window.localStorage.setItem(cacheName,JSON.stringify(cache));
                return;
            }

            var item = window.localStorage.getItem(cacheName);
            if (item !== null && item !== undefined && item != "undefined") {
                return JSON.parse(item);
            } else {
                window.localStorage.setItem(cacheName,JSON.stringify(empty));
                self.setCacheDate(cacheName,now);
                return empty;
            }
        };

        self.lastRecordCache = function(cache) {
            return self.getCache("tmutils.lastRecord",cache);
        };
        self.recordCurrentCache = function(cache) {
            return self.getCache("tmutils.currentRecord",cache);
        };
        self.tacticsCache = function(cache) {
            return self.getCache("tmutils.tactics",cache);
        };

        self.addLastRecord = function(playerId, record) {
            var c = self.lastRecordCache();
            c[playerId] = record;
            self.lastRecordCache(c);
            return true;
        };
        self.addCurrentRecord = function(playerId, record) {
            var c = self.recordCurrentCache();
            c[playerId] = record;
            self.recordCurrentCache(c);
            return true;
        };
        self.setTactics = function(tactics) {
            self.tacticsCache(tactics);
            return true;
        };

        self.getPlayerLastRecord = function(api, playerId, callback) {
            var lplayer = self.lastRecordCache()[playerId];
            if (lplayer !== null && lplayer !== undefined &&
                lplayer.playerId !== null && lplayer.playerId !== undefined) {
                if (callback)
                    callback(lplayer);
            } else {
                api.loadLastPlayerRecord(playerId, function(res) {
                    if (res.data.players === null || res.data.players=== undefined) {
                        if (callback)
                            callback(null);
                    } else {
                        self.addLastRecord(playerId,res.data.players[0]);
                        log("Loaded lastrecord for player " + playerId + " to cache",2);
                        if (callback)
                            callback(self.lastRecordCache()[playerId]);
                    }
                });
            }
        };

        self.getPlayerCurrentRecord = function(api, playerId, callback) {
            var cplayer = self.recordCurrentCache()[playerId];
            if (cplayer !== null && cplayer !== undefined &&
                cplayer.playerId !== null && cplayer.playerId !== undefined) {
                if (callback)
                    callback(cplayer);
            } else {
                api.loadCurrentPlayerRecord(playerId, function(res) {
                    if (res.data.players === null || res.data.players=== undefined) {
                        if (callback)
                            callback(null);
                    } else {
                        self.addCurrentRecord(playerId,res.data.players[0]);
                        log("Loaded currentrecord for player " + playerId + " to cache",2);
                        if (callback)
                            callback(self.recordCurrentCache()[playerId]);
                    }
                });
            }
        };

        self.getTactics = function(api, callback) {
            var ctactics = self.tacticsCache();
            //log("TACTICS CACHE",2);
            //log(ctactics,2);
            if (ctactics !== null && ctactics !== undefined &&
                ctactics.club !== null && ctactics.club !== undefined) {
                if (callback)
                    callback(ctactics);
            } else {
                api.getTMTactics(function(res) {
                    if (res === null || res === undefined) {
                        if (callback)
                            callback(null);
                    } else {
                        self.setTactics(res);
                        log("Loaded tactics to cache",2);
                        if (callback)
                            callback(res);
                    }
                });
            }
        };
    }

    var tmutils = {
        cache: new tmcache(),
        zeroPad: function(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        },
        formatDate: function(date) {
            var cDate = date.getDate();
            var cMonth = date.getMonth();
            var cYear = date.getFullYear();
            var cHour = date.getHours();
            var cMin = date.getMinutes();
            var cSec = date.getSeconds();
            return tmutils.zeroPad(cDate,2)+"."+tmutils.zeroPad(cMonth+1,2)+"."+cYear;
        },
        delay: function(fn, delay) {
            return window.setTimeout(fn,delay);
        },
        addCss: function(cssString) {
            var head = document.getElementsByTagName('head')[0];
            var newCss = document.createElement('style');
            newCss.type = "text/css";
            newCss.innerHTML = cssString;
            head.appendChild(newCss);
        },
        lastHoverPlayer: null,
        initHoverListener: function(selector, api) {
            if (selector === null || selector === undefined || selector === "")
                selector = '#tactics_list_list ul li span.player_name,.field_player, .bench_player';
            tmutils.delay(function() {
                log("init tm-hover listener",1);
                $(selector).on('hover mouseover',function() {
                    //log("hover start",2);
                    var playermId = $(this).attr("player_id");
                    if (playermId === null || playermId === undefined) {
                        playermId = $(this).attr("player_link");
                        if (playermId === null || playermId === undefined) {
                            var tmp = $(this).attr("href");
                            if (tmp.indexOf("/players/") !== -1) {
                                playermId = parseInt(tmp.replace("/players/",""));
                            }
                            if (playermId === null || playermId === undefined)
                                return;
                        }
                    }
                    if (playermId == tmutils.lastHoverPlayer)
                        return;
                    tmutils.lastHoverPlayer = playermId;
                    tmutils.waitForTooltip(function(tablet) {
                        $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row").remove();
                        $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row2").remove();
                        $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row3").remove();
                        if (tablet === null) {
                            log("Tooltip aborted",1);
                        } else {
                            log("Tooltip loaded",2);
                            var ttbody = $(tablet).find("tbody");
                            var rr2 = new playerrr(api).getDataTooltip($('#tooltip'));
                            var r2 = rr2.r2;
                            var r3 = rr2.r3;
                            if(r2.indexOf("/") > -1) {
                                var r2p = r2.split("/");
                                r2 = Math.max.apply(Math, r2p);                                
                            }
                            if(r3.indexOf("/") > -1) {
                                var r3p = r3.split("/");
                                r3 = Math.max.apply(Math, r3p);
                            }
                            r2 = parseFloat(r2).toFixed(3);
                            r3 = parseFloat(r3).toFixed(3);
                            var rr = Math.max.apply(Math, rr2.rerec2);
                            rr = parseFloat(rr).toFixed(3);
                            var app = '<tr id="playerSaver_rr_r2_row" style="color:gold;"><td colspan="1"></td><td class="align_left">RRec</td><td class="align_left"><b>'+rr+'</b></td></tr>';
                            app += '<tr id="playerSaver_rr_r2_row2" style="color:gold;"><td colspan="1"></td><td class="align_left">RatingR2</td><td class="align_left"><b>'+r2+'</b></td></tr>';
                            app += '<tr id="playerSaver_rr_r2_row3" style="color:gold;"><td colspan="1"></td><td class="align_left">RatingR3</td><td class="align_left"><b>'+r3+'</b></td></tr>';
                            $(ttbody).append(app);
                            tmutils.cache.getPlayerCurrentRecord(api, playermId,function(res) {
                                if (res === null) {
                                    log("no playerrecord found",2);
                                }
                                if (res != null && res.record.sellData !== null && res.record.sellData !== undefined) {
                                      if (tmutils.lastHoverPlayer == res.playerId) {
                                        var papp = '';
                                        var pot = res.record.sellData.potential;
                                        if (pot !== null && pot !== undefined && !$('#playerSaver_rr_r2_row3').length)
                                            papp += '<tr id="playerSaver_rr_r2_row3" style="color:white;"><td colspan="1"></td><td class="align_left">Pot</td><td class="align_left"><b>'+pot+'</b></td></tr>';
                                        $(ttbody).append(papp);
                                    }
                                    //log("loaded tooltip extension",2);
                                }
                            });
                        }
                    });
                });
            },100);
        },
        waitForTooltip: function(callback, timeout) {
            tmutils.delay(function() {
                var tablet = $("#tooltip .player_tooltip").find("table")[0];
                if (tablet != null) {
                    var ttbody = $(tablet).find("tbody");
                    if (ttbody !== null && $(ttbody).find("tr").length > 5) {
                        callback(tablet);
                    } else {
                        if (timeout != undefined && timeout > 0) {
                            var nt =  timeout-1;
                            tmutils.waitForTooltip(callback, nt);
                        } else if (timeout != undefined && timeout === 0)
                            callback(null);
                        else {
                            tmutils.waitForTooltip(callback, 10);
                        }
                    }
                } else {
                     if (timeout != undefined && timeout > 0) {
                         var nt =  timeout-1;
                         tmutils.waitForTooltip(callback, nt);
                     } else if (timeout != undefined && timeout === 0)
                         callback(null);
                    else {
                        tmutils.waitForTooltip(callback, 10);
                    }
                }
            },100);
        },
        waitForMatch: function(callback, timeout) {
            tmutils.delay(function() {
                var field = $('.player_list');
                if (field != null && field != undefined &&
                   $(field).children().length >= 16) {
                    log("match loaded, "+$(field).children().length+" players found",2);
                    callback();
                } else {
                    if (timeout != undefined && timeout > 0) {
                         var nt =  timeout-1;
                         tmutils.waitForMatch(callback, nt);
                     } else if (timeout != undefined && timeout === 0)
                         callback(null);
                    else {
                        tmutils.waitForMatch(callback, 10);
                    }
                }
            },100);
        },
        waitForTabContent: function(callback, timeout) {
            tmutils.delay(function() {
                var tabs = $('.tabs_content');
                if (tabs != null && tabs != undefined &&
                   $(tabs).height() >= 600) {
                    tmutils.delay(function() {
                        callback();
                    },100);
                } else {
                    if (timeout != undefined && timeout > 0) {
                         var nt =  timeout-1;
                         tmutils.waitForTabContent(callback, nt);
                     } else if (timeout != undefined && timeout === 0)
                         callback(null);
                    else {
                        tmutils.waitForTabContent(callback, 10);
                    }
                }
            },100);
        },
        waitForTransferlist: function(callback, timeout) {
            tmutils.delay(function() {
                var tabs = $('#transfer_list > table');
                if (tabs != null && tabs != undefined &&
                   $(tabs).find("tr") >= 1) {
                    callback();
                } else {
                    if (timeout != undefined && timeout > 0) {
                         var nt =  timeout-1;
                         tmutils.waitForTransferlist(callback, nt);
                     } else if (timeout != undefined && timeout === 0)
                         callback(null);
                    else {
                        tmutils.waitForTransferlist(callback, 10);
                    }
                }
            },100);
        }
    };

    /*
    * TMAPI
    *
    */
    function tmapi() {
        var self = this;
        self.serviceUrl = "";
        self.init = function(callback) {
            log("- INIT api -");
            if (window.location.protocol != "https:") {
                self.serviceUrl = "http://trophy.kraxnor.at/web/api/";
            } else {
                self.serviceUrl = "https://trophy.kraxnor.at/web/api/";
            }
            if (self.checkForCredentials()) {
                log("authenticated successfully");
                if (callback)
                    callback(true);
            } else {
                log("error authenticating",0);
                if (callback)
                    callback(false);
            }
        };

        self.setCookie = function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            var path = "path=/";
            document.cookie = cname + "=" + cvalue + "; " + expires + "; " + path;
        };

        self.getCookie = function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) === 0) return c.substring(name.length,c.length);
            }
            return "";
        };

        self.getApiKey = function() {
            var apiKey = self.getCookie("PlayerSaver_apiKey");
            if (apiKey !== null && apiKey != "undefined" && apiKey !== "")
                return apiKey;
            else
                return null;
        };

        self.checkForCredentials = function() {
            if (self.getApiKey() !== null) {
                return true;
            }
            self.showCredentialInput();
            return false;
        };

        self.showCredentialInput = function() {
            var dialog = $('<div></div>');
            $(dialog).css("position","absolute");
            $(dialog).css("top","0px");
            $(dialog).css("right","0px");
            $(dialog).css("width","350px");
            $(dialog).css("height","30px");
            $(dialog).css("padding","5px");
            $(dialog).css("background-color","#649024");

            $(dialog).html("PlayerSaver - Api Key: <input type='text' id='PlayerSaver_apiKey'/>");
            var apiSave = $('<input type="button" value="save"/>');
            $(dialog).append(apiSave);
            $(apiSave).click(function() {
                //verify apikey
                self.loadUser($('#PlayerSaver_apiKey').val(), function(res) {
                    if (res !== null) {
                        //store apikey
                        self.setCookie('PlayerSaver_apiKey',$('#PlayerSaver_apiKey').val(),90);
                        window.setTimeout(function(){
                            $(dialog).hide();
                            window.location.reload();
                        },500);
                    } else {
                        var msg = "Wrong Api Key, please try again";
                        log(msg,0);
                        alert(msg);
                    }
                });
            });
            $('body').append(dialog);
        };

        self.loadUser = function(apiKey, callback) {
            var url = self.serviceUrl + "user?apikey="+apiKey;
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                }
            });
        };

        self.loadLastPlayerRecord = function(playerId, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/lastrecord"+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                }
            });
        };

        self.loadCurrentPlayerRecord = function(playerId, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/record"+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){

                    if (callback)
                        callback(res);
                }
            });


        };

        self.postPlayerRecord = function(playerId, record, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/records"+"?apikey="+self.getApiKey();
            log("posting: "+url,2);
            $.ajax
            ({
                type: "POST",
                url: url,
                dataType: 'json',
                data: record ,
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                }
            });
        };

        self.loadPlayerTransferHistory = function(playerId, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/transfers"+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                }
            });
        };

        self.postPlayerTransferRecord = function(playerId, record, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/transfers"+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {"record": record },
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                },
            });
        };

        self.loadPlayerGraphs = function(playerId, graphType, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/graphs/"+graphType+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                },
            });
        };

        self.loadClubSelldata = function(clubId, pids, callback) {
            if (pids === null || pids === undefined)
                pids = [];
            var pidsStr = "";
            $(pids).each(function() {
                pidsStr += this+",";
            });

            var url = self.serviceUrl + "clubs/"+clubId+"/selldata"+"?apikey="+self.getApiKey()+"&pids="+pidsStr;
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                },
            });
        };

        self.updateSellData = function(playerId, type, value, callback) {
            var url = self.serviceUrl + "players/"+playerId+"/selldatas/"+type+"/updates/"+value+"?apikey="+self.getApiKey();
            log("calling: "+url,2);
            $.ajax
            ({
                type: "GET",
                url: url,
                dataType: 'json',
                error: function() {
                    if (callback)
                        callback(null);
                },
                success: function (res){
                    if (callback)
                        callback(res);
                },
            });
        };

        self.getTMTactics = function(callback) {
            var tactics = {};
            var url = "//trophymanager.com/ajax/tactics_get.ajax.php";
            var urlclub = {reserves: 0, miniGameId: 0};
            var urlbteam = {reserves: 1, miniGameId: 0};
            var callCnt = 2;
            $.each([urlclub,urlbteam],function(i,e) {
                log("calling: "+url,2);
                $.ajax
                ({
                    type: "POST",
                    url: url,
                    data: e,
                    dataType: 'json',
                    error: function() {
                        callCnt--;
                        if (callCnt === 0) {
                            if (callback) {
                                callback(null);
                            }
                        }
                    },
                    success: function (res){
                        callCnt--;
                        if (i === 0)
                            tactics.club = res;
                        if (i == 1)
                            tactics.bteam = res;

                        if (callCnt === 0) {
                            if (callback) {
                                callback(tactics);
                            }
                        }
                    },
                });
            });
        };
    }

    function playerseller(_api) {
        var self = this;
        self._levent = null;
        self._loaded = function() {
            log(self.tacticsData,3);
            log(self.clubData,3);
            log(self.bteamData,3);

            if (self._levent)
                self._levent();
        };
        self.loaded = function(levent) {
            self._levent = levent;
        };
        self.api = _api;
        self.player = null;
        self.tacticsData = null;

        self.bloomedStatusMapping = {
            "ne": "Not bloomed: Early bloomer",
            "nn": "Not bloomed: Normal bloomer",
            "nl": "Not bloomed - Late bloomer",
            "sb": "Starting to bloom",
            "mb": "In the middle of his bloom",
            "lb": "In his late bloom",
            "b": "Bloomed",
        };
        self.dongleColors = {
            "red": "#f40",
            "yellow": "#FFFF00",
            "green": "#cf0",
        };
        self.clubData = null;
        self.bteamData = null;

        var cacheName = "playerseller_tooltip_cache_3";
        var tooltipCache = {
            players: {},
            lastSave: new Date().getTime(),
        };

        self.init = function(player) {
            log("####### INIT PlayerSeller2.0 #######");
            if (window.localStorage.getItem(cacheName)) {
                tooltipCache = JSON.parse(window.localStorage.getItem(cacheName));
                var now = new Date();
                var cacheDiff = now.getTime() - parseInt(tooltipCache.lastSave);
                if (cacheDiff > 0.5 * 60 * 60 * 1000) {
                    window.localStorage.removeItem(cacheName);
                }
            } else {
                window.localStorage.setItem(cacheName,JSON.stringify(tooltipCache));
            }
            self.player = player;
            tmutils.cache.getTactics(self.api,function(res) {
                self.tacticsData = res;
                tmutils.delay(self.loadSelldata,200);
            });
        };

        var seasonStartFirst = new Date("2015/11/30 00:00");
        var seasonDates = [
            {
                "s":44,
                "start":seasonStartFirst.getTime(),
                "half":seasonStartFirst.getTime() + (6 * 7 * 24 * 60 * 60 * 1000),
                "end":seasonStartFirst.getTime() + (12 * 7 * 24 * 60 * 60 * 1000),
            }
        ];
        for(i = 45; i < 65; i++) {
            var lastEnd = new Date(seasonDates[i - 45].end);
            seasonDates.push({
                "s":i,
                "start":lastEnd.getTime(),
                "half":lastEnd.getTime() + (6 * 7 * 24 * 60 * 60 * 1000),
                "end":lastEnd.getTime() + (12 * 7 * 24 * 60 * 60 * 1000),
            });
        }
        self.getCurrentSeasonHalf = function() {
            var now = new Date();
            for(i = 0; i < seasonDates.length; i++) {
                if (seasonDates[i].start < now.getTime() && now.getTime() <= seasonDates[i].end)
                    return new Date(seasonDates[i].half);
            }
            return null;
        };
        self.getCurrentSeasonEnd = function() {
            var now = new Date();
            for(i = 0; i < seasonDates.length; i++) {
                if (seasonDates[i].start < now.getTime() && now.getTime() <= seasonDates[i].end)
                    return new Date(seasonDates[i].end);
            }
            return null;
        };

        self.loadSelldata = function() {
            var callsCnt = 2;
            var processPlayerSellData = function() {
                var resO = this;
                setTimeout(function() {
                    if (self.player !== null && self.player !== undefined && self.player.playerId == resO.playerId) {
                        $('#ps_player_box_type_select_'+resO.playerId).val(resO.sellData.type);
                        $('#ps_player_seller_save_button').prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                        
                        var pot = $('#ps_player_box_potential_'+resO.playerId);
                        if (resO.sellData.potential && resO.sellData.potential !== null && resO.sellData.potential != "undefined" && parseInt(resO.sellData.potential) > 0) {
                            if ($(pot).prop("disabled") === true && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                $(pot).val(resO.sellData.potential);
                            }
                            if ($(pot).prop("disabled") === false && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                $(pot).next("label").after("<br/><small><i>value changed, push save button</i></small>");
                            }
                        } else {
                            if ($(pot).prop("disabled") === false && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                $(pot).next("label").after("<br/><small><i>value changed, push save button</i></small>");
                            } else {
                                $(pot).next("label").after("<br/><small><i>*go to scout tab and reload page<br/> to update avg pot.</i></small>");
                            }
                        }
                        
                        var stat = $('#ps_player_box_status_'+resO.playerId);
                        if (resO.sellData.status !== undefined && resO.sellData.status !== null && resO.sellData.status != -1) {
                            if ($('#ps_player_box_status_'+resO.playerId+'').val() == -1)
                                $('#ps_player_box_status_'+resO.playerId+' option[value='+resO.sellData.status+']').prop('selected', true);
                        }
                        return true;
                    }
                },100);
            };

            var cpids = [];
            $.each(self.tacticsData.club.players, function(i,e) {
                cpids.push(parseInt(e.player_id));
            });
            self.api.loadClubSelldata(SESSION.main_id,cpids, function(res) {
                self.clubData = res.data;
                $(self.clubData).each(processPlayerSellData);
                callsCnt--;
                if (callsCnt === 0) {
                    log("loaded selldata");
                    self._loaded();
                }
            });
            var bpids = [];
            $.each(self.tacticsData.bteam.players, function(i,e) {
                bpids.push(parseInt(e.player_id));
            });
            self.api.loadClubSelldata(SESSION.b_team,bpids, function(res) {
                self.bteamData = res.data;
                $(self.bteamData).each(processPlayerSellData);
                callsCnt--;
                if (callsCnt === 0) {
                    log("loaded selldata");
                    self._loaded();
                }
            });
        };

        self.detectScoutPage = function() {
            var res = {};
            res.detected = false;
            res.avgPotential = 0;
            res.reportCount = 0;
            res.status = "";
            res.date = -1;
            var scoutTab = $('#player_scout_new');
            if ($(scoutTab).is(':visible')) {
                res.detected = true;
                var reports = $('#player_scout_new > div');
                var repCnt = reports.length;
                $(reports).each(function(i,e) {
                    var rows = $(this).children("div");
                    var pot = rows[3];
                    pot = $(pot).text();
                    if (i === 0) {
                        var tstatus = rows[4];
                        var status = -1;
                        tstatus = $(tstatus).text();
                        var stps = tstatus.split("status: ");
                        tstatus = stps[1];
                        $.each(self.bloomedStatusMapping, function(i,e) {
                            if (e == tstatus)
                                status = i;
                        });
                        res.status = status;
                        var tdate = rows[0];
                        if (tdate !== null && tdate !== undefined) {
                            tdate = tdate.children[1];
                            tdate = $(tdate).text();
                            tdate = tdate.replace("(","").replace(")","");
                            var tparts = tdate.split("-");
                            tdate = new Date(parseInt(tparts[0]),parseInt(tparts[1])-1,parseInt(tparts[2]));
                            tdate = Math.round(tdate.getTime()/1000);
                            res.date = tdate;
                        }
                    }
                    var temp = pot.split("(");
                    if (temp !== null && temp.length > 1 && res.reportCount < (repCnt-1) && res.reportCount < 5) {
                        pot = temp[1].replace(")","");
                        res.avgPotential += parseInt(pot);
                        res.reportCount++;
                    }
                });
                res.avgPotential = res.avgPotential / res.reportCount;
            }
            return res;
        };

        self.loadPlayerMenu = function(player) {
            tmutils.delay(function() {
                if (self.player.isOwnPlayer()) {
                    var scoutr = self.detectScoutPage();

                    var cont = $('.column3_a');
                    var box = $('<div class="box" style="margin-top:20px"></div>');
                    $(box).append('<div class="box_head"><h2 class="std">Player Seller<br/><small><i>PlayerSaver</i></small></h2></div>');
                    var boxContent = $('<div class="box_body"></div>');
                    $(boxContent).append('<div class="box_shadow"></div>');

                    var boxBody = $('<div class="std align_center transfer_box"></div>');

                    var form = $('<form></form>');
                    var select = $('<select id="ps_player_box_type_select_'+self.player.playerId+'"></select>');

                    $(select).append('<option value="sell_3_0">sell 3.0 stars</option>');
                    $(select).append('<option value="sell_3_5">sell 3.5 stars</option>');
                    $(select).append('<option value="sell_4_0">sell 4.0 stars</option>');
                    $(select).append('<option value="sell_4_5">sell 4.5 stars</option>');
                    $(select).append('<option value="sell_5_0">sell 5.0 stars</option>');

                    $(select).append('<option value="talent">talent</option>');

                    $(select).append('<option value="first_16">first 16</option>');
                    $(select).append('<option value="reserve_16">reserve 16</option>');

                    $(select).append('<option value="sold">sold</option>');
                    $(select).append('<option value="remove">remove</option>');
                    $(form).append("<label>Type: </label>");
                    $(form).append(select);

                    var button = $('<button id="ps_player_seller_save_button">save</button>');
                    $(button).click(function() {
                        log("saving selldata");
                        var sbtn = this;
                        $(sbtn).html("save");
                        //update selldata: type
                        self.api.updateSellData(self.player.playerId, "type", $(select).val(), function(res) {
                            if (res === null) {
                                $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                            } else {
                                var roundVal = null;
                                if ($('#ps_player_box_type_frnd_'+self.player.playerId).prop("checked")) {
                                    roundVal = 1;
                                } else if ($('#ps_player_box_type_srnd_'+self.player.playerId).prop("checked")) {
                                    roundVal = 2;
                                } else if ($('#ps_player_box_type_nrnd_'+self.player.playerId).prop("checked")) {
                                    roundVal = 0;
                                }
                                //update selldata: round
                                self.api.updateSellData(self.player.playerId, "round", roundVal, function(resr) {
                                    if (resr === null) {
                                        $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                    } else {
                                        var pot = $('#ps_player_box_potential_'+self.player.playerId);
                                        if ($(pot).prop("disabled") === false && $(pot).val() > 0) {
                                            var potStr = (""+$(pot).val()).replace(".",",");
                                            //update selldata: potential
                                            self.api.updateSellData(self.player.playerId, "potential", potStr, function(resp) {
                                                if (resp === null) {
                                                    $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                } else {
                                                    var stat = $('#ps_player_box_status_'+self.player.playerId);
                                                    if ($(stat).prop("disabled") === false && $(stat).val() != -1) {
                                                        //update selldata: status
                                                        self.api.updateSellData(self.player.playerId, "status", $(stat).val(), function(ress) {
                                                            if (ress === null) {
                                                                $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                            } else {
                                                                var dt = $('#ps_player_box_date_'+self.player.playerId);
                                                                //update selldata: date
                                                                self.api.updateSellData(self.player.playerId, "date", $(dt).val(), function(resd) {
                                                                    if (resd === null) {
                                                                        $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                                    } else {
                                                                        var tbutton = $('.transfer_box').find('span.button.disabled')[0];
                                                                        if (tbutton !== null) {
                                                                            //console.log($(tbutton).attr("tooltip"));
                                                                            var ddate_str = $(tbutton).attr("tooltip");
                                                                            if (ddate_str !== null && ddate_str !== undefined) {
                                                                                ddate_str = ddate_str.replace("This player locked for transfer until ","");
                                                                                var ddate = new Date(ddate_str);
                                                                                ddate_str = Math.floor(ddate.getTime() /1000);
                                                                            }
                                                                            //update selldata: transferDate
                                                                            self.api.updateSellData(self.player.playerId, "transferDate", ddate_str, function(restd) {
                                                                                if (restd === null) {
                                                                                    $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                                                } else {
                                                                                    $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                                                                }
                                                                            });
                                                                        }  else {
                                                                            $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                                                        }

                                                                        $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                                                    }
                                                                });
                                                                $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                                            }
                                                        });
                                                        $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                                    }
                                                }
                                            });
                                            $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                        }
                                    }
                                });
                                $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                            }
                        });
                        return false;
                    });

                    $(form).append(button);

                    $(form).append("<br/>");
                    var nrnd = $('<input type="radio" name="ps_player_box_type_rnd_'+self.player.playerId+'[]" id="ps_player_box_type_nrnd_'+self.player.playerId+'" checked/>');
                    $(form).append(nrnd);
                    $(form).append("<label>no</label>");

                    var frnd = $('<input type="radio" name="ps_player_box_type_rnd_'+self.player.playerId+'[]" id="ps_player_box_type_frnd_'+self.player.playerId+'" style="margin-left:10px"/>');
                    $(form).append(frnd);
                    $(form).append("<label>1st</label>");

                    var srnd = $('<input type="radio" name="ps_player_box_type_rnd_'+self.player.playerId+'[]" id="ps_player_box_type_srnd_'+self.player.playerId+'" style="margin-left:10px"/>');
                    $(form).append(srnd);
                    $(form).append("<label>2st</label>");


                    $(form).append("<br/>");
                    var pot = $('<input type="text" name="ps_player_box_potential_'+self.player.playerId+'" id="ps_player_box_potential_'+self.player.playerId+'" style="width:50px;margin-right:10px"><label>Avg. Pot</label>');
                    if (scoutr.detected === false)
                        $(pot).prop("disabled",true);
                    else {
                        $(pot).val(scoutr.avgPotential.toFixed(2));
                    }
                    $(form).append(pot);

                    var stDiv = $('<div style="width:100%;margin-top:5px"></div>');
                    var status = $('<select name="ps_player_box_status_'+self.player.playerId+'" id="ps_player_box_status_'+self.player.playerId+'"></select>');
                    $(status).append("<option value='-1'>---</option>");
                    $.each(self.bloomedStatusMapping,function(i,e) {
                        if (scoutr.status == i)
                            $(status).append("<option value='"+i+"' selected>"+e+"</option>");
                        else
                            $(status).append("<option value='"+i+"'>"+e+"</option>");
                    });

                    $(stDiv).append(status);
                    $(form).append(stDiv);

                    var dDiv = $('<div style="width:100%;margin-top:5px"></div>');
                    var dDivIn = $('<input type="hidden" id="ps_player_box_date_'+self.player.playerId+'"/>');
                    $(dDiv).html(dDivIn);

                    if (scoutr.date !== null && scoutr.date !== undefined && scoutr.date > 0) {
                        $(dDivIn).val(scoutr.date);
                        var tdate = new Date(scoutr.date * 1000);
                        var datestring = ("0"+tdate.getDate()).slice(-2)  + "-" + ("0"+(tdate.getMonth()+1)).slice(-2) + "-" + tdate.getFullYear();
                        var tdateDiff = Date.now() - tdate.getTime();
                        var days = (14 * 24 * 60 * 60 * 1000);
                        var dateEle = $('<div>Last report: </div>');
                        if (tdateDiff > days)
                            $(dateEle).css("color","#FF8A8A");
                        $(dateEle).append(datestring);
                        $(dDiv).append(dateEle);
                    }

                    $(form).append(dDiv);

                    $(boxBody).append(form);
                    $(boxContent).append(boxBody);

                    $(box).append(boxContent);
                    $(box).append('<div class="box_footer"><div></div></div>');

                    $(cont).append(box);
                }
            }, 200);
        };

        self.initSellerDongle = function() {
            self.createSellerDongle();
            self.fillSellerDongle();
        };

        var ps_menuOpen = false;
        var ps_menuOpen_width = "420px";
        var ps_menuClosed_width = "320px";
        var ps_menuClosed_height = "55px";
        var resTotals = {
            wages: 0,
            onList: 0,
            fRnd: 0,
            sRnd: 0,
        };

        self.fillSellerDongle = function() {
            var saveRequiredPlayers = [];
            if (self.clubData === null || self.bteamData === null) {
            } else {
                log("club players: "+self.clubData.length,2);
                log("bteam players: "+self.bteamData.length,2);
                $.each([self.clubData,self.bteamData],function(i,e) {
                    var rescb = this;
                    var resCntMax = rescb.length;
                    var resCnt = 0;
                    var tbody = $("#playerseller_overview_table tbody");
                    //check all players that are in team but not in selldata
                    if (i === 0) {
                        $.each(self.tacticsData.club.players,function(ii,ee) {
                            var inSellData = false;
                            $(rescb).each(function() {
                                if (this.playerId == ee.player_id) {
                                    inSellData = true;
                                    return;
                                }
                            });
                            if (!inSellData) {
                                //todo: check in lastrecord if save required
                                tmutils.cache.getPlayerCurrentRecord(self.api,ee.player_id,function(res) {
                                    if (res !== null) {
                                        if (res.saveRequired !== null && res.saveRequired === true) {
                                            //console.log(res);
                                            saveRequiredPlayers.push(res);
                                        }
                                    } else {
                                        saveRequiredPlayers.push({
                                            playerId: ee.player_id,
                                            name: ee.name
                                        });
                                        //console.log("aa");
                                        log("added save required player",2);
                                    }
                                });

                            }
                        });
                    }
                    if (i == 1) {
                        $.each(self.tacticsData.bteam.players,function(ii,ee) {
                            var inSellData = false;
                            $(rescb).each(function() {
                                if (this.playerId == ee.player_id) {
                                    inSellData = true;
                                    return;
                                }
                            });
                            if (!inSellData) {
                                tmutils.cache.getPlayerCurrentRecord(self.api,ee.player_id,function(res) {
                                    if (res !== null) {
                                        if (res.saveRequired !== null && res.saveRequired === true) {
                                            saveRequiredPlayers.push(res);
                                        }
                                    } else {
                                        saveRequiredPlayers.push({
                                            playerId: ee.player_id,
                                            name: ee.name
                                        });
                                        log("added save required player");
                                    }
                                });
                            }
                        });
                    }
                    $(rescb).each(function() {
                        var resO = this;
                        if (resO.saveRequired === true) {
                            if ((tooltipCache.players[resO.playerId] == null || tooltipCache.players[resO.playerId].si === undefined) || tooltipCache.players[resO.playerId].si != resO.si) {
                                saveRequiredPlayers.push(resO);
                                //console.log(resO);
                            }
                        }
                        if (resO.sellData !== null && resO.sellData !== undefined && resO.sellData.type &&
                            (resO.sellData.type == "sell_3_0" || resO.sellData.type == "sell_3_5" || resO.sellData.type == "sell_4_0" || resO.sellData.type == "sell_4_5" || resO.sellData.type == "sell_5_0")
                           ) {
                            var tr = $('<tr id="ps_player_row_'+resO.playerId+'" class="ps_player_row_'+resO.sellData.type+'"></tr>');
                            var rec = 0;
                            $(tr).append('<td class="ps_player_type_'+resO.sellData.type+'">'+resO.sellData.type.replace("sell_","")+'</td>');
                            $(tr).append('<td><span class="ps_player_rec"></span></td>');
                            if (resO.rec.indexOf("/") != -1) {
                                var recs = resO.rec.split("/");
                                if (recs[0] > recs[1]) {
                                    $(tr).append('<td>'+recs[0]+'</td>');
                                } else {
                                    $(tr).append('<td>'+recs[1]+'</td>');
                                }
                            } else
                                $(tr).append('<td>'+resO.rec+'</td>');
                            var plNameStr = (this.pos ? '['+this.pos+'] ' : '' )+this.name;
                            if (plNameStr.length > 24) {
                                plNameStr = (this.pos ? '['+this.pos+'] ' : '' )+this.name.substring(0,16);
                            }
                            $(tr).append('<td><div><span class="ps_player_name"><a target="_blank" href="/players/'+resO.playerId+'/'+this.name+'/" class="normal" player_link="'+resO.playerId+'">'+plNameStr+' (ti:'+resO.tiGain+')</a></span></td>');
                            $(tr).append('<td><span class="ps_player_age"></span></td>');

                            $(tr).append('<td><span class="ps_player_status"></span></td>');
                            $(tbody).append(tr);
                            activate_player_links($('#playerseller_overview_table a'));

                            if (!tooltipCache.players[resO.playerId]) {
                                $.post("/ajax/tooltip.ajax.php",{"player_id":resO.playerId, minigame:""},function(data){
                                    if(data !== null) {
                                        log("loaded tooltip for "+resO.playerId+" from tm-server");
                                        data = JSON.parse(data);
                                        var p = data.player;
                                        tooltipCache.players[resO.playerId] = p;
                                        tooltipCache.lastSave = new Date().getTime();
                                        self.updatePsRow(resO,p);
                                        resCnt++;
                                        if (resCnt >= resCntMax) {
                                            window.setTimeout(function() {
                                                self.updatePsTable(tbody);
                                            },300);
                                        }
                                        window.localStorage.setItem(cacheName,JSON.stringify(tooltipCache));
                                        if (p.club_id != SESSION.main_id && p.club_id != SESSION.b_team) {
                                            self.api.updateSellData(resO.playerId,"type","sold",function(res) {
                                                //log(res);
                                            });
                                        }
                                    }
                                });
                            } else {
                                var p = tooltipCache.players[resO.playerId];
                                self.updatePsRow(resO,p);
                                resCnt++;
                                if (resCnt >= resCntMax) {
                                    self.updatePsTable(tbody);
                                }
                                if (p.club_id != SESSION.main_id && p.club_id != SESSION.b_team) {
                                    self.api.updateSellData(resO.playerId,"type","sold",function(res) {
                                        //log(res);
                                    });
                                }
                            }
                        } else if (resO.sellData !== null && resO.sellData !== undefined && resO.sellData.type && resO.sellData.type != "sold" && resO.sellData.type != "remove") {
                            resCnt++;
                            if (resCnt >= resCntMax) {
                                self.updatePsTable(tbody);
                            }
                        }
                    });
                });
                tmutils.delay(function() {
                    log(saveRequiredPlayers,3);
                    if (saveRequiredPlayers.length > 0) {
                        var pllist = $("<ul></ul>");
                        var wdiv = $('<div></div>');
                        $(wdiv).attr("id","player_seller_warning");
                        $(wdiv).css("background","#f40");
                        var msg = $('<a></a>');
                        $(msg).html("Warning: you have "+saveRequiredPlayers.length+" unsaved players");
                        $(wdiv).html(msg);
                        $(wdiv).append(pllist);
                        $(msg).click(function() {
                            $(pllist).toggle();
                            return false;
                        });
                        $(pllist).css("display","none");

                        $('#ps_menu_div_container').parent().after(wdiv);

                        log("Save required players",2);
                        $.each(saveRequiredPlayers, function() {
                            var player = this;
                            log(player,2);
                            var li = $('<li></li>');
                            var a = $('<a>'+player.name+'</a>');
                            var pllurl = "//trophymanager.com/players/"+player.playerId+"/"+player.name;
                            $(a).attr("href",pllurl);
                            $(a).attr("target","_blank");
                            $(li).html(a);
                            $(pllist).append(li);
                            return;
                        });
                    }
                },300);
            }
        };

        self.createSellerDongle = function() {
            log("load playerseller dongle");
            var ps_menuContainer = $("<div></div>").css("position","fixed").css("top","0").css("left","0").css("z-index","10");
            $(ps_menuContainer).css("max-width",ps_menuOpen_width);
            $(ps_menuContainer).css("font-size","13px");
            $(ps_menuContainer).css("line-height","140%");
            var ps_menu = $("<div></div>");
            $(ps_menu).css("width",ps_menuClosed_width).css("height",ps_menuClosed_height);
            $(ps_menu).css("background","rgb(51, 51, 51)");
            $(ps_menu).css("overflow","hidden");
            $(window).resize(function() {
                var w = $(window).width();
                if (w < 1200) {
                    $(ps_menuContainer).css("top","auto");
                    $(ps_menuContainer).css("bottom","0");
                } else {
                    $(ps_menuContainer).css("top","0");
                    $(ps_menuContainer).css("bottom","auto");
                }
            });
            if ($(window).width() < 1200) {
                $(ps_menuContainer).css("top","auto");
                $(ps_menuContainer).css("bottom","0");
            }
            var menuStatus = $("<div id='ps_menu_div_status'></div>");
            var ps_menuHeader = $('<div style="width:100%;float:left;padding-left:3px;"><small id="ps_menu_div_title" style="margin:15px 10px;display:none;">PlayerSeller 2.1</small></div>');
            $(ps_menuHeader).append(menuStatus);
            $(ps_menuHeader).css("cursor","pointer");
            $(ps_menuHeader).click(function() {
                if (!ps_menuOpen){
                    $(ps_menu).css("width","100%").css("min-height","200px");
                    $(ps_menu).css("height","auto");
                    $('#ps_menu_div_container'/*,#ps_menu_div_title'*/).show();
                    $(ps_menu).css("padding","10px");
                    $('#player_seller_warning').css("width","100%");
                    $('#player_seller_warning').css("padding","0 10px");
                    ps_menuOpen = true;
                } else {
                    window.setTimeout(function() {
                        $('#ps_menu_div_container'/*,#ps_menu_div_title'*/).hide();
                        $(ps_menu).css("width",ps_menuClosed_width).css("height",ps_menuClosed_height);
                        $('#player_seller_warning').css("width",ps_menuClosed_width)
                        $('#player_seller_warning').css("padding","0 0px");
                        $(ps_menu).css("min-height",ps_menuClosed_height);
                        //$(ps_menu).css("overflow","hidden");
                        $(ps_menu).css("padding","0px");
                        ps_menuOpen = false;
                    },200);
                }
            });
            var stTable = $('<table></table>');
            $(menuStatus).css("float","right");
            $(menuStatus).css("padding","3px 10px");
            $(menuStatus).css("width","95%");
            $(stTable).css("width","45%");
            $(stTable).css("line-height","1.1em");
            $(stTable).css("float","right");
            $(stTable).append('<tr align="center"><td style="border-left:1px solid white;border-right:1px solid white;color:gold"><b>3.5</b></td><td style="color:gold;border-right:1px solid white;"><b>4.0</b></td><td style="color:gold"><b>4.5</b></td></tr>');
            $(stTable).append('<tr align="center"><td style="border-left:1px solid white;border-right:1px solid white;"><span id="ps_menu_status_35cnt">0</span></td><td style="border-right:1px solid white;"><span id="ps_menu_status_40cnt">0</span></td><td><span id="ps_menu_status_45cnt">0</span></td></tr>'); 
            var stTable2 = $('<table></table>');
            $(stTable2).css("width","50%");
            $(stTable2).css("float","left");
            $(stTable2).css("font-size","1.0em");
            $(stTable2).css("line-height","1.1em");
            $(stTable2).append("<tr><td>wgs: </td><td><span id='ps_menu_status_wages'>0</span></td><td>oTL: </td><td><span id='ps_menu_status_onlist'>0</span></td></tr>");
            $(stTable2).append("<tr><td>1Rnd: </td><td><span id='ps_menu_status_frnd'>0</span></td><td>2Rnd: </td><td><span id='ps_menu_status_srnd'>0</span></td></tr>");
            var shalf = self.getCurrentSeasonHalf();
            var now = new Date();
            if (now.getTime() >= shalf.getTime())
                shalf = self.getCurrentSeasonEnd();

            var diffHalf = new Date(shalf.getTime()-now.getTime());
            var diffHalfDays = Math.ceil(diffHalf/(24 * 60 * 60 * 1000));
            shalf.setDate(shalf.getDate()-5);
            $(stTable2).append("<tr><td>rem: </td><td colspan='3'><span id='ps_menu_status_seasonhalf'><b>"+diffHalfDays+"d - "+tmutils.formatDate(shalf)+"</b></span></td></tr>");

            $(menuStatus).append(stTable2);
            $(menuStatus).append(stTable);

            var ps_menuDiv = $("<div style='width:100%; display:none;' id='ps_menu_div_container'></div>");
            $(ps_menu).html(ps_menuDiv);
            $(ps_menu).append(ps_menuHeader);

            $(ps_menuContainer).append(ps_menu);

            if (location.href.indexOf("/players/") != -1) {
                var ps_menuDongle = $("<div id='ps_PlayerSaverMenu_dongle'></div>").css("max-width",ps_menuOpen_width).css("height",ps_menuClosed_height);
                $(ps_menuDongle).css("background",self.dongleColors.green);
                $(ps_menuDongle).css("display","none");
                $(ps_menuContainer).append(ps_menuDongle);
            }
            $("body").append(ps_menuContainer);

            var table = $('<table id="playerseller_overview_table" cellpadding="0" cellspacing="0"></table>');
            var thead = $('<thead></thead>');
            $(table).append(thead);

            var th = $('<tr align="center"></tr>');
            $(th).append('<th>T</th>');
            $(th).append('<th>Rec</th>');
            $(th).append('<th>RR</th>');
            $(th).append('<th>Player</th>');
            $(th).append('<th>Age</th>');
            $(th).append('<th>St.</th>');

            $(thead).append(th);
            $(table).append(thead);
            var tbody = $('<tbody></tbody>');
            $(table).append(tbody);
            $(ps_menuDiv).append(table);
        };

        self.updatePsRow = function(resO,p) {
            //log(p);
            $('#ps_player_row_'+resO.playerId+" .ps_player_rec").html(p.recommendation);
            $('#ps_player_row_'+resO.playerId+" .ps_player_age").html(p.age+"."+p.months);
            $('#ps_player_row_'+resO.playerId+" .ps_player_name").prepend(p.flag);
            var row = $('#ps_player_row_'+resO.playerId);
            var timage = null;
            if ((resO.sellData.type == "sell_3_0" && p.rec_sort >= 3.0 && p.rec_sort < 3.5) ||
                (resO.sellData.type == "sell_3_5" && p.rec_sort >= 3.5 && p.rec_sort < 4.0) ||
                (resO.sellData.type == "sell_4_0" && p.rec_sort >= 4.0 && p.rec_sort < 4.5) ||
                (resO.sellData.type == "sell_5_0" && p.rec_sort >= 5) ||
                (resO.sellData.type == "sell_4_5" && p.rec_sort >= 4.5 && p.rec_sort < 5.0)) {
                if (resO.sellData.transferDate && resO.sellData.transferDate !== null && resO.sellData.transferDate != "undefined") {
                    if (new Date().getTime() < parseInt(resO.sellData.transferDate)*1000) {
                        $('#ps_player_row_'+resO.playerId+" td").css("background","#696969");
                        $(row).addClass("ps_player_row_orange");
                        timage = $('<img src="/pics/small_red_x.png" title="sell '+tmutils.formatDate(new Date(parseInt(resO.sellData.transferDate)*1000))+'">');
                        $('#ps_player_row_'+resO.playerId+' .ps_player_status').append(timage);
                    } else {
                        $('#ps_player_row_'+resO.playerId+" td").css("background","#2e8b57");
                        $(row).addClass("ps_player_row_green");
                    }
                } else {
                    $('#ps_player_row_'+resO.playerId+" td").css("background","#2e8b57");
                    $(row).addClass("ps_player_row_green");
                }
            } else {
                $('#ps_player_row_'+resO.playerId+" td").css("background","darkred");
                $(row).addClass("ps_player_row_red");
            }
            //check transfer status
            var onList = false;
            var hasEndTime = false;
            if (resO.transferRecords && resO.transferRecords.length > 0) {
                var onListEnd = null;
                var lastTRecord = null;
                $(resO.transferRecords).each(function(i,t) {
                    var stringToParse = t.endTime;
                    var dateParts     = stringToParse.match(/(\d{2})\.(\d{2})\.(\d{4})\s+(\d{2}):(\d{2})/);
                    dateParts[2] -= 1; //months are zero-based
                    dateParts = dateParts.slice(1);
                    var endTime = new Date(dateParts[2], dateParts[1], dateParts[0], dateParts[3], dateParts[4], 0,0);
                    lastTRecord = endTime;
                    if (new Date().getTime() < endTime.getTime())  {
                        onList = true;
                        onListEnd = endTime;
                        hasEndTime = true;
                    }
                });
                if (onList) {
                    timage = $('<img src="/pics/auction_hammer_small.png" title="until '+onListEnd.toLocaleString()+'">');
                    $('#ps_player_row_'+resO.playerId+' .ps_player_status').append(timage);
                    resTotals.onList++;
                } else {
                    if (new Date().getTime() < lastTRecord.getTime() + (5 * 24 * 60 * 60 * 1000))  {
                        $('#ps_player_row_'+resO.playerId+' .ps_player_status').append("+");
                    }
                }
            }
            if (resO.sellData.round && resO.sellData.round !== null && resO.sellData.round != "undefined" && parseInt(resO.sellData.round) > 0) {

                $('#ps_player_row_'+resO.playerId+' .ps_player_status').append("<span style='background:#333;color:#fff;padding:1px 3px'>"+resO.sellData.round+"</span>");
                if (parseInt(resO.sellData.round) == 1) {
                    $('#ps_player_box_type_frnd_'+resO.playerId).prop("checked",true);
                    resTotals.fRnd++;
                } else if (parseInt(resO.sellData.round) == 2) {
                    $('#ps_player_box_type_srnd_'+resO.playerId).prop("checked",true);
                    resTotals.sRnd++;
                } else {
                    $('#ps_player_box_type_nrnd_'+resO.playerId).prop("checked",true);
                }
            } else {
                $('#ps_player_box_type_nrnd_'+resO.playerId).prop("checked",true);
            }
            var pwage = $(p.wage);
            var pwt = $(pwage).text();
            pwt = pwt.replace(",","").replace(",","").replace(",","");
            resTotals.wages += parseInt(pwt);
        };

        self.sortPsRowsCompare = function(a,b) {
            var rra = $(a).find('td:nth-child(3)').text();
            var rrb = $(b).find('td:nth-child(3)').text();
            //console.log(rra);
            rra = parseFloat(rra);
            rrb = parseFloat(rrb);
            if (rra < rrb) {
                return 1;
            }
            if (rra > rrb) {
                return -1;
            }
            return 0;
        };

        self.updatePsTable = function(tbody) {
            var greens = $(tbody).find("tr.ps_player_row_green");
            var oranges = $(tbody).find("tr.ps_player_row_orange");
            var reds = $(tbody).find("tr.ps_player_row_red");

            $('#ps_menu_status_35cnt').html("<b>"+$('.ps_player_type_sell_3_5').length+"</b>"+"<br/>");

            $('#ps_menu_status_35cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_3_5').length+"</span");
            $('#ps_menu_status_35cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_3_5').length+"</span");
            $('#ps_menu_status_35cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_3_5').length+"</span");

            $('#ps_menu_status_40cnt').html("<b>"+$('.ps_player_type_sell_4_0').length+"</b>"+"<br/>");
            $('#ps_menu_status_40cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_4_0').length+"</span");
            $('#ps_menu_status_40cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_4_0').length+"</span");
            $('#ps_menu_status_40cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_4_0').length+"</span");

            $('#ps_menu_status_45cnt').html("<b>"+$('.ps_player_type_sell_4_5').length+"</b>"+"<br/>");
            $('#ps_menu_status_45cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_4_5').length+"</span");
            $('#ps_menu_status_45cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_4_5').length+"</span");
            $('#ps_menu_status_45cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_4_5').length+"</span");

            $('#ps_menu_status_wages').html("<b>"+(resTotals.wages/(1000*1000)).toFixed(2)+"m"+"</b>");
            $('#ps_menu_status_onlist').html("<b>"+resTotals.onList+"</b>");

            $('#ps_menu_status_frnd').html("<b>"+resTotals.fRnd+"</b>");
            $('#ps_menu_status_srnd').html("<b>"+resTotals.sRnd+"</b>");


            $(tbody).html("");

            greens = $(greens).sort(self.sortPsRowsCompare);

            $(greens).each(function() {
                if ($(this).hasClass('ps_player_row_sell_5_0'))
                    $(tbody).append(this);
            });
            $(greens).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_5'))
                    $(tbody).append(this);
            });
            $(greens).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_0'))
                    $(tbody).append(this);
            });
            $(greens).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_5'))
                    $(tbody).append(this);
            });
            $(greens).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_0'))
                    $(tbody).append(this);
            });

            oranges = $(oranges).sort(self.sortPsRowsCompare);

            $(oranges).each(function() {
                if ($(this).hasClass('ps_player_row_sell_5_0'))
                    $(tbody).append(this);
            });
            $(oranges).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_5'))
                    $(tbody).append(this);
            });
            $(oranges).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_0'))
                    $(tbody).append(this);
            });
            $(oranges).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_5'))
                    $(tbody).append(this);
            });
            $(oranges).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_0'))
                    $(tbody).append(this);
            });


            reds = $(reds).sort(self.sortPsRowsCompare);

            $(reds).each(function() {
                if ($(this).hasClass('ps_player_row_sell_5_0'))
                    $(tbody).append(this);
            });
            $(reds).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_5'))
                    $(tbody).append(this);
            });
            $(reds).each(function() {
                if ($(this).hasClass('ps_player_row_sell_4_0'))
                    $(tbody).append(this);
            });
            $(reds).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_5'))
                    $(tbody).append(this);
            });
            $(reds).each(function() {
                if ($(this).hasClass('ps_player_row_sell_3_0'))
                    $(tbody).append(this);
            });
        };
    }

    /*
    * TMPARSER
    *
    */
    function tmparser() {
        var self = this;
        self.init = function() {
            log(" - INIT parser -");
        };

        self.parsePlayer = function(playerTable,infoTable) {
            if(playerTable!=null) {
                setTimeout(function() {
                    var rxobj = $("b:contains('RatingR4')");
                    if (rxobj.length != 0 ) {
                        rversion = 4;
                    } else {
                        rversion = 2;
                        var overlay = $('<div/>')
                        .css("width","100%")
                        .css("height","100%")
                        .css("display","table-cell")
                        .css("position","fixed")
                        .css("top","0")
                        .css("left","0")
                        .css("z-index","10001")
                        .css("text-align","center")
                        .css("background","rgba(0,0,0,0.7)")
                        .css("padding-top","20%");

                        overlay.html("<h2>PlayerSaver Info:</h2><h3>Please uninstall the \"RatingR2 beta\" Userscript and install the new \"RatingR4 beta\":</h3><h3><a href='http://vftomato.web.fc2.com/RatingR4beta.user.js' target='_blank'>http://vftomato.web.fc2.com/RatingR4beta.user.js</a></h3>");

                        $("body").append(overlay);
                    }
                },500);
            }

            var res = {
                SKILLS: self.getSkills2(playerTable),
                INFOS: self.getInfos2(infoTable),
                HSKILLS: self.getHiddenSkills(),
            };


            return res;
        };

        self.getSkills2 = function(table) {
            var playerPosition = $('div.box_sub_header div.large span.small > strong').text();
            var skillArray = {};
            var tableData = table.getElementsByTagName("td");

            if (tableData.length > 1) {
                for (var j = 0; j < tableData.length; j += 1) {

                    var skillName = "";
                    if (playerPosition != "Goalkeeper") {
                        skillName = skillNames()[j];
                    } else {
                        skillName = skillNamesGoalKeeper[j];
                    }

                    if (tableData[j].innerHTML.indexOf("star.png") > 0) {
                        skillArray[skillName] = 20;
                    }
                    else if (tableData[j].innerHTML.indexOf("star_silver.png") > 0) {
                        skillArray[skillName] = 19;
                    }
                    else if (tableData[j].textContent.length !== 0) {
                        skillArray[skillName] = tableData[j].textContent;
                    }
                }
            }
            //console.log(skillArray);
            return skillArray;
        };

        self.getInfos2 = function(table) {
            var infoArray = {};
            var tableData = table.getElementsByTagName("td");
            if (tableData.length > 1) {
                for (var j = 1; j < tableData.length; j += 1) {
                    //if ($(tableData[j]).children().length > 0) {
                    if (infoNames[j] == "routine") {
                        var tdContent = $(tableData[j]).clone();
                        var r2 = $(tdContent).children().clone();
                        $(tdContent).remove("div");
                        var routine = {
                            "routine": $(tdContent).html().substring(0,5).replace("<d","").replace("<",""),
                            "r2":r2[0].textContent.trim(),
                        };
                        infoArray[infoNames[j]] = routine;

                    } else if (infoNames[j] == "skill index") {

                        var tData = $(tableData[j]).html();
                        var divIndex = tData.indexOf("<div>");
                        var divEndIndex = tData.indexOf("</div>");

                        var _si = tData.substring(0,divIndex);

                        var stiStart = (divIndex+5);
                        var stiEnd = (divEndIndex);

                        var _sti = tData.substring(stiStart,stiEnd);

                        var si = {
                            "si": _si,
                            "seasonTi": _sti,
                        };

                        infoArray[infoNames[j]] = si;

                    } else if (infoNames[j] == "club") {

                        var clubId = $(tableData[j]).find("a").attr("club_link");
                        var club = {
                            "clubId": clubId,
                            "name": tableData[j].textContent.trim(),
                        };

                        infoArray[infoNames[j]] = club;

                    } else if (infoNames[j] == "recommandation") {
                        //check for recrecB, ignore this value
                        var recTd = tableData[j];
                        var divs = $(recTd).find("div");

                        if (divs.length > 0)
                            infoArray[infoNames[j]] = divs[0].textContent.trim();
                        else
                            infoArray[infoNames[j]] = tableData[j].textContent.trim();

                    } else {
                        infoArray[infoNames[j]] = tableData[j].textContent.trim();
                    }
                    //}

                }
            }

            return infoArray;
        };

        function getHSkillsValue(input) {
            if (input === null || input === undefined || input === "") return 0;
            var p1 = input.split("(");
            var p2 = p1[1];
            var p3 = p2.split("/");
            return parseInt(p3[0]);
        }
        self.getHiddenSkills = function() {
            var res = {};
            var ht = $('#hidden_skill_table');
            if (ht !== null && ht !== undefined) {
                var htds = $(ht).find("td");
                $(htds).each(function(i,e) {
                    switch(i) {
                        case 0:
                            res.inuryProneness = getHSkillsValue($(this).text());
                            break;
                        case 1:
                            res.aggression = getHSkillsValue($(this).text());
                            break;
                        case 2:
                            res.professionalism = getHSkillsValue($(this).text());
                            break;
                        case 3:
                            res.adaptability = getHSkillsValue($(this).text());
                            break;
                    }
                });
            }
            return res;
        };
    }

    /*
    * TMPLAYER
    *
    */

    function tmplayer(_api) {
        var self = this;
        self.parser = new tmparser();
        self.api = _api;

        self.localData = null;
        self.remoteData = null;
        self.transferData = null;
        self.saveButton = null;
        self.playerTable = null;
        self.infoTable = null;
        self.playerId = null;
        self.playerPosition = null;

        self.init = function(ptable,itable) {
            self.parser.init();

            self.playerTable = ptable;
            self.infoTable = itable;
            self.localData = self.parser.parsePlayer(self.playerTable,self.infoTable);

            var urlArray = location.href.split("/players/");
            var urlSplitPlayer = urlArray[1].split("/");
            self.playerId= urlSplitPlayer[0].replace("#","");
            self.playerPosition = $('div.box_sub_header div.large span.small > strong').text();

            self.saveButton = document.createElement("a");
            self.saveButton.click(function() {
                self.savePlayerRecord();
            });

            var container = document.createElement("div");
            container.setAttribute("style","float:right; margin-left:10px;width:100%;");

            var label = document.createElement("label");
            label.setAttribute("style","margin-right:10px;");
            self.saveButton.setAttribute("type","button");
            $(label).html("<small id='player_last_save'></small>");
            $(container).append(label);
            $(self.saveButton).html("Save to db");
            self.saveButton.setAttribute("style","display:none;float:right;text-align:center;");

            self.saveButton.onclick=function() {
                self.savePlayerRecord();
            };

            $(container).append(self.saveButton);
            $('.skill_table:first-child').after(container);
        };

        self.isOwnPlayer = function() {
            if (self.localData.INFOS.club.clubId == SESSION.main_id ||
                self.localData.INFOS.club.clubId == SESSION.b_team) {
                return true;
            }
            return false;
        };

        self.showSaveButton = function() {
            if (self.isOwnPlayer()) {
                $(self.saveButton).attr("class","red_button");
            }
            $(self.saveButton).show();
        };

        self.loadPlayerData = function() {
            self.loadLastPlayerRecord();
            tmutils.delay(function() {
                if (self.remoteData != null && self.remoteData.players != undefined) {
                    //log(self.remoteData);
                    self.showPlayerGraphs();
                }
                if (self.isOwnPlayer()) {
                    self.loadTransferHistory();
                    self.listenTransferButton();
                }
            },200);
            
            log("loaded last player data");
            //log(self);
        };

        self.savePlayerRecord = function() {
            var playerText = $('div.box_sub_header div.large > strong').html();
            var playerPosition = $('div.box_sub_header div.large span.small > strong').text();
            var playerArray = playerText.split(". ");
            var playerName = playerArray[1];
            var playerNr = playerArray[0];

            var dataObject = {
                "player": {
                    "id":self.playerId,
                    "name":playerName,
                    "record": {
                        "nr":playerNr,
                        "position": playerPosition,
                        "skills" : self.localData.SKILLS,
                        "infos": self.localData.INFOS,
                    }
                }
            };

            log("Saving player record for: "+playerName);
            log(dataObject);

            self.api.postPlayerRecord(self.playerId, dataObject,function(res) {
                self.saveButton.setAttribute("disabled","disabled");
                $(self.saveButton).html("Saved");
                window.setTimeout(function(){
                    $(self.saveButton).hide();
                },1000);
            });
        };


        self.loadLastPlayerRecord = function() {
            self.api.loadLastPlayerRecord(self.playerId, function(res) {
                var playerTable = document.getElementsByClassName("skill_table zebra")[0];

                /*var rr = new playerrr().getDataTable(playerTable);
                log("r2 from player table");
                log(rr);*/

                if (res !== null) {
                    self.remoteData = res.data;
                    if (res.data.status !== null && res.data.status == "no player found") {
                        self.showSaveButton();
                    } else {
                        self.showSaveButton();
                        self.showSkillDiffernce();
                    }

                    /*var rr2 = new playerrr(self.api).getDataPlayer(self);
                    log("r2 from last record");
                    log(rr2);*/
                }
            });
        };

        self.loadTransferHistory = function() {
            self.api.loadPlayerTransferHistory(self.playerId, function(res) {
                if (res !== null) {
                    self.transferData = res.data;
                    var cont = $('.column3_a');

                    var box = $('<div class="box" style="margin-top:20px"></div>');
                    $(box).append('<div class="box_head"><h2 class="std">Transfer Records<br/><small><i>PlayerSaver</i></small></h2></div>');
                    var boxContent = $('<div class="box_body"></div>');
                    $(boxContent).append('<div class="box_shadow"></div>');

                    var boxBody = $('<div class="std align_center transfer_box"></div>');

                    var table = $('<table></table>');
                    var th = $('<tr></tr>');
                    $(th).append("<th>age</th>");
                    $(th).append("<th>rec/Si</th>");
                    $(th).append("<th>min/Max</th>");
                    $(th).append("<th>end</th>");
                    $(th).append("<th>info</th>");
                    $(table).append(th);
                    $(self.transferData.records).each(function(index, record) {
                        var tr = $("<tr></tr>");
                        $(tr).append("<td>"+record.age+"</td>");
                        $(tr).append("<td>"+record.rec+" (<small>"+record.si+"</small>)</td>");
                        $(tr).append("<td><b>"+(record.minPrice/1000000).toFixed(2)+"M</b> (<small>"+(record.maxPrice/1000000).toFixed(2)+"M</small>)</td>");
                        $(tr).append("<td><small>"+record.endTime+"</small></td>");

                        $(tr).append("<td><a title='note: "+record.note+"\nbold: "+record.bold+"\nblue: "+record.boldAndBlue+"\nexpiry: "+record.expiry+"' onclick='return false;'>o</a></td>");

                        $(table).append(tr);
                    });

                    $(boxBody).append(table);
                    $(boxContent).append(boxBody);

                    $(box).append(boxContent);
                    $(box).append('<div class="box_footer"><div></div></div>');

                    $(cont).append(box);
                }
            });
        };

        self.listenTransferButton = function() {
            $('#transferlist_button').on("click",function() {
            //$('#transfer_submit_button').on("click",function() {    
                var checkInterval = setInterval(function() {
                    var button = $('#modal').find('#transfer_submit_button')[0];
                    var okButton = $('#modal').find('span.button')[0];

                    var btn = null;
                    if (button !== null && button != "undefined")
                        btn = button;
                    else if (okButton !== null && okButton != "undefined")
                        btn = okButton;

                    if (btn != null) {
                        clearInterval(checkInterval);
                        
                        //console.log(btn);
                        if (button !== null && button != "undefined") {
                            log("Transferwindow opened");
                            //var newButton = $("<button>Test Transfer</button>");
                            //$(btn).after(newButton);

                            $(btn).click(function() {
                                var minprice = $('#min_price').val();
                                var maxprice = $('#max_price').val();
                                var cleanMax = maxprice.split('.')[0];
                                var expiry = "";
                                if ($('#transfer_expiry1').prop("checked") === true) {
                                    expiry = "5days";
                                } else if ($('#transfer_expiry2').prop("checked") === true) {
                                    expiry = $('#exp_time1').val();
                                } else if ($('#transfer_expiry3').prop("checked") === true) {
                                    expiry = $('#exp_days').val()+"#"+$('#exp_time2').val();
                                }
                                var note = $('#transfer_textarea').val();

                                var entry = {
                                    "playerId": self.playerId,
                                    "minPrice": minprice,
                                    "maxPrice": cleanMax,
                                    "expiry": expiry,
                                    "note": note,
                                    "bold": 0,
                                    "boldAndBlue": 0,
                                };

                                log(entry);
                                
                                self.api.postPlayerTransferRecord(self.playerId, entry, function(res) {
                                    log("Player transfer record saved");
                                    log(res);
                                });
                            });
                        }
                    }
                },300);
            });
        };

        self.showPlayerGraphs = function() {
            var divContainer1 = $('<div></div>').attr("id","chart55").css("width","100%").css("height","200px").attr("class","graph jqplot-target");
            var menuDiv = $('<div></div>').attr("class","tabs_new");
            $(availableGraphTypes).each(function(i,e) {

                var div = $('<div onclick="" id="" class="playerSaver_graph_tab" data-graph-type="'+e+'"><div style="text-transform:uppercase;">'+e+'</div></div>');
                if (i === 0) {
                    $(div).addClass("active_tab");
                }
                div.click(function() {
                    self.api.loadPlayerGraphs(self.playerId, $(this).attr("data-graph-type"),function(res) {
                        var data = res.data.players[0].graph;
                        var min = res.data.players[0].graphMin;
                        self.makePlayerGraph(divContainer1.attr("id"), data, min);
                    });

                    $('.playerSaver_graph_tab').each(function() {
                        $(this).removeClass("active_tab");
                    });
                    $(this).addClass("active_tab");
                });

                menuDiv.append(div);

            });

            $(self.infoTable).parent().append(menuDiv);
            $(self.infoTable).parent().append(divContainer1);

            self.api.loadPlayerGraphs(self.playerId, availableGraphTypes[0],function(res) {
                var data = res.data.players[0].graph;
                var min = res.data.players[0].graphMin;
                self.makePlayerGraph(divContainer1.attr("id"), data, min);
            });
        };

        self.makePlayerGraph = function(target, plots, min) {
            var ystyle='%.0f';
            var colors =  ["#ffffff","gold","#ccc"];
            $("#"+target).html("");
            if(plots[0].length === 0) {
                $("#"+target).html("No data available yet");
                return;
            }
            var yMin = 0.01;
            var c = min.toString();
            var cd = c[c.length-1];
            if (min !== null && min != 'undefined' && min >= 6) {
                if (cd == 1)
                    yMin = Number((min - 0.50).toFixed(2));
                else
                    yMin = Number((min - 0.51).toFixed(2));
            } else {
                if (min < 5.9) {
                    if (cd == 1)
                        yMin = Number((min - 0.02).toFixed(2));
                    else
                        yMin = Number((min - 0.01).toFixed(2));
                }
            }
            var plot1 = $.jqplot (target, plots,{
                grid:{
                    background:"transparent",
                    borderColor:"#6c9922",
                    gridLineColor:"#6c9922",
                    shadow:false
                },
                axes: {
                    xaxis: {
                        tickOptions:{formatString:'%.00f'},
                        pad: 0,
                        min: 1,
                        max: plots[0].length+1,
                        label: "",
                        tickInterval:1
                    },
                    yaxis: {
                        min: yMin,
                    },
                },
                series:[{
                    lineWidth:3,
                    markerOptions: {size:2,style:"circle"}
                }],
                seriesDefaults:{
                    showMarker:true,
                    lineWidth:3,
                    markerOptions: {size:4,style:"circle"},
                    pointLabels:{ show:true,"location": "n",edgeTolerance:-20,}
                },
                seriesColors:colors
            });
            // add Labels
            $("#"+target+" .jqplot-point-label").hide();
            $("#"+target+" .jqplot-point-label").css("background","#333");
            $("#"+target+" .jqplot-point-label").css("padding","3px");
            previousPoint = null;
            $('#'+target).bind('jqplotDataMouseOver', function (ev, seriesIndex, pointIndex, data) {
                var labels = $(this).find('.jqplot-point-label');
                labels.hide();
                // var point = labels.length-pointIndex;
                $(this).find(".jqplot-point-"+pointIndex).show();
            });
        };

        self.showSkillDiffernce = function() {
            var skillArray = self.remoteData.players[0].record.skills;
            var infoArray = self.remoteData.players[0].record.infos;
            var tableData = self.playerTable.getElementsByTagName("td");
            var playerPosition = $('div.box_sub_header div.large span.small > strong').text();

            if (tableData.length > 1) {
                for (var j = 0; j < skillNames.length; j += 1) {
                    //var skillName = skillNames[j];
                    var skillName = "";
                    var skillValue = null;
                    if (playerPosition != "Goalkeeper") {
                        skillName = skillNames()[j];
                        skillValue = skillArray[skillName];
                    } else {
                        skillName = skillNamesGoalKeeper[j];
                        skillValue = skillArray[skillName];
                    }

                    if (skillName !== "" && skillValue !== null && skillValue != "undefined") {

                        var sml = document.createElement("span");
                        $(sml).css("float","left");
                        $(sml).css("font-size","9px");
                        $(sml).css("text-align","center");


                        if (skillName.indexOf("rr_") === -1 && skillName.indexOf("r2_") === -1) {
                            $(sml).css("margin-left","-1px");
                        } else {
                            $(sml).css("width","100%");
                        }
                        var starImg = "";
                        if (skillValue == 20 || skillValue == "20") {
                            starImg = document.createElement("img");
                            $(starImg).attr("title", 20);
                            $(starImg).attr("alt", 20);
                            $(starImg).attr("src","/pics/star.png");
                            $(sml).append(starImg);

                        } else if (skillValue == 19 || skillValue == "19") {
                            starImg = document.createElement("img");
                            $(starImg).attr("title", 19);
                            $(starImg).attr("alt", 19);
                            $(starImg).attr("src","/pics/star_silver.png");
                            $(sml).append(starImg);

                        } else {

                            $(sml).append("<i>"+skillValue+"</i>");

                        }
                        var skillClass = "";
                        var currentValue = 0;
                        if (tableData[j] && tableData[j].innerHTML.indexOf("star.png") > 0) {
                            currentValue = 20;
                        }
                        else if (tableData[j] && tableData[j].innerHTML.indexOf("star_silver.png") > 0) {
                            currentValue = 19;
                        }
                        else if (tableData[j] && tableData[j].textContent.length !== 0) {
                            currentValue = parseFloat(tableData[j].textContent);
                        }

                        var lastValue = parseFloat(skillValue);
                        var wrapEle = $('<div></div>');
                        //wrapEle.css("border","1px solid #fff");
                        wrapEle.css("display","inline-block");
                        //wrapEle.css("font-size","14px");
                        //wrapEle.css("font-weight","bold");
                        wrapEle.attr("title", lastValue);

                        if (currentValue == 19 || currentValue == 20) {
                            var img = $(tableData[j]).find('img')[0];
                            if (img)
                                $(img).attr("title", lastValue);
                        }

                        if (currentValue > lastValue) {
                            wrapEle.css("background","linear-gradient(135deg, #007700 0%, #007000 44%, #005500 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");       
                            $(tableData[j]).wrapInner( wrapEle );
                        } else if (currentValue < lastValue) {
                            wrapEle.css("background","linear-gradient(135deg, #990000 0%, #8f0222 44%, #6d0019 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");
                            $(tableData[j]).wrapInner( wrapEle );
                        } else {
                            $(tableData[j]).attr("title", lastValue);
                        }

                    }

                }

                $('#player_last_save').html(self.remoteData.players[0].date);
                if (self.remoteData.players[0].saveRequired === true) {
                    if (true || self.remoteData.players[0].saveCurrentSi != self.localData.INFOS['skill index'].si) {
                        log("SI CHANGED - SAVE NEEDED");
                        log("old si: "+infoArray['skill index'].si+"/ new si: "+self.localData.INFOS['skill index'].si);
                        $(self.saveButton).show();
                    } else {
                        $(self.saveButton).hide();
                    }
                } else {
                    $(self.saveButton).hide();
                }
            }

            //Infos
            var tableData2 = self.infoTable.getElementsByTagName("td");
            if (tableData2.length > 1) {
                for (var jj = 1; jj < tableData2.length; jj += 1) {

                    var infoName = infoNames[jj];
                    if (infoName != "club" && infoName != "clubId" && infoName != "height/weight" && infoName != "age") {

                        var lastValue2 = infoArray[infoName];
                        var wrapEle2 = $('<div></div>');
                        wrapEle2.css("display","inline-block");

                        if (typeof lastValue2 === "object" || $.isArray(lastValue2)) {
                            lastValue2 = JSON.stringify(lastValue2);
                        }
                        wrapEle2.attr("title", lastValue2 );

                        $(tableData2[jj]).wrapInner( wrapEle2 );

                    }
                }
            }
        };
    }

    /*
    * TMTACTICS
    *
    */

    function tmtactics(_api) {
        var self = this;
        self.api = _api;

        self.init = function(player) {
            log("####### INIT Tactics #######");
            tmutils.delay(function() {
                self.initDoubleClickListener();
                self.initTacticsChangedListener();
                self.initPlayerSelldata();
                self.initStatsBar();
                self.loadPlayerStatsTable(function() {               
                });
            },300);
        };

        var loadPlayerTimeout = null;
        var loadPlayerId = null;
        var fieldPlayersStats = {"totalR2": 0,"totalRR": 0,"totalAge": 0,"totalRoutine": 0,"playingR2": 0,"playingRR": 0,"playingAge": 0,"count": 0,"playingCount": 0,};
        var lastLastSaved = "";
        var positionMappingStats = {"gk": "gk","dl": "dlr","dr": "dlr","dc": "dc","dcl": "dc","dcr": "dc","dml": "dmlr","dmr": "dmlr","dmc": "dmc","dmcl": "dmc","dmcr": "dmc","ml": "mlr","mr": "mlr","mc": "mc","mcl": "mc","mcr": "mc","oml": "omlr","omr": "omlr","omc": "omc","omcl": "omc","omcr": "omc","fc": "fc","fcl": "fc","fcr": "fc",};
        var pinfos = {
            tactics : {
                direct: ["Pac", "Sta", "Pas", "Pos", "Wor"],
                wings: ["Pac", "Tec", "Cro", "Wor", "Sta", "Str"],
                shortpassing: ["Pas", "Tec", "Wor", "Pac", "Pos", "Sta"],
                longpassing: ["Pas", "Cro", "Tec", "Pos", "Wor", "Sta"],
                throughballs: ["Pas", "Tec", "Pac", "Wor", "Pos", "Sta", "Cro"],
            }
        };

        self.initDoubleClickListener = function() {
            $('.field_player').dblclick(function() {
                window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
            });
            $('.sub_player').closest("li").dblclick(function() {
                window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
            });
            $('.player_name').dblclick(function() {
                window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
            });
        };

        self.initPlayerSelldata = function() {
            var cid = null;
            if (location.href.indexOf("/tactics/reserves") != -1) {
                cid = SESSION.b_team;
            } else {
                cid = SESSION.main_id;
            }
            self.api.loadClubSelldata(cid,[],function(res) {
                var resCntMax = res.data.length;
                var resCnt = 0;
                $(res.data).each(function() {
                    var resO = this;


                    if (resO.sellData.potential && resO.sellData.potential !== "") {
                        var fPot = parseFloat(resO.sellData.potential.replace(",",".")).toFixed(1);
                        var fstat = "";
                        if (resO.sellData.status !== null && resO.sellData.status !== undefined && resO.sellData.status != -1 && resO.sellData.status != "b")
                            fstat = " "+resO.sellData.status.toUpperCase();
                        var pl = $('span.player_name[player_id="'+resO.playerId+'"]');
                        $(pl).append(" <span class='player_name_extension'>["+fPot+fstat+"]</span>");
                        var pl2 = $('div.field_player[player_id="'+resO.playerId+'"] div.field_player_name');
                        $(pl2).append(" <span class='player_name_extension'>["+fPot+fstat+"]</span>");
                        var pl3 = $('li.bench_player[player_id="'+resO.playerId+'"] div.bench_player_name');
                        var pl3text = $(pl3).text();
                        var pl3tp = pl3text.split(" ");
                        pl3text = pl3tp[1];
                        pl3text += " <span class='player_name_extension'>["+fPot+fstat+"]</span>";
                        $(pl3).html(pl3text);
                        var lreport = resO.sellData.date;
                        var lage = -1;
                        if (resO.age !== null) {
                            var lageParts = resO.age.split(" Years");
                            lage = lageParts[0];
                            lage = parseInt(lage);
                        }
                        if (lage <= 0 || lage <= 26) {
                            if (lreport !== null && lreport !== undefined && lreport > 0) {
                                var tdate = new Date(lreport * 1000);
                                var tdateDiff = Date.now() - tdate.getTime();
                                //var days = (14 * 24 * 60 * 60 * 1000);
                                var days2 = (21 * 24 * 60 * 60 * 1000);
                                /*if (tdateDiff > days) {
                                    $(pl).find('.player_name_extension').css("color","#FFFF99");
                                    $(pl2).find('.player_name_extension').css("color","#FFFF99");
                                    $(pl3).find('.player_name_extension').css("color","#FFFF99");
                                }*/
                                if (tdateDiff > days2 && lage < 25.0) {
                                    $(pl).find('.player_name_extension').css("color","#FF8A8A");
                                    $(pl2).find('.player_name_extension').css("color","#FF8A8A");
                                    $(pl3).find('.player_name_extension').css("color","#FF8A8A");
                                }


                            } else {
                                $(pl).find('.player_name_extension').css("color","#FF8A8A");
                                $(pl2).find('.player_name_extension').css("color","#FF8A8A");
                                $(pl3).find('.player_name_extension').css("color","#FF8A8A");
                            }
                        }
                    }

                    if (resO.r2 && resO.r2 !== "") {
                        //console.log(resO);
                        var pl9 = $('div.field_player[player_id="'+resO.playerId+'"] div.field_player_name');
                        var r2insert = $("<div/>");
                        $(r2insert).attr("style","margin-top:0px;font-size:10px;line-height:10px;font-weight:bold");
                        $(r2insert).html(Math.max(...resO.r2));
                        if(Math.max(...resO.r2Diff) >= 0) {
                            $(r2insert).css("color","#68f568");
                        } else {
                            $(r2insert).css("color","#f7b5b5");
                        }
                        $(pl9).prepend(r2insert);

                        $('div.field_player').css("margin-top","-10px");
                    }
                });
            });
        };

        /*self.chkInts = [];
        self.lastHoverPlayer = null;
        self.initHoverListener = function(selector) {
            if (selector === null || selector === undefined || selector === "")
                selector = '#tactics_list_list ul li span.player_name,.field_player, .bench_player';
            var chkIds = 0;
            tmutils.delay(function() {
                //delay load, because tm page needs some processing time
                self.loadPlayerStatsTable(function() {
                    log("init hover listener",1);
                    log("on: "+selector,2);

                    $(selector).on('hover mouseover',function() {
                        //log("hover start",2);
                        var playermId = $(this).attr("player_id");
                        if (playermId === null || playermId === undefined) {
                            playermId = $(this).attr("player_link");
                            if (playermId === null || playermId === undefined) {
                                var tmp = $(this).attr("href");
                                if (tmp.indexOf("/players/") !== -1) {
                                    playermId = parseInt(tmp.replace("/players/",""));
                                }
                                if (playermId === null || playermId === undefined)
                                    return;
                            }
                        }
                        if (playermId == self.lastHoverPlayer)
                            return;
                        self.lastHoverPlayer = playermId;

                        tmutils.waitForTooltip(function(tablet) {
                            if (tablet === null) {
                                log("Tooltip aborted",2);
                            } else {
                                log("Tooltip loaded",2);
                                var ttbody = $(tablet).find("tbody");
                                $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row").remove();
                                $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row2").remove();
                                var rr2 = new playerrr(self.api).getDataTooltip($('#tooltip'));
                                var r2 = rr2.r2;
                                if(r2.indexOf("/") > -1) {
                                    var r2p = r2.split("/");
                                    r2 = Math.max.apply(Math, r2p);
                                }
                                r2 = parseFloat(r2).toFixed(3);
                                var rr = Math.max.apply(Math, rr2.rerec2);
                                rr = parseFloat(rr).toFixed(3);
                                var app = '<tr id="playerSaver_rr_r2_row" style="color:gold;"><td colspan="1"></td><td class="align_left">RRec</td><td class="align_left"><b>'+rr+'</b></td></tr>';
                                app += '<tr id="playerSaver_rr_r2_row2" style="color:gold;"><td colspan="1"></td><td class="align_left">RatingR2</td><td class="align_left"><b>'+r2+'</b></td></tr>';
                                $(ttbody).append(app);
                                tmutils.cache.getPlayerCurrentRecord(self.api,playermId,function(res) {
                                    if (res === null) {
                                        log("no playerrecord found",2);
                                    }
                                    if (res != null && res.record.sellData !== null && res.record.sellData !== undefined) {
                                        var papp = '';
                                        var pot = res.record.sellData.potential;
                                        if (pot !== null && pot !== undefined)
                                            papp += '<tr id="playerSaver_rr_r2_row2" style="color:white;"><td colspan="1"></td><td class="align_left">Pot</td><td class="align_left"><b>'+pot+'</b></td></tr>';
                                        $(ttbody).append(papp);
                                        //log("loaded tooltip extension",2);
                                    }
                                });
                            }

                        });


                        if (self.chkInts !== null && self.chkInts.length > 0) {
                            $.each(self.chkInts,function() {
                                clearInterval(this);
                            });
                            self.chkInts = [];
                        }
                        $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row").remove();
                        $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row2").remove();
                        if (loadPlayerTimeout !== null && playermId != loadPlayerId) {
                            window.clearTimeout(loadPlayerTimeout);
                            loadPlayerTimeout = null;
                        }
                        if (loadPlayerTimeout === null){
                            loadPlayerId = playermId;
                            loadPlayerTimeout = {};

                            var intCnt = 0;
                            var chkint = window.setInterval(function() {
                                if (intCnt === 0) {
                                    chkIds++;
                                    this.__id = chkIds;
                                }
                                intCnt++;
                                //log("checking tooltip " + this.__id,2);
                                var tablet = $("#tooltip .player_tooltip").find("table")[0];
                                if (tablet !== null) {
                                    var ttbody = $(tablet).find("tbody");
                                    if (ttbody !== null && $(ttbody).find("tr").length > 5) {
                                        loadPlayerTimeout = null;
                                        var rr2 = new playerrr(self.api).getDataTooltip($('#tooltip'));
                                        var r2 = rr2.r2;
                                        if(r2.indexOf("/") > -1) {
                                            var r2p = r2.split("/");
                                            r2 = Math.max.apply(Math, r2p);
                                        }
                                        r2 = parseFloat(r2).toFixed(3);
                                        var rr = Math.max.apply(Math, rr2.rerec2);
                                        rr = parseFloat(rr).toFixed(3);
                                        var app = '<tr id="playerSaver_rr_r2_row" style="color:gold;"><td colspan="1"></td><td class="align_left">RRec</td><td class="align_left"><b>'+rr+'</b></td></tr>';
                                        app += '<tr id="playerSaver_rr_r2_row2" style="color:gold;"><td colspan="1"></td><td class="align_left">RatingR2</td><td class="align_left"><b>'+r2+'</b></td></tr>';
                                        $(ttbody).append(app);
                                        tmutils.cache.getPlayerCurrentRecord(self.api,playermId,function(res) {
                                            if (res === null) {
                                                log("no playerrecord found",2);
                                            }
                                            if (res != null && res.record.sellData !== null && res.record.sellData !== undefined) {
                                                var papp = '';
                                                var pot = res.record.sellData.potential;
                                                if (pot !== null && pot !== undefined)
                                                    papp += '<tr id="playerSaver_rr_r2_row2" style="color:white;"><td colspan="1"></td><td class="align_left">Pot</td><td class="align_left"><b>'+pot+'</b></td></tr>';
                                                $(ttbody).append(papp);
                                                //log("loaded tooltip extension",2);
                                            }
                                        });
                                        if (chkint)
                                            clearInterval(chkint);
                                    }
                                }
                                if (intCnt > 40) {
                                    if (chkint)
                                            clearInterval(chkint);
                                }
                            },50);
                            self.chkInts.push(chkint);
                            //},1);
                        
                    });
                });
            },200);
        };}*/

        self.initTacticsChangedListener = function() {
            lastLastSaved = $('#tactics_last_save').html();
            var checkLastSaveInterval = setInterval(function(){
                var lls = $('#tactics_last_save').html();
                if (lastLastSaved != lls) {
                    lastLastSaved = lls;
                    self.loadPlayerStatsTable();
                    self.initDoubleClickListener();
                    log("tactics changed");
                }
            },600);
        };

        self.initStatsBar = function() {
            var statsDiv = $('<div class="odd align_center" style="line-height: 35px;margin-bottom:5px"></div>');
            var statsTable = $('<table></table>');
            var statsRow = $('<tr></tr>');
            $(statsRow).append("<td><b>Avg. R2:</b></td><td><b><span id='psstats_avgr2' style='font-size:1.1em'></span></b></td>");
            $(statsRow).append("<td><b>Avg. RR:</b></td><td><b><span id='psstats_avgrr' style='font-size:1.1em'></span></b></td>");
            $(statsRow).append("<td><b>Avg. Age:</b></td><td><b><span id='psstats_avgage' style='font-size:1.1em'></span></b></td>");
            $(statsRow).append("<td><b>Avg. Rout.:</b></td><td><b><span id='psstats_avgrout' style='font-size:1.1em'></span></b></td>");
            $(statsTable).append(statsRow);
            $(statsDiv).append(statsTable);
            $('#tactics').after(statsDiv);
        };

        self.loadPlayerStatsTable = function(callback) {
            fieldPlayersStats.totalR2 = 0;
            fieldPlayersStats.totalRR = 0;
            fieldPlayersStats.totalAge = 0;
            fieldPlayersStats.totalRoutine = 0;
            fieldPlayersStats.playingR2 = 0;
            fieldPlayersStats.playingRR = 0;
            fieldPlayersStats.playingCount = 0;
            fieldPlayersStats.count = 0;
            var fplayers = $('#tactics_field div.field_player[player_set="true"]');
            var updateInterval = window.setInterval(function() {
                if (fieldPlayersStats.count == $(fplayers).length) {
                    log("finished loading player stats");
                    //log(fieldPlayersStats);
                    if (fieldPlayersStats.count == fieldPlayersStats.playingCount) {
                        $('#psstats_avgr2').html((fieldPlayersStats.playingR2/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalR2/fieldPlayersStats.count).toFixed(2)).css("color","gold");
                        $('#psstats_avgrr').html((fieldPlayersStats.playingRR/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalRR/fieldPlayersStats.count).toFixed(2)).css("color","gold");
                        $('#psstats_avgage').html((fieldPlayersStats.totalAge/fieldPlayersStats.count).toFixed(2)).css("color","gold");
                        $('#psstats_avgrout').html((fieldPlayersStats.totalRoutine/fieldPlayersStats.count).toFixed(2)).css("color","gold");
                    } else {
                        $('#psstats_avgr2').html((fieldPlayersStats.playingR2/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalR2/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
                        $('#psstats_avgrr').html((fieldPlayersStats.playingRR/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalRR/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
                        $('#psstats_avgage').html((fieldPlayersStats.totalAge/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
                        $('#psstats_avgrout').html((fieldPlayersStats.totalRoutine/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
                    }
                    clearInterval(updateInterval);
                    if (callback)
                        callback();
                }
            },100);
            $(fplayers).each(function() {
                var playerId = $(this).attr("player_id");
                var playerSet = $(this).attr("player_set");
                var playerStatus = $(this).find(".status");
                var playerMood = $(this).find(".mood");
                var playerPosition = $(this).attr("position");

                if (playerId !== null && playerId !== undefined &&
                    playerSet !== null && playerSet !== undefined && playerSet == "true") {

                    tmutils.cache.getPlayerCurrentRecord(self.api,playerId,function(_res) {
                        if (_res == null)
                            return;

                        var res = _res.record;
                        var r2 = res.infos.routine.r2;
                        var rr = res.infos.recommandation;
                        var rou = res.infos.routine.routine;
                        if (r2.indexOf("/") != -1) {
                            var r2p = r2.split("/");
                            var r2pa = parseFloat(r2p[0]);
                            var r2pb = parseFloat(r2p[1]);
                            if(r2pa > r2pb)
                                r2 = r2pa;
                            else
                                r2 = r2pb;
                        }
                        if (playerPosition !== null && playerPosition !== undefined && playerPosition != "gk") {
                            if (positionMappingStats[playerPosition] !== null && positionMappingStats[playerPosition] !== undefined) {
                                var ppms = "r2_"+positionMappingStats[playerPosition];
                                if (res.skills[ppms] !== null && res.skills[ppms] !== undefined) {
                                    r2 = res.skills[ppms];
                                } else {
                                    log(ppms+" not found");
                                }
                            } else {
                                log(playerPosition+" not found");
                            }
                        }
                        var ageo = res.infos.age.toLowerCase();
                        var agep = ageo.split(" years ");
                        var agem = agep[1].split(" months");
                        var age = parseFloat(agep[0]) + parseFloat(agem[0]/12);
                        fieldPlayersStats.totalR2 += parseFloat(r2);
                        fieldPlayersStats.totalRR += parseFloat(rr);
                        fieldPlayersStats.totalAge += parseFloat(age);
                        fieldPlayersStats.totalRoutine += parseFloat(rou);
                        fieldPlayersStats.count++;
                        var isPlaying = false;
                        if (playerStatus === null || playerStatus === undefined || playerStatus.length === 0) {
                            isPlaying = true;
                        } else {
                            var status = playerStatus[0];
                            var yellow = $(status).find("img")[0];
                            var ysrc = $(yellow).attr("src");
                            if (ysrc.indexOf("yellow_card") > -1 || ysrc.indexOf("status_unknown") > -1) {
                                isPlaying = true;
                            }
                        }
                        if (isPlaying) {
                            fieldPlayersStats.playingCount++;
                        }
                        var isOffposition = false;
                        if (playerMood !== null && playerMood !== undefined && playerMood.length > 0) {
                            isOffposition = true;
                        }
                        if (isOffposition) {
                            var malus = 1.0;
                            if ($(playerMood).hasClass("mood1"))
                                malus = 0.95;
                            if ($(playerMood).hasClass("mood2"))
                                malus = 0.90;
                            if ($(playerMood).hasClass("mood3"))
                                malus = 0.85;
                            if ($(playerMood).hasClass("mood4"))
                                malus = 0.80;
                            if ($(playerMood).hasClass("mood5"))
                                malus = 0.75;
                            if ($(playerMood).hasClass("mood6"))
                                malus = 0.70;
                            fieldPlayersStats.playingR2 += (parseFloat(r2) * malus);
                            fieldPlayersStats.playingRR += (parseFloat(rr) * malus);
                            fieldPlayersStats.playingAge += parseFloat(age);
                        } else {
                            fieldPlayersStats.playingR2 += parseFloat(r2);
                            fieldPlayersStats.playingRR += parseFloat(rr);
                            fieldPlayersStats.playingAge += parseFloat(age);
                        }
                    });

                }
            });
        };
    }

    function playerrr(_api) {
        var self = this;
        self.api = _api;

        self.getSkillsPlayer = function(tmplayer, positionArray) {
            var skills = [];
            var psk = tmplayer.remoteData.players[0].record.skills;
            var fpsarr = ["strength","stamina","pace","marking","tackling","workrate","positioning","passing","crossing","technique","heading","finishing","longshots","set pieces"];
            if (positionArray[0] == "Goalkeeper")
                fpsarr = ["strength","stamina","pace","handling","one on ones","reflexes","aerial ability","jumping","communication","kicking","throwing"];
            $.each(fpsarr,function() {
                var skill = psk[this];
                skills.push(skill);
            });
            return skills;
        };

        self.getSkillsTable = function(table) {
            var skillArray = [];
            var tableData = table.getElementsByTagName("td");
            if (tableData.length > 1) {
                for (var i = 0; i < 2; i++) {
                    for (var j = i; j < tableData.length; j += 2) {
                        if (tableData[j].innerHTML.indexOf("star.png") > 0) {
                            skillArray.push(20);
                        }
                        else if (tableData[j].innerHTML.indexOf("star_silver.png") > 0) {
                            skillArray.push(19);
                        }
                        else if (tableData[j].textContent.length !== 0) {
                            if (tableData[j].textContent.indexOf(".") ==  -1) {
                                var skillvalue = parseInt(tableData[j].textContent);
                                if (skillvalue != null && skillvalue > -1)
                                    skillArray.push(skillvalue);
                            }
                        }
                    }
                }
            }
            return skillArray;
        };

        self.getSkillsTooltip = function(table) {
            var skillArray = [];
            $.each($(table).find("tr"),function(i, e) {
                var tds = $(e).find("td");
                var val1 = tds[1];
                var val2 = tds[4];
                $.each([val1,val2], function(j, y) {
                    var val = 0;
                    if ($(y).html().indexOf("star.png") > 0) {
                        val = 20;
                    }
                    else if ($(y).html().indexOf("star_silver.png") > 0) {
                        val = 19;
                    }
                    else if ($(y).html().length !== 0) {
                        if(Number.isInteger(parseInt($(y).html())))
                           val = $(y).html();
                    }
                    if (val > 0) {
                        if (j === 0) {
                            skillArray[i] = val;
                        } else {
                            skillArray[(i+7)] = val;
                        }
                    }
                });
            });
            skillArray = skillArray.filter(function(n){ return n !== undefined; });
            return skillArray;
        };

        self.getDataTable = function(table) {
            var positionCell = document.getElementsByClassName("favposition long")[0].childNodes;
            var positionArray = [];
            if (positionCell.length == 1){
                positionArray[0] = positionCell[0].textContent;
            } else if (positionCell.length == 2){
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
            } else if (positionCell[1].className == "split"){
                positionArray[0] = positionCell[0].textContent + positionCell[3].textContent;
                positionArray[1] = positionCell[2].textContent + positionCell[3].textContent;
            } else if (positionCell[3].className == "f"){
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
                positionArray[1] = positionCell[3].textContent;
            } else {
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
                positionArray[1] = positionCell[0].textContent + positionCell[3].textContent;
            }
            var skills = self.getSkillsTable(table);
            var gettr = $("div.box_body tr");
            var sitd = gettr[6].getElementsByTagName("td")[0].innerHTML;
            var SI = new String(sitd).split("<div")[0].replace(/,/g, "");
            var rou = gettr[8].getElementsByTagName("td")[0].innerHTML;
            rou = new String(rou).split("<div")[0].replace(/,/g, "");
            rou = Math.pow(5/3, Math.LOG2E * Math.log(rou * 10)) * 0.4;
            log("RR SKills from table",2);
            return self._getData(positionArray,skills,SI,rou);
        };

        self.getDataTooltip = function(tooltip) {
            var tableInfos = $(tooltip).find("table")[0];
            var tableSkills = $(tooltip).find("table")[1];

            var positionCell = $(tooltip).find("span.favposition")[0].childNodes;
            var positionArray = [];
            if (positionCell.length == 1){
                positionArray[0] = positionCell[0].textContent;
            } else if (positionCell.length == 2){
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
            } else if (positionCell[1].className == "split"){
                positionArray[0] = positionCell[0].textContent + positionCell[3].textContent;
                positionArray[1] = positionCell[2].textContent + positionCell[3].textContent;
            } else if (positionCell[3].className == "f"){
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
                positionArray[1] = positionCell[3].textContent;
            } else {
                positionArray[0] = positionCell[0].textContent + positionCell[1].textContent;
                positionArray[1] = positionCell[0].textContent + positionCell[3].textContent;
            }

            var skills = self.getSkillsTooltip(tableSkills);
            var gettr = $(tableInfos).find("tr");
            var sitd = $(gettr[6]).find("td")[1].innerHTML;
            var SI = new String(sitd).split("<div")[0].replace(/,/g, "");
            var rou = $(gettr[4]).find("td")[1].innerHTML;
            rou = new String(rou).split("<div")[0].replace(/,/g, "");
            rou = Math.pow(5/3, Math.LOG2E * Math.log(rou * 10)) * 0.4;
            log("RR SKills from tooltip",2);
            return self._getData(positionArray,skills,SI,rou);
        };

        self.getDataPlayer = function(tmplayer) {
            var positionArray = tmplayer.remoteData.players[0].record.position.split("/");
            var skills = self.getSkillsPlayer(tmplayer, positionArray);
            var SI = parseInt(tmplayer.remoteData.players[0].saveCurrentSi.replace(",",""));
            var rou = parseFloat(tmplayer.remoteData.players[0].record.infos.routine.routine);
            rou = Math.pow(5/3, Math.LOG2E * Math.log(rou * 10)) * 0.4;
            log("RR SKills from tmplayer",2);
            return self._getData(positionArray,skills,SI,rou);
        };

        self._getData = function(positionArray,skills,SI,rou) {            
            var logd = {
                positionArray: positionArray,
                skills: skills,
                SI: SI,
                rou: rou
            };
            log(logd,2);
            var res = { };
            var REREC = [[],[],[]];
            var REREC2 = [];
            var FP = [];
            for (var i = 0; i < positionArray.length; i++){
                var positionIndex = self.findPositionIndex(positionArray[i]);
                FP[i] = positionIndex;
                FP[i+1] = FP[i];
                if (positionIndex > -1) {
                    REREC2[i] = self.calculateREREC2(positionIndex, skills, SI);
                }
                if (i === 0) REREC = self.calculateREREC(positionIndex, skills, SI, rou);
            }
            //console.log(REREC);
            res.rerec2 = REREC2;
            var FP2 = [FP[0], FP[1]];
            for (i = 0; i < FP.length; i++) {
                for (j = 0; 2+j <= FP[i]; j += 2) FP[i]--;
            }
            if (FP[0] != FP[1]) {
                res.r2 = REREC[2][FP[0]] + "/" + REREC[2][FP[1]];
                res.r3 = REREC[3][FP[0]] + "/" + REREC[3][FP[1]];
            }
            else {
                res.r2 = REREC[2][FP[0]];
                res.r3 = REREC[3][FP[0]];
            }

            log("Calculating Player RR SKills",2);
            log(res, 2);
            return res;
        };

        // RatingR2 beta
        // 4.6.1
        // REREC(+b), Season TI, TrExMa, RatingR2
        self.calculateREREC2 = function (positionIndex, skills, SI){
            var weight = 263533760000;
            if (positionIndex == 13) weight = 48717927500;
            var skillSum = 0;
            for (var j = 0; j < skills.length; j++) {
                skillSum += parseInt(skills[j]);
            }
            var remainder = Math.round((Math.pow(2,Math.log(weight*SI)/Math.log(Math.pow(2,7))) - skillSum)*10)/10;     // 正確な余り
            var recb = 0;
            var weightSumb = 0;
            var not20 = 0;

            for (i = 0; 2+i <= positionIndex; i += 2) {     // TrExMaとRECのweight表のずれ修正
                positionIndex--;
            }
            for (var i = 0; i < weightR[positionIndex].length; i++) {
                recb += skills[i] * weightRb[positionIndex][i];
                if (skills[i] != 20) {
                    weightSumb += weightRb[positionIndex][i];
                    not20++;
                }
            }
            if (not20 === 0) recb = 6;      // All MAX
            else recb = (recb + remainder * weightSumb / not20 - 2) / 3;

            return recb;
        };

        self.calculateREREC = function (positionIndex, skills, SI, rou){
            var rec = [];           // REREC
            var ratingR = [];       // RatingR2
            var ratingR2 = [];      // RatingR2 + routine
            var ratingR3 = [];      // RatingR2 + new routine
            var skillSum = 0;
            var skillWeightSum = 0;
            var weight = 0;
            var addrou = (3/100)*(100-(100)*Math.exp(-rou*0.035));
            //console.log(skills);
            //console.log(addrou);

            if (positionIndex == 13) {
                skillWeightSum = Math.pow(SI, 0.143) / 0.02979;         // GK Skillsum
                weight = 48717927500;
            }
            else {
                skillWeightSum = Math.pow(SI, 1/6.99194)/0.02336483;    // Other Skillsum
                weight = 263533760000;
            }
            for (var i = 0; i < skills.length; i++) {
                skillSum += parseInt(skills[i]);
            }
            for (i = 0; 2+i <= positionIndex; i += 2) {     // TrExMaとRECのweight表のずれ修正
                positionIndex--;
            }
            skillWeightSum -= skillSum;         // REREC remainder
            var remainder = Math.round((Math.pow(2,Math.log(weight*SI)/Math.log(Math.pow(2,7))) - skillSum)*10)/10;     // RatingR2 remainder
            for (var i = 0; i < 10; i++) {
                rec[i] = 0;
                ratingR[i] = 0;
                ratingR3[i] = 0;
            }
            for (var j = 0; j < 9; j++) {       // All position
                var remainderWeight = 0;        // REREC remainder weight sum
                var remainderWeight2 = 0;       // RatingR2 remainder weight sum
                var not20 = 0;                  // 20以外のスキル数
                if (positionIndex == 9) j = 9;  // GK

                for (var i = 0; i < weightR[positionIndex].length; i++) {
                    rec[j] += skills[i] * weightR[j][i];
                    ratingR[j] += skills[i] * weightR2[j][i];
                    ratingR3[j] += (parseInt(skills[i]) + addrou) * weightR2[j][i];
                    if (skills[i] != 20) {
                        remainderWeight += weightR[j][i];
                        remainderWeight2 += weightR2[j][i];
                        not20 += 1;
                    }
                }
                rec[j] += skillWeightSum * remainderWeight / not20;     //REREC Score
                if (positionIndex == 9) rec[j] *= 1.27;                 //GK
                rec[j] = funFix((rec[j] - recLast[0][j]) / recLast[1][j]);
                
                ratingR[j] += remainder * remainderWeight2 / not20;
                ratingR3[j] += remainder * remainderWeight2 / not20;
                
                ratingR2[j] = funFix(ratingR[j] * (1 + rou * rou_factor));
                ratingR[j] = funFix(ratingR[j]);
                ratingR3[j] = funFix(ratingR3[j]);

                if (positionIndex == 9) j = 9;      // Loop end
            }

            var recAndRating = [rec, ratingR, ratingR2, ratingR3];
            return recAndRating;
        };

        self.findPositionIndex = function(position) {
            var index = -1;
            for (var i=0; i< positionFullNames.length; i++) {
                for (var j=0; j< positionFullNames[i].length; j++) {
                    if (position.indexOf(positionFullNames[i][j]) === 0) return j;
                }
            }
            return index;
        };

        var rou_factor = 0.00405;
        var wage_rate = 19.76;
        // Array to setup the weights of particular skills for each player's actual ability
        // This is the direct weight to be given to each skill.
        // Array maps to these skills:
        //               [Str,Sta,Pac,Mar,Tac,Wor,Pos,Pas,Cro,Tec,Hea,Fin,Lon,Set]
        var positions = [[  1,  3,  1,  1,  1,  3,  3,  2,  2,  2,  1,  3,  3,  3], // D C
                         [  2,  3,  1,  1,  1,  3,  3,  2,  2,  2,  2,  3,  3,  3], // D L
                         [  2,  3,  1,  1,  1,  3,  3,  2,  2,  2,  2,  3,  3,  3], // D R
                         [  1,  2,  2,  1,  1,  1,  1,  1,  2,  2,  1,  3,  3,  3], // DM C
                         [  2,  3,  1,  1,  1,  3,  3,  2,  2,  2,  2,  3,  3,  3], // DM L
                         [  2,  3,  1,  1,  1,  3,  3,  2,  2,  2,  2,  3,  3,  3], // DM R
                         [  2,  2,  3,  1,  1,  1,  1,  1,  3,  1,  2,  3,  3,  3], // M C
                         [  2,  2,  1,  1,  1,  1,  1,  1,  1,  1,  2,  3,  3,  3], // M L
                         [  2,  2,  1,  1,  1,  1,  1,  1,  1,  1,  2,  3,  3,  3], // M R
                         [  2,  3,  3,  2,  2,  1,  1,  1,  3,  1,  2,  1,  1,  3], // OM C
                         [  2,  2,  1,  3,  3,  2,  2,  3,  1,  1,  2,  2,  2,  3], // OM L
                         [  2,  2,  1,  3,  3,  2,  2,  3,  1,  1,  2,  2,  2,  3], // OM R
                         [  1,  2,  2,  3,  3,  2,  2,  3,  3,  2,  1,  1,  1,  3], // F
                         [  2,  3,  2,  1,  2,  1,  2,  2,  3,  3,  3]]; // GK

        // Weights need to total 100
        var weights = [ [85,12, 3],  // D C
                       [70,25, 5],  // D L
                       [70,25, 5],  // D R
                       [90,10, 0],  // DM C
                       [50,40,10],  // DM L
                       [50,40,10],  // DM R
                       [85,12, 3],  // M C
                       [90, 7, 3],  // M L
                       [90, 7, 3],  // M R
                       [90,10, 0],  // OM C
                       [60,35, 5],  // OM  L
                       [60,35, 5],  // OMR
                       [80,18, 2],  // F
                       [50,42, 8]]; // GK

        var weightR2 = [[   0.51872935  ,   0.29081119  ,   0.57222393  ,   0.89735816  ,   0.84487852  ,   0.50887940  ,   0.50887940  ,   0.13637928  ,   0.05248024  ,   0.09388931  ,   0.57549122  ,   0.07310254  ,   0.02834753  ,   0.00000000  ],  // DC
                        [   0.46087883  ,   0.31034824  ,   0.65619359  ,   0.73200504  ,   0.70343948  ,   0.49831122  ,   0.46654859  ,   0.16635132  ,   0.22496087  ,   0.19697949  ,   0.48253326  ,   0.07310254  ,   0.02834753  ,   0.00000000  ],  // DL/R
                        [   0.43732502  ,   0.31888984  ,   0.53618097  ,   0.63897616  ,   0.59319466  ,   0.51330795  ,   0.53166961  ,   0.32536200  ,   0.06340582  ,   0.27886822  ,   0.49996910  ,   0.18940400  ,   0.07344664  ,   0.00000000  ],  // DMC
                        [   0.42233965  ,   0.32373447  ,   0.62437404  ,   0.54169665  ,   0.51669428  ,   0.49853202  ,   0.47851686  ,   0.26551219  ,   0.22685609  ,   0.32146118  ,   0.45396969  ,   0.23513340  ,   0.09117948  ,   0.00000000  ],  // DML/R
                        [   0.34304950  ,   0.35058989  ,   0.49918296  ,   0.34631352  ,   0.30595388  ,   0.52078076  ,   0.56068322  ,   0.52568923  ,   0.08771222  ,   0.47650463  ,   0.41232903  ,   0.41160135  ,   0.15960981  ,   0.00000000  ],  // MC
                        [   0.37404045  ,   0.33153172  ,   0.62642777  ,   0.33260815  ,   0.30559265  ,   0.50117998  ,   0.47502314  ,   0.28759565  ,   0.33838614  ,   0.44322386  ,   0.40347341  ,   0.41859521  ,   0.16232188  ,   0.00000000  ],  // ML/R
                        [   0.31998474  ,   0.35180968  ,   0.49002842  ,   0.23116817  ,   0.19239312  ,   0.52687030  ,   0.57839880  ,   0.53861416  ,   0.07598706  ,   0.56096162  ,   0.39614367  ,   0.53152625  ,   0.20611401  ,   0.00000000  ],  // OMC
                        [   0.36069138  ,   0.33248748  ,   0.62214126  ,   0.20034326  ,   0.17595073  ,   0.50091992  ,   0.47631079  ,   0.29235505  ,   0.35086625  ,   0.52960856  ,   0.39553712  ,   0.54964726  ,   0.21314094  ,   0.00000000  ],  // OML/R
                        [   0.40324698  ,   0.29906901  ,   0.39676419  ,   0.10106757  ,   0.07620466  ,   0.50471883  ,   0.58512049  ,   0.37506253  ,   0.05291339  ,   0.53882195  ,   0.51604535  ,   0.82935839  ,   0.32160667  ,   0.00000000  ],  // F
                        [   0.45462811  ,   0.30278232  ,   0.45462811  ,   0.90925623  ,   0.45462811  ,   0.90925623  ,   0.45462811  ,   0.45462811  ,   0.30278232  ,   0.15139116  ,   0.15139116  ]]; // GK

        // RECb weights     Str             Sta             Pac             Mar             Tac             Wor             Pos             Pas             Cro             Tec             Hea             Fin             Lon             Set
        var weightRb = [[   0.10476131  ,   0.05214691  ,   0.07928798  ,   0.14443775  ,   0.13140328  ,   0.06543399  ,   0.07762453  ,   0.06649973  ,   0.05174317  ,   0.02761713  ,   0.12122597  ,   0.01365182  ,   0.02547069  ,   0.03869574  ],  // DC
                        [   0.07660230  ,   0.05043295  ,   0.11528887  ,   0.11701021  ,   0.12737497  ,   0.07681385  ,   0.06343039  ,   0.03777422  ,   0.10320519  ,   0.06396543  ,   0.09155298  ,   0.01367035  ,   0.02554511  ,   0.03733318  ],  // DL/R
                        [   0.08289553  ,   0.08655174  ,   0.07386703  ,   0.09784055  ,   0.08807642  ,   0.09017265  ,   0.09391390  ,   0.08893449  ,   0.04707010  ,   0.05961795  ,   0.05373727  ,   0.05013894  ,   0.05768539  ,   0.02949804  ],  // DMC
                        [   0.06705156  ,   0.06600599  ,   0.10002073  ,   0.08249862  ,   0.09719526  ,   0.09243450  ,   0.08504033  ,   0.06129130  ,   0.10295145  ,   0.08088686  ,   0.04665721  ,   0.03841339  ,   0.05222570  ,   0.02732710  ],  // DML/R
                        [   0.07333243  ,   0.08171847  ,   0.07197804  ,   0.08469622  ,   0.07098103  ,   0.09554048  ,   0.09470328  ,   0.09576006  ,   0.04729121  ,   0.07092367  ,   0.04588383  ,   0.05986604  ,   0.07170498  ,   0.03562024  ],  // MC
                        [   0.06527363  ,   0.06410270  ,   0.09701305  ,   0.07406706  ,   0.08563595  ,   0.09648566  ,   0.08651209  ,   0.06357183  ,   0.10819222  ,   0.07386495  ,   0.03245554  ,   0.05430668  ,   0.06572005  ,   0.03279859  ],  // ML/R
                        [   0.07886961  ,   0.07955547  ,   0.07497831  ,   0.06915926  ,   0.05059290  ,   0.08160950  ,   0.08206952  ,   0.10911727  ,   0.03482457  ,   0.07593779  ,   0.06515279  ,   0.07472116  ,   0.09098089  ,   0.03243097  ],  // OMC
                        [   0.06545375  ,   0.06145378  ,   0.10503536  ,   0.06421508  ,   0.07627526  ,   0.09232981  ,   0.07763931  ,   0.07001035  ,   0.11307331  ,   0.07298351  ,   0.04248486  ,   0.06462713  ,   0.07038293  ,   0.02403557  ],  // OML/R
                        [   0.07739710  ,   0.05095200  ,   0.07641981  ,   0.01310784  ,   0.01149133  ,   0.06383764  ,   0.07762980  ,   0.07632566  ,   0.02708970  ,   0.07771063  ,   0.12775187  ,   0.15339719  ,   0.12843583  ,   0.03845360  ],  // F

                        [   0.07466384  ,   0.07466384  ,   0.07466384  ,   0.14932769  ,   0.10452938  ,   0.14932769  ,   0.10452938  ,   0.10344411  ,   0.07512610  ,   0.04492581  ,   0.04479831  ]]; // GK
        // 上書き文字化け注意

        // REC weights Str                 Sta                Pac                Mar                 Tac                 Wor                Pos                Pas                Cro                Tec                Hea                Fin                Lon                Set
        var weightR = [[0.653962303361921,  0.330014238020285, 0.562994547223387, 0.891800163983125,  0.871069095865164,  0.454514672470839, 0.555697278549252, 0.42777598627972,  0.338218821750765, 0.134348455965202, 0.796916786677566, 0.048831870932616, 0.116363443378865, 0.282347752982916],   //DC
                       [0.565605120229193,  0.430973382039533, 0.917125432457378, 0.815702528287723,  0.99022325015212,   0.547995876625372, 0.522203232914265, 0.309928898819518, 0.837365352274204, 0.483822472259513, 0.656901420858592, 0.137582588344562, 0.163658117596413, 0.303915447383549],   //DL/R
                       [0.55838825558912,   0.603683502357502, 0.563792314670998, 0.770425088563048,  0.641965853834719,  0.675495235675077, 0.683863478201805, 0.757342915150728, 0.473070797767482, 0.494107823556837, 0.397547163237438, 0.429660916538242, 0.56364174077388,  0.224791093448809],   //DMC
                       [0.582074038075056,  0.420032202680124, 0.7887541874616,   0.726221389774063,  0.722972329840151,  0.737617252827595, 0.62234458453736,  0.466946909655194, 0.814382915598981, 0.561877829393632, 0.367446981999576, 0.360623408340649, 0.390057769678583, 0.249517737311268],   //DML/R
                       [0.578431939417021,  0.778134685048085, 0.574726322388294, 0.71400292078636,   0.635403391007978,  0.822308254446722, 0.877857040588335, 0.864265671245476, 0.433450219618618, 0.697164252367046, 0.412568516841575, 0.586627586272733, 0.617905053049757, 0.308426814834866],   //MC
                       [0.497429376361348,  0.545347364699553, 0.788280917110089, 0.578724574327427,  0.663235306043286,  0.772537143243647, 0.638706135095199, 0.538453108494387, 0.887935381275257, 0.572515970409641, 0.290549550901104, 0.476180499897665, 0.526149424898544, 0.287001645266184],   //ML/R
                       [0.656437768926678,  0.617260722143117, 0.656569986958435, 0.63741054520629,   0.55148452726771,   0.922379789905246, 0.790553566121791, 0.999688557334153, 0.426203575603164, 0.778770912265944, 0.652374065121788, 0.662264393455567, 0.73120100926333,  0.274563618133769],   //OMC
                       [0.483341947292063,  0.494773052635464, 0.799434804259974, 0.628789194186491,  0.633847969631333,  0.681354437033551, 0.671233869875345, 0.536121458625519, 0.849389745477645, 0.684067723274814, 0.389732973354501, 0.499972692291964, 0.577231818355874, 0.272773352088982],   //OML/R
                       [0.493917051093473,  0.370423904816088, 0.532148929996192, 0.0629206658586336, 0.0904950078155216, 0.415494774080483, 0.54106107545574,  0.468181146095801, 0.158106484131194, 0.461125738338018, 0.83399612271067,  0.999828328674183, 0.827171977606305, 0.253225855459207],   //F
                       //              For  Rez    Vit  Ind  One  Ref Aer  Sar  Com    Deg    Aru
                       [0.5, 0.333, 0.5, 1,   0.5, 1,  0.5, 0.5, 0.333, 0.333, 0.333]]; //GK

        //              DC         DL/R       DMC         DML/R       MC          ML/R        OMC         OML/R       F           GK
        var recLast = [[14.866375, 15.980742, 15.8932675, 15.5835325, 17.6955092, 16.6189141, 18.1255351, 15.6304867, 13.2762119, 15],
                       [18.95664,  22.895539, 23.1801296, 23.2813871, 26.8420884, 23.9940623, 27.8974544, 24.54323,   19.5088591, 22.3]];

        // L    DC  R   L   DMC R   L   MC  R   L   OMC R   F
        var positionsAll = [[2, 0,  2,  3,  1,  3,  4,  2,  4,  4,  3,  4,  4], // D C
                            [0, 2,  1,  1,  3,  2,  2,  4,  3,  3,  4,  4,  4], // D L
                            [1, 2,  0,  2,  3,  1,  3,  4,  2,  4,  4,  3,  4], // D R
                            [3, 1,  3,  2,  0,  2,  3,  1,  3,  4,  2,  4,  3], // DM C
                            [1, 3,  2,  0,  2,  1,  1,  3,  2,  2,  4,  3,  4], // DM L
                            [2, 3,  1,  1,  2,  0,  2,  3,  1,  3,  4,  2,  4], // DM R
                            [4, 2,  4,  3,  1,  3,  2,  0,  2,  3,  1,  3,  2], // M C
                            [2, 4,  3,  1,  3,  2,  0,  2,  1,  1,  3,  2,  4], // M L
                            [3, 4,  2,  2,  3,  1,  1,  2,  0,  2,  3,  1,  4], // M R
                            [4, 3,  4,  4,  2,  4,  3,  1,  3,  2,  0,  2,  1], // OM C
                            [3, 4,  4,  2,  4,  3,  1,  3,  2,  0,  2,  1,  3], // OM L
                            [4, 4,  3,  3,  4,  2,  2,  3,  1,  1,  2,  0,  3], // OM R
                            [4, 4,  4,  4,  3,  4,  4,  2,  4,  3,  1,  3,  0]];    // F
        var positionNames = ["D C", "D L", "D R", "DM C", "DM L", "DM R", "M C", "M L", "M R", "OM C", "OM L", "OM R", "F", "GK"];
        var positionFullNames = [
            /* EN */    ["Defender Center", "Defender Left", "Defender Right", "Defensive Midfielder Center", "Defensive Midfielder Left", "Defensive Midfielder Right", "Midfielder Center", "Midfielder Left", "Midfielder Right", "Offensive Midfielder Center", "Offensive Midfielder Left", "Offensive Midfielder Right", "Forward", "Goalkeeper"],
            /* JP */    ["ディフェンダー 中央", "ディフェンダー 左", "ディフェンダー 右", "守備的ミッドフィルダー 中央", "守備的ミッドフィルダー 左", "守備的ミッドフィルダー 右", "ミッドフィルダー 中央", "ミッドフィルダー 左", "ミッドフィルダー 右", "攻撃的ミッドフィルダー 中央", "攻撃的ミッドフィルダー 左", "攻撃的ミッドフィルダー 右", "フォワード", "ゴールキーパー"],
            /* P  */    ["Obrońca środkowy", "Obrońca lewy", "Obrońca prawy", "Defensywny pomocnik środkowy", "Defensywny pomocnik lewy", "Defensywny pomocnik prawy", "Pomocnik środkowy", "Pomocnik lewy", "Pomocnik prawy", "Ofensywny pomocnik środkowy", "Ofensywny pomocnik lewy", "Ofensywny pomocnik prawy", "Napastnik", "Bramkarz"],
            /* D  */    ["Forsvar Centralt", "Forsvar Venstre", "Forsvar Højre", "Defensiv Midtbane Centralt", "Defensiv Midtbane Venstre", "Defensiv Midtbane Højre", "Midtbane Centralt", "Midtbane Venstre", "Midtbane Højre", "Offensiv Midtbane Centralt", "Offensiv Midtbane Venstre", "Offensiv Midtbane Højre", "Angriber", "Målmand"],
            /* I  */    ["Difensore Centrale", "Difensore Sinistro", "Difensore Destro", "Centrocampista Difensivo Centrale", "Centrocampista Difensivo Sinistro", "Centrocampista Difensivo Destro", "Centrocampista Centrale", "Centrocampista Sinistro", "Centrocampista Destro", "Centrocampista Offensivo Centrale", "Centrocampista Offensivo Sinistro", "Centrocampista Offensivo Destro", "Attaccante", "Portiere"],
            /* H  */    ["Defensa Central", "Defensa Izquierdo", "Defensa Derecho", "Mediocampista Defensivo Central", "Mediocampista Defensivo Izquierdo", "Mediocampista Defensivo Derecho", "Mediocampista Central", "Mediocampista Izquierdo", "Mediocampista Derecho", "Mediocampista Ofensivo Central", "Mediocampista Ofensivo Izquierdo", "Mediocampista Ofensivo Derecho", "Delantero", "Portero"],
            /* F  */    ["Défenseur Central", "Défenseur Gauche", "Défenseur Droit", "Milieu défensif Central", "Milieu défensif Gauche", "Milieu défensif Droit", "Milieu Central", "Milieu Gauche", "Milieu Droit", "Milieu offensif Central", "Milieu offensif Gauche", "Milieu offensif Droit", "Attaquant", "Gardien de but"],
            /* A  */    ["Defender Center", "Defender Left", "Defender Right", "Defensive Midfielder Center", "Defensive Midfielder Left", "Defensive Midfielder Right", "Midfielder Center", "Midfielder Left", "Midfielder Right", "Offensive Midfielder Center", "Offensive Midfielder Left", "Offensive Midfielder Right", "Forward", "Goalkeeper"],
            /* C  */    ["Obrambeni Sredina", "Obrambeni Lijevo", "Obrambeni Desno", "Defenzivni vezni Sredina", "Defenzivni vezni Lijevo", "Defenzivni vezni Desno", "Vezni Sredina", "Vezni Lijevo", "Vezni Desno", "Ofenzivni vezni Sredina", "Ofenzivni vezni Lijevo", "Ofenzivni vezni Desno", "Napadač", "Golman"],
            /* G  */    ["Verteidiger Zentral", "Verteidiger Links", "Verteidiger Rechts", "Defensiver Mittelfeldspieler Zentral", "Defensiver Mittelfeldspieler Links", "Defensiver Mittelfeldspieler Rechts", "Mittelfeldspieler Zentral", "Mittelfeldspieler Links", "Mittelfeldspieler Rechts", "Offensiver Mittelfeldspieler Zentral", "Offensiver Mittelfeldspieler Links", "Offensiver Mittelfeldspieler Rechts", "Stürmer", "Torhüter"],
            /* PO */    ["Defesa Centro", "Defesa Esquerdo", "Defesa Direito", "Médio Defensivo Centro", "Médio Defensivo Esquerdo", "Médio Defensivo Direito", "Medio Centro", "Medio Esquerdo", "Medio Direito", "Medio Ofensivo Centro", "Medio Ofensivo Esquerdo", "Medio Ofensivo Direito", "Avançado", "Guarda-Redes"],
            /* R  */    ["Fundas Central", "Fundas Stânga", "Fundas Dreapta", "Mijlocas Defensiv Central", "Mijlocas Defensiv Stânga", "Mijlocas Defensiv Dreapta", "Mijlocas Central", "Mijlocas Stânga", "Mijlocas Dreapta", "Mijlocas Ofensiv Central", "Mijlocas Ofensiv Stânga", "Mijlocas Ofensiv Dreapta", "Atacant", "Portar"],
            /* T  */    ["Defans Orta", "Defans Sol", "Defans Sağ", "Defansif Ortasaha Orta", "Defansif Ortasaha Sol", "Defansif Ortasaha Sağ", "Ortasaha Orta", "Ortasaha Sol", "Ortasaha Sağ", "Ofansif Ortasaha Orta", "Ofansif Ortasaha Sol", "Ofansif Ortasaha Sağ", "Forvet", "Kaleci"],
            /* RU */    ["Defender Center", "Defender Left", "Defender Right", "Defensive Midfielder Center", "Defensive Midfielder Left", "Defensive Midfielder Right", "Midfielder Center", "Midfielder Left", "Midfielder Right", "Offensive Midfielder Center", "Offensive Midfielder Left", "Offensive Midfielder Right", "Forward", "Goalkeeper"],
            /* CE */    ["Obránce Střední", "Obránce Levý", "Obránce Pravý", "Defenzivní Záložník Střední", "Defenzivní Záložník Levý", "Defenzivní Záložník Pravý", "Záložník Střední", "Záložník Levý", "Záložník Pravý", "Ofenzivní záložník Střední", "Ofenzivní záložník Levý", "Ofenzivní záložník Pravý", "Útočník", "Gólman"],
            /* HU */    ["Védő , középső", "Védő , bal oldali", "Védő , jobb oldali", "Védekező Középpályás , középső", "Védekező Középpályás , bal oldali", "Védekező Középpályás , jobb oldali", "Középpályás , középső", "Középpályás , bal oldali", "Középpályás , jobb oldali", "Támadó középpályás , középső", "Támadó középpályás , bal oldali", "Támadó középpályás , jobb oldali", "Csatár", "Kapus"],
            /* GE */    ["მცველი ცენტრალური", "მცველი მარცხენა", "მცველი მარჯვენა", "საყრდენი ნახევარმცველი ცენტრალური", "საყრდენი ნახევარმცველი მარცხენა", "საყრდენი ნახევარმცველი მარჯვენა", "ნახევარმცველი ცენტრალური", "ნახევარმცველი მარცხენა", "ნახევარმცველი მარჯვენა", "შემტევი ნახევარმცველი ცენტრალური", "შემტევი ნახევარმცველი მარცხენა", "შემტევი ნახევარმცველი მარჯვენა", "თავდამსხმელი", "მეკარე"],
            /* FI */    ["Puolustaja Keski", "Puolustaja Vasen", "Puolustaja Oikea", "Puolustava Keskikenttä Keski", "Puolustava Keskikenttä Vasen", "Puolustava Keskikenttä Oikea", "Keskikenttä Keski", "Keskikenttä Vasen", "Keskikenttä Oikea", "Hyökkäävä Keskikenttä Keski", "Hyökkäävä Keskikenttä Vasen", "Hyökkäävä Keskikenttä Oikea", "Hyökkääjä", "Maalivahti"],
            /* SV */    ["Försvarare Central", "Försvarare Vänster", "Försvarare Höger", "Defensiv Mittfältare Central", "Defensiv Mittfältare Vänster", "Defensiv Mittfältare Höger", "Mittfältare Central", "Mittfältare Vänster", "Mittfältare Höger", "Offensiv Mittfältare Central", "Offensiv Mittfältare Vänster", "Offensiv Mittfältare Höger", "Anfallare", "Målvakt"],
            /* NO */    ["Forsvar Sentralt", "Forsvar Venstre", "Forsvar Høyre", "Defensiv Midtbane Sentralt", "Defensiv Midtbane Venstre", "Defensiv Midtbane Høyre", "Midtbane Sentralt", "Midtbane Venstre", "Midtbane Høyre", "Offensiv Midtbane Sentralt", "Offensiv Midtbane Venstre", "Offensiv Midtbane Høyre", "Angrep", "Keeper"],
            /* SC */    ["Defender Centre", "Defender Left", "Defender Richt", "Defensive Midfielder Centre", "Defensive Midfielder Left", "Defensive Midfielder Richt", "Midfielder Centre", "Midfielder Left", "Midfielder Richt", "Offensive Midfielder Centre", "Offensive Midfielder Left", "Offensive Midfielder Richt", "Forward", "Goalkeeper"],
            /* VL */    ["Verdediger Centraal", "Verdediger Links", "Verdediger Rechts", "Verdedigende Middenvelder Centraal", "Verdedigende Middenvelder Links", "Verdedigende Middenvelder Rechts", "Middenvelder Centraal", "Middenvelder Links", "Middenvelder Rechts", "Aanvallende Middenvelder Centraal", "Aanvallende Middenvelder Links", "Aanvallende Middenvelder Rechts", "Aanvaller", "Doelman"],
            /* BR */    ["Zagueiro Central", "Zagueiro Esquerdo", "Zagueiro Direito", "Volante Central", "Volante Esquerdo", "Volante Direito", "Meio-Campista Central", "Meio-Campista Esquerdo", "Meio-Campista Direito", "Meia Ofensivo Central", "Meia Ofensivo Esquerdo", "Meia Ofensivo Direito", "Atacante", "Goleiro"],
            /* GR */    ["Αμυντικός Κεντρικός", "Αμυντικός Αριστερός", "Αμυντικός Δεξιός", "Αμυντικός Μέσος Κεντρικός", "Αμυντικός Μέσος Αριστερός", "Αμυντικός Μέσος Δεξιός", "Μέσος Κεντρικός", "Μέσος Αριστερός", "Μέσος Δεξιός", "Επιθετικός μέσος Κεντρικός", "Επιθετικός μέσος Αριστερός", "Επιθετικός μέσος Δεξιός", "Επιθετικός", "Τερματοφύλακας"],
            /* BG */ ["Защитник Централен", "Защитник Ляв", "Защитник Десен", "Дефанзивен Халф Централен", "Дефанзивен Халф Ляв", "Дефанзивен Халф Десен", "Халф Централен", "Халф Ляв", "Халф Десен", "Атакуващ Халф Централен", "Атакуващ Халф Ляв", "Атакуващ Халф Десен", "Нападател"]];

        function funFix (i) {
            i = (Math.round(i*100)/100).toFixed(2);
            return i;
        }

        function funFix2 (i) {
            i = (Math.round(i*10)/10).toFixed(1);
            return i;
        }

        function funFix3 (i) {
            i = (Math.round(i*1000)/1000).toFixed(3);
            return i;
        }
    }

    /*
    * Init
    *
    */
    var ps = new playersaver();
    ps.init();

});
})();