// ==UserScript==
// @name        PlayerSaver2.0
// @author      Kraxnor
// @namespace   kraxnor
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasTextRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasAxisLabelRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.pointLabels.min.js
// @include     http://trophymanager.com/*
// @include     https://trophymanager.com/*
// @exclude     http://trophymanager.com/players
// @exclude     https://trophymanager.com/players
// @downloadURL https://bitbucket.org/dmirk/tm-playersaver/raw/master/tm-playersaver.user.js
// @updateURL   https://bitbucket.org/dmirk/tm-playersaver/raw/master/tm-playersaver.meta.js
// @version     4.3
// @grant        none
// ==/UserScript==