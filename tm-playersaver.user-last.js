// ==UserScript==
// @name        PlayerSaver
// @description This scripts saves tm+rrec playerdata to a database
// @author      Kraxnor
// @namespace   kraxnor
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/jquery.jqplot.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasTextRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.canvasAxisLabelRenderer.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.8/plugins/jqplot.pointLabels.min.js
// @include     http://trophymanager.com/home/*
// @include     http://trophymanager.com/players/*
// @include     http://trophymanager.com/finances/
// @include     http://trophymanager.com/tactics/*
// @include     http://trophymanager.com/transfer/*
// @exclude     http://trophymanager.com/players
// @downloadURL https://bitbucket.org/dmirk/tm-playersaver/raw/master/tm-playersaver.user.js
// @updateURL   https://bitbucket.org/dmirk/tm-playersaver/raw/master/tm-playersaver.meta.js
// @version     3.4
// @grant       none
// ==/UserScript==

console.log("TM PlayerSaver V2");

//Avoid conflicts
this.$ = this.jQuery = jQuery.noConflict(true);

var serviceUrl = "http://trophy.kraxnor.at/web/api/";
//var serviceUrl = "http://localhost/tm/web/app_dev.php/api/";
var saveButton = document.createElement("a");
var availableGraphTypes = ['r2','rr','sk','r2+sk',];
var SKILLS = null;

var positionNames = ["D C", "D L", "D R", "DM C", "DM L", "DM R", "M C", "M L", "M R", "OM C", "OM L", "OM R", "F", "GK"];

if (location.href.indexOf("/players/") != -1){

  var urlArray = location.href.split("/players/");
  var urlSplitPlayer = urlArray[1].split("/");
  var playerId = urlSplitPlayer[0].replace("#","");
  
  var container = document.createElement("div");
  container.setAttribute("style","float:right; margin-left:10px;width:100%;");
  
  var label = document.createElement("label");
  label.setAttribute("style","margin-right:10px;");
  saveButton.setAttribute("type","button");
  $(label).html("<small id='player_last_save'></small>");
  $(container).append(label);    
  $(saveButton).html("Save to db");
  saveButton.setAttribute("style","display:none;float:right;text-align:center;");
  
  saveButton.onclick=function() {        
    
    document.savePlayer(playerId,SKILLS, INFOS);
  };  
  
  $(container).append(saveButton);
  $('.skill_table:first-child').after(container);
}

if (location.href.indexOf("/tactics/") != -1){  
  var loadPlayerTimeout = null;
  var loadPlayerId = null;
  
  var fieldPlayersStats = {
    "totalR2": 0,
    "totalRR": 0,
    "totalAge": 0,
    "totalRoutine": 0,
    "playingR2": 0,
    "playingRR": 0,
    "playingAge": 0,
    "count": 0,
    "playingCount": 0,
  };
  var lastLastSaved = "";
  var positionMappingStats = {
     "gk": "gk",
     "dl": "dlr",
     "dr": "dlr",
     "dc": "dc",
     "dcl": "dc",
     "dcr": "dc",
     "dml": "dmlr",
     "dmr": "dmlr",
     "dmc": "dmc",
     "dmcl": "dmc",
     "dmcr": "dmc",
     "ml": "mlr",
     "mr": "mlr",
     "mc": "mc",
     "mcl": "mc",
     "mcr": "mc",
     "oml": "omlr",
     "omr": "omlr",
     "omc": "omc",
     "omcl": "omc",
     "omcr": "omc",
     "fc": "fc",
     "fcl": "fc",
     "fcr": "fc",    
  };
  
  document.getPlayerStatsTable = function() {
    fieldPlayersStats.totalR2 = 0;
    fieldPlayersStats.totalRR = 0;
    fieldPlayersStats.totalAge = 0;
    fieldPlayersStats.totalRoutine = 0;
    fieldPlayersStats.playingR2 = 0;
    fieldPlayersStats.playingRR = 0;
    fieldPlayersStats.playingCount = 0;
    fieldPlayersStats.count = 0;
    var fplayers = $('#tactics_field div.field_player[player_set="true"]');
    var updateInterval = window.setInterval(function() {
      
      if (fieldPlayersStats.count == $(fplayers).length) {
                 
        console.log("finished loading player stats");
        //console.log(fieldPlayersStats);

        if (fieldPlayersStats.count == fieldPlayersStats.playingCount) {
          $('#psstats_avgr2').html((fieldPlayersStats.playingR2/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalR2/fieldPlayersStats.count).toFixed(2)).css("color","gold");
          $('#psstats_avgrr').html((fieldPlayersStats.playingRR/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalRR/fieldPlayersStats.count).toFixed(2)).css("color","gold");
          $('#psstats_avgage').html((fieldPlayersStats.totalAge/fieldPlayersStats.count).toFixed(2)).css("color","gold");
          $('#psstats_avgrout').html((fieldPlayersStats.totalRoutine/fieldPlayersStats.count).toFixed(2)).css("color","gold");          
        } else {
          $('#psstats_avgr2').html((fieldPlayersStats.playingR2/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalR2/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
          $('#psstats_avgrr').html((fieldPlayersStats.playingRR/fieldPlayersStats.count).toFixed(2)+"/"+(fieldPlayersStats.totalRR/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
          $('#psstats_avgage').html((fieldPlayersStats.totalAge/fieldPlayersStats.count).toFixed(2)).css("color","#f40");
          $('#psstats_avgrout').html((fieldPlayersStats.totalRoutine/fieldPlayersStats.count).toFixed(2)).css("color","#f40");  
        }
        
        clearInterval(updateInterval);
        //console.log(fieldPlayersStats);

      }
      
    },1000);
      
      $(fplayers).each(function() {
        var playerId = $(this).attr("player_id");
        var playerSet = $(this).attr("player_set");
        var playerStatus = $(this).find(".status");
        var playerMood = $(this).find(".mood");
        var playerPosition = $(this).attr("position");
        
        if (playerId != null && playerId != "undefined" &&
           playerSet != null && playerSet != "undefined" && playerSet == "true") {
            //console.log(playerId);
            document.getPlayerNRecord(playerId,function(res) {
              
              var r2 = res.infos.routine.r2;
              var rr = res.infos.recommandation;       
              var rou = res.infos.routine.routine;
              
               //console.log(res);
              
              if (r2.indexOf("/") != -1) {
                var r2p = r2.split("/");
                if(r2p[0] > r2p[1])
                  r2 = r2p[0];
                else
                  r2 = r2p[1];
              }
              
              //console.log(playerPosition);
              
              if (playerPosition != null && playerPosition != "undefined" && playerPosition != "gk") {
                if (positionMappingStats[playerPosition] != null && positionMappingStats[playerPosition] != "undefined") {
                  var ppms = "r2_"+positionMappingStats[playerPosition];
                  if (res.skills[ppms] != null && res.skills[ppms] != "undefined") {
                    r2 = res.skills[ppms];
                    //console.log(ppms+": "+r2);
                  } else {
                    console.log(ppms+" not found");
                  }
                } else {
                  console.log(playerPosition+" not found");
                }
              } 
               
              var ageo = res.infos.age.toLowerCase();
              var agep = ageo.split(" years ");
              var agem = agep[1].split(" months");
              
              var age = parseFloat(agep[0]) + parseFloat(agem[0]/12);            
              
               fieldPlayersStats.totalR2 += parseFloat(r2);
               fieldPlayersStats.totalRR += parseFloat(rr);
               fieldPlayersStats.totalAge += parseFloat(age);
               fieldPlayersStats.totalRoutine += parseFloat(rou);
              
               fieldPlayersStats.count++;
              
              var isPlaying = false;
              
              if (playerStatus == null || playerStatus == "undefined" || playerStatus.length == 0) {
                isPlaying = true;
              } else {
                var status = playerStatus[0];
                var yellow = $(status).find("img")[0];
                var ysrc = $(yellow).attr("src");
                if (ysrc.indexOf("yellow_card") > -1 || ysrc.indexOf("status_unknown") > -1) {
                  isPlaying = true;
                }
              }
              
              if (isPlaying) {
                fieldPlayersStats.playingCount++;
              }
              
              
              var isOffposition = false;
              
              if (playerMood != null && playerMood != "undefined" && playerMood.length > 0) {
                isOffposition = true;
              }
              
              if (isOffposition) {
                
                var malus = 1.0;
                if ($(playerMood).hasClass("mood1"))
                  malus = 0.95;
                if ($(playerMood).hasClass("mood2"))
                  malus = 0.90;
                if ($(playerMood).hasClass("mood3"))
                  malus = 0.85;
                if ($(playerMood).hasClass("mood4"))
                  malus = 0.80;
                if ($(playerMood).hasClass("mood5"))
                  malus = 0.75;
                if ($(playerMood).hasClass("mood6"))
                  malus = 0.70;
                
                fieldPlayersStats.playingR2 += (parseFloat(r2) * malus);
                fieldPlayersStats.playingRR += (parseFloat(rr) * malus);
                fieldPlayersStats.playingAge += parseFloat(age);
              } else {
                
                fieldPlayersStats.playingR2 += parseFloat(r2);
                fieldPlayersStats.playingRR += parseFloat(rr);
                fieldPlayersStats.playingAge += parseFloat(age);
                
              }              
              
               //console.log(fieldPlayersStats.count+"/"+$(fplayers).length);              
              
            });        
        }        
      }); 
  };
  
  
  
  jQuery(document).ready(function($) {
    
    var statsDiv = $('<div class="odd align_center" style="line-height: 35px;margin-bottom:5px"></div>');
    var statsTable = $('<table></table>');
    var statsRow = $('<tr></tr>');
    $(statsRow).append("<td><b>Avg. R2:</b></td><td><b><span id='psstats_avgr2' style='font-size:1.1em'></span></b></td>");
    $(statsRow).append("<td><b>Avg. RR:</b></td><td><b><span id='psstats_avgrr' style='font-size:1.1em'></span></b></td>");
    $(statsRow).append("<td><b>Avg. Age:</b></td><td><b><span id='psstats_avgage' style='font-size:1.1em'></span></b></td>");
    $(statsRow).append("<td><b>Avg. Rout.:</b></td><td><b><span id='psstats_avgrout' style='font-size:1.1em'></span></b></td>");
    
    $(statsTable).append(statsRow);
    
    $(statsDiv).append(statsTable);
    $('#tactics').after(statsDiv);    
        
    lastLastSaved = $('#tactics_last_save').html();
    var checkLastSaveInterval = setInterval(function(){
      var lls = $('#tactics_last_save').html();
      if (lastLastSaved != lls) {
        lastLastSaved = lls;
        document.getPlayerStatsTable();   
        console.log("tactics changed");
      }
    },600);    
        
    window.setTimeout(function() {
      //delay load, because tm page needs some processing time
      console.log("tactics ready");

      document.getPlayerStatsTable();      
     
      $('#tactics_list_list ul li span.player_name,.field_player, .bench_player').hover(function() {
          //remove existing extension
          $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row").remove();
          $("#tooltip .player_tooltip table tr#playerSaver_rr_r2_row2").remove();
        
          var playermId = $(this).attr("player_id");
          if (playermId == null || playermId == "undefined") return;
        
          if (loadPlayerTimeout != null && playermId != loadPlayerId) {
            window.clearTimeout(loadPlayerTimeout);
            loadPlayerTimeout = null;
            //console.log("canceled player loading");
          } 
        
          if (loadPlayerTimeout == null){
            loadPlayerId = playermId;
            loadPlayerTimeout = window.setTimeout(function() {            
              document.getPlayerNRecord(playermId,function(res) {
                  var r2 = res.infos.routine.r2;
                  var rr = res.infos.recommandation;
                  var app = '<tr id="playerSaver_rr_r2_row" style="color:gold;"><td colspan="1"></td><td class="align_left">RRec</td><td class="align_left"><b>'+rr+'</b></td></tr>';
                   app += '<tr id="playerSaver_rr_r2_row2" style="color:gold;"><td colspan="1"></td><td class="align_left">RatingR2</td><td class="align_left"><b>'+r2+'</b></td></tr>';

                  var tablet = $("#tooltip .player_tooltip").find("table")[0];
                  $(tablet).find("tbody").append(app);

                  loadPlayerTimeout = null;
              });            
            },550);    
            
          }
      });
    },1200);
      
      window.setTimeout(function() {

          $('.field_player').dblclick(function() {        
              console.log("open player: "+ $(this).attr("player_id"));
              window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
          });

          $('.sub_player').closest("li").dblclick(function() {        
              console.log("open player: "+ $(this).attr("player_id"));
              window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
          });


          $('.player_name').dblclick(function() {        
              console.log("open player: "+ $(this).attr("player_id"));
              window.open("http://trophymanager.com/players/"+ $(this).attr("player_id"));
          });
          
          var spurl = "";
          if (location.href.indexOf("/tactics/reserves") != -1) {  
              spurl = serviceUrl + "clubs/"+SESSION['b_team']+"/selldata"+"?apikey="+document.getApiKey();              
          } else {
              spurl = serviceUrl + "clubs/"+SESSION['main_id']+"/selldata"+"?apikey="+document.getApiKey();
          }
          
          
          $.ajax
          ({
              type: "GET",
              url: spurl,
              dataType: 'json',
              error: function() {

              },
              success: function (res) {
                  console.log(res);      

                  var resCntMax = res.data.length;
                  var resCnt = 0;

                  $(res.data).each(function() {
                      var resO = this;
                      if (resO.sellData.potential && resO.sellData.potential != "") {
                          //console.log(resO.sellData.potential);
                          var fPot = parseFloat(resO.sellData.potential.replace(",",".")).toFixed(1);
                          
                          var pl = $('span.player_name[player_id="'+resO.playerId+'"]');
                          $(pl).append(" ["+fPot+"]");
                          
                          var pl2 = $('div.field_player[player_id="'+resO.playerId+'"] div.field_player_name');
                          $(pl2).append(" ["+fPot+"]");
                          
                          var pl3 = $('li.bench_player[player_id="'+resO.playerId+'"] div.bench_player_name');
                          $(pl3).append(" ["+fPot+"]");
                      }
                  });
              },
          });

      },300);
      
      
                
      
    
      
  });
    
    
}

jQuery(document).ready(function() {
  
  if (document.checkForCredentials()) {
    
    //$('#tabplayer_scout_new').trigger('click');    
    

    var playerTable = document.getElementsByClassName("skill_table zebra")[0];
    var infoTable = document.getElementsByClassName("float_left info_table zebra")[0];        

    //workaround for chrome
    if (location.href.indexOf("/players/") != -1) {
        window.setTimeout(function() {
            SKILLS = document.getSkills2(playerTable);
            INFOS = document.getInfos2(infoTable);   

            document.loadLastPlayerRecord(playerTable,infoTable);

            document.updateMenu();
        },200);
    }
  }
  
  document.addCss(".playerSaverGraphMenu ul {list-style:none} .playerSaverGraphMenu ul li {float:left;padding:5px;}");
  
});

var recordCache = {};
document.getPlayerRecord = function(playerId, callback) {  
  if (recordCache[playerId] != null && recordCache[playerId] != "undefined" && recordCache[playerId].nr != null && recordCache[playerId].nr != "undefined") {
    if (callback)
     callback(recordCache[playerId]);
  } else {
    var url = serviceUrl + "players/"+playerId+"/lastrecord"+"?apikey="+document.getApiKey();
    $.ajax
    ({
      type: "GET",
      url: url,
      dataType: 'json',
      error: function() {

      },
      success: function (res){
        recordCache[playerId] = res.data.players[0].record;
        console.log("Loaded lastrecord for player " + playerId + " to cache");
        if (callback)
         callback(recordCache[playerId]);
      },
    });
  }
}

var recordNCache = {};
document.getPlayerNRecord = function(playerId, callback) {  
  if (recordNCache[playerId] != null && recordNCache[playerId] != "undefined" && recordNCache[playerId].nr != null && recordNCache[playerId].nr != "undefined") {
    if (callback)
     callback(recordNCache[playerId]);
  } else {
    var url = serviceUrl + "players/"+playerId+"/record"+"?apikey="+document.getApiKey();
    $.ajax
    ({
      type: "GET",
      url: url,
      dataType: 'json',
      error: function() {

      },
      success: function (res){
        recordNCache[playerId] = res.data.players[0].record;
        console.log("Loaded lastrecord for player " + playerId + " to cache");
        if (callback)
         callback(recordNCache[playerId]);
      },
    });
  }
}



document.addCss = function(cssString) {
  var head = document.getElementsByTagName('head')[0];
  var newCss = document.createElement('style');
  newCss.type = "text/css";
  newCss.innerHTML = cssString;
  head.appendChild(newCss);
};

document.setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    var path = "path=/";
    document.cookie = cname + "=" + cvalue + "; " + expires + "; " + path;
};

document.getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
} 


document.getApiKey = function() {
  var apiKey = document.getCookie("PlayerSaver_apiKey"); 
  //return window.localStorage.getItem('PlayerSaver_apiKey');
  if (apiKey != null && apiKey != "undefined" && apiKey != "")
    return apiKey;
  else
    return null;
};

document.checkForCredentials = function() {
  if (document.getApiKey() != null) {
      return true;
  }
  document.showCredentialInput();
  return false;
};

document.showCredentialInput = function() {

  var dialog = $('<div></div>');
  $(dialog).css("position","absolute");
  $(dialog).css("top","0px");
  $(dialog).css("right","0px");
  $(dialog).css("width","350px");
  $(dialog).css("height","30px");
  $(dialog).css("padding","5px");
  $(dialog).css("background-color","#649024");
  
  $(dialog).html("PlayerSaver - Api Key: <input type='text' id='PlayerSaver_apiKey'/>");
  var apiSave = $('<input type="button" value="save"/>');
  $(dialog).append(apiSave);
  $(apiSave).click(function() {
    //verify apikey
      $.ajax
      ({
        type: "GET",
        url: serviceUrl + "user?apikey="+$('#PlayerSaver_apiKey').val(),
        dataType: 'json',
        success: function (res){
          //store apikey
          //window.localStorage.setItem('PlayerSaver_apiKey',$('#PlayerSaver_apiKey').val());
          document.setCookie('PlayerSaver_apiKey',$('#PlayerSaver_apiKey').val(),90);
                    
          window.setTimeout(function(){
            $(dialog).hide();
            window.location.reload();
          },500);
        },
        error: function() {
          alert("Wrong Api Key, please try again");
        }
        });
  });
  
  
  $('body').append(dialog);
};

function getHSkillsValue(input) {
    if (input == null || input == undefined || input == "") return 0;
    var p1 = input.split("(");
    var p2 = p1[1];
    var p3 = p2.split("/");
    return parseInt(p3[0]);
}
document.getHiddenSkills = function() {
    var res = {};
    var ht = $('#hidden_skill_table');
    if (ht != null && ht != undefined) {
        var htds = $(ht).find("td");
        $(htds).each(function(i,e) {
            switch(i) {
                case 0:
                    res.inuryProneness = getHSkillsValue($(this).text());
                    break;
                case 1:
                    res.aggression = getHSkillsValue($(this).text());
                    break;
                case 2:
                    res.professionalism = getHSkillsValue($(this).text());
                    break;
                case 3:
                    res.adaptability = getHSkillsValue($(this).text());
                    break;
            }
        });
        if (res.inuryProneness && res.inuryProneness > 0)
            console.log(res);
    }
    return res;
}


document.savePlayer = function(playerId,skills, infos) {
  
  
    
  var playerText = $('div.box_sub_header div.large > strong').html();
  var playerPosition = $('div.box_sub_header div.large span.small > strong').text();
  var playerArray = playerText.split(". ");
  var playerName = playerArray[1];
  var playerNr = playerArray[0];
  
  var dataObject = {
    "player": {
      "id":playerId,
      "name":playerName,
      "record": {
        "nr":playerNr,        
        "position": playerPosition,
        "skills" : skills,      
        "infos": infos,
      }
    }
  };
  
  console.log("Save player: "+playerName);
  console.log(dataObject);
  
  $.ajax
  ({
    type: "POST",
    url: serviceUrl + "players/"+playerId+"/records"+"?apikey="+document.getApiKey(),
    dataType: 'json',
    data: dataObject ,
    success: function (res){
      console.log(res);
      
      //localStorage.setItem("player_"+playerId, new Date());
      saveButton.setAttribute("disabled","disabled");
      $(saveButton).html("Saved");
      window.setTimeout(function(){
        $(saveButton).hide();
      },1000);
    }
  });
}

var infoNames = ["","club","age","height/weight","wage","recommandation","skill index", "status", "routine"];
document.getInfos2 = function(table) {
  var infoArray = {};
  var tableData = table.getElementsByTagName("td");
  if (tableData.length > 1) {
      for (var j = 1; j < tableData.length; j += 1) {    
        //if ($(tableData[j]).children().length > 0) {
          if (infoNames[j] == "routine") {    
            var tdContent = $(tableData[j]).clone();
            var r2 = $(tdContent).children().clone();
            $(tdContent).remove("div");
            var routine = {
              "routine": $(tdContent).html().substring(0,5).replace("<d","").replace("<",""),
              "r2":r2[0].textContent.trim(),
            };
            infoArray[infoNames[j]] = routine;
            
          } else if (infoNames[j] == "skill index") {  
            
            var tData = $(tableData[j]).html();
            var divIndex = tData.indexOf("<div>");
            var divEndIndex = tData.indexOf("</div>");
            
            var _si = tData.substring(0,divIndex);
            
            var stiStart = (divIndex+5);
            var stiEnd = (divEndIndex);
            
            var _sti = tData.substring(stiStart,stiEnd);
            
            var si = {
              "si": _si,
              "seasonTi": _sti,
            };
            
            infoArray[infoNames[j]] = si;
            
          } else if (infoNames[j] == "club") {  
            
            var clubId = $(tableData[j]).find("a").attr("club_link");
            console.log("players club: "+clubId);
            
            var club = {
              "clubId": clubId,
              "name": tableData[j].textContent.trim(),
            }
            
            infoArray[infoNames[j]] = club;
              
          } else if (infoNames[j] == "recommandation") {  
              //check for recrecB, ignore this value
              var recTd = tableData[j];
              var divs = $(recTd).find("div");

              if (divs.length > 0)
                  infoArray[infoNames[j]] = divs[0].textContent.trim();
              else
                  infoArray[infoNames[j]] = tableData[j].textContent.trim();
              
              console.log("RRb: "+infoArray[infoNames[j]]);
              
          } else {
            infoArray[infoNames[j]] = tableData[j].textContent.trim();
          }
        //}
        
      }    
  }  
  return infoArray;
};

var skillNames = ["strength","passing","stamina","crossing","pace","technique","marking","heading","tackling","finishing","workrate","longshots","positioning","set pieces","sk1","sk2","rr_dc","rr_dlr","rr_dmc","rr_dmlr","rr_mc","rr_mlr","rr_omc","rr_omlr","rr_fc","r2_dc","r2_dlr","r2_dmc","r2_dmlr","r2_mc","r2_mlr","r2_omc","r2_omlr","r2_fc"];

var skillNamesGoalKeeper = ["strength","handling","stamina","one on ones","pace","reflexes","","aerial ability","","jumping","","communication","","kicking","","throwing","sk1","sk2","rr_dc","rr_dlr","rr_dmc","rr_dmlr","rr_mc","rr_mlr","rr_omc","rr_omlr","rr_fc","r2_dc","r2_dlr","r2_dmc","r2_dmlr","r2_mc","r2_mlr","r2_omc","r2_omlr","r2_fc"];

document.getSkills2 = function(table) {
  
  var playerPosition = $('div.box_sub_header div.large span.small > strong').text();
  var skillArray = {};
  var tableData = table.getElementsByTagName("td");
  
  if (tableData.length > 1) {
      for (var j = 0; j < tableData.length; j += 1) {
        
        var skillName = "";
        if (playerPosition != "Goalkeeper") {
          skillName = skillNames[j];    
        } else {
          skillName = skillNamesGoalKeeper[j]; 
        }
        
        if (tableData[j].innerHTML.indexOf("star.png") > 0) {
            skillArray[skillName] = 20;
          }
          else if (tableData[j].innerHTML.indexOf("star_silver.png") > 0) {
            skillArray[skillName] = 19;
          }
          else if (tableData[j].textContent.length != 0) {
            skillArray[skillName] = tableData[j].textContent;
          }
      }
  } 
  
  return skillArray;
};

var scoutNames = ["scout","date","age","potential","bloom status","development status","speciality","should develop"];

document.getScoutReports = function(div) {
   var scoutReports = {};
   $(div).find('div.odd').each(function() {
     //scoutReports[] = {};
   });
  
   return scoutReports;
};

document.makeGraph = function(target, plots, min) {  
  
  var ystyle='%.0f';
  var colors =  ["#ffffff","gold","#ccc"];
  
  $("#"+target).html("");
  
  if(plots[0].length==0)
  {
   $("#"+target).html("No data available yet");
   return;
  }
  
  var yMin = 0.01;
  var c = min.toString();
  var cd = c[c.length-1];
  
  if (min != null && min != 'undefined' && min >= 6) {
    if (cd == 1)
       yMin = Number((min - 0.50).toFixed(2));
    else 
       yMin = Number((min - 0.51).toFixed(2));
  } else {
    if (min < 5.9) {       
       if (cd == 1)
          yMin = Number((min - 0.02).toFixed(2));
       else
          yMin = Number((min - 0.01).toFixed(2));
    }
  }

  var plot1 = $.jqplot (target, plots,{
    grid:{
      background:"transparent",
      borderColor:"#6c9922",
      gridLineColor:"#6c9922",
      shadow:false
    },
    axes: {
      xaxis: {
        tickOptions:{formatString:'%.00f'},
        pad: 0,
        min: 1,
        max: plots[0].length+1,
        label: "",
        tickInterval:1
      },
      yaxis: {
        /*tickOptions:{formatString:ystyle},
        pad: 0,
        min:ymin,
        max:ymax,
        tickOptions:{formatString:'%.00f'},
        pad: 0,
        tickInterval: 0.01*/
        min: yMin,
      },
      
    },
    series:[{
       lineWidth:3,
       markerOptions: {size:2,style:"circle"}
    }],
    seriesDefaults:{
      showMarker:true,
      lineWidth:3,
      markerOptions: {size:4,style:"circle"},
      pointLabels:{ show:true,"location": "n",edgeTolerance:-20,}
    },
    seriesColors:colors
  });
  
  // add Labels
    $("#"+target+" .jqplot-point-label").hide();
    $("#"+target+" .jqplot-point-label").css("background","#333");
    $("#"+target+" .jqplot-point-label").css("padding","3px");
    previousPoint = null;
    $('#'+target).bind('jqplotDataMouseOver', function (ev, seriesIndex, pointIndex, data) {
      var labels = $(this).find('.jqplot-point-label');
      labels.hide();
      // var point = labels.length-pointIndex;
      $(this).find(".jqplot-point-"+pointIndex).show();
    });
  
};

document.callPlayerGraphsAjax = function(graphType, callback) {
         var urlArray = location.href.split("/players/");
         var urlSplitPlayer = urlArray[1].split("/");
         var playerId = urlSplitPlayer[0].replace("#","");
  
         var url = serviceUrl + "players/"+playerId+"/graphs/"+graphType+"?apikey="+document.getApiKey();
         $.ajax
          ({
            type: "GET",
            url: url,
            dataType: 'json',
            error: function() {

            },
            success: function (res){
               callback(res);
            },
          });       
};

document.loadPlayerGraphs = function(table2) {
        //create graphs
        console.log("testing graphs");
       
        var divContainer1 = $('<div></div>').attr("id","chart55").css("width","100%").css("height","200px").attr("class","graph jqplot-target");
  
        var menuDiv = $('<div></div>').attr("class","tabs_new");
  
        $(availableGraphTypes).each(function(i,e) {
              
              var div = $('<div onclick="" id="" class="playerSaver_graph_tab" data-graph-type="'+e+'"><div style="text-transform:uppercase;">'+e+'</div></div>');
              if (i == 0) {
                 $(div).addClass("active_tab");                  
              }
              div.click(function() {
                  document.callPlayerGraphsAjax($(this).attr("data-graph-type"),function(res) {  
                      var data = res.data.players[0].graph;
                      var min = res.data.players[0].graphMin;
                      document.makeGraph(divContainer1.attr("id"), data, min);
                  });
                  
                  $('.playerSaver_graph_tab').each(function() {
                    $(this).removeClass("active_tab");
                  });
                  $(this).addClass("active_tab");
              });
          
              menuDiv.append(div);
          
        }); 

        $(table2).parent().append(menuDiv);
        $(table2).parent().append(divContainer1);
        
        document.callPlayerGraphsAjax(availableGraphTypes[0],function(res) {
              //var data = [[[1, 2],[3,5.12],[5,13.1],[7,33.6],[9,85.9],[11,219.9]]];        
              var data = res.data.players[0].graph;
              var min = res.data.players[0].graphMin;
              document.makeGraph(divContainer1.attr("id"), data, min);
        });

};


document.loadLastPlayerRecord = function(table, table2) { 
  
  var urlArray = location.href.split("/players/");
  var urlSplitPlayer = urlArray[1].split("/");
  var playerId = urlSplitPlayer[0].replace("#","");
  var playerPosition = $('div.box_sub_header div.large span.small > strong').text();
  var url = serviceUrl + "players/"+playerId+"/lastrecord"+"?apikey="+document.getApiKey();
  console.log("Load last data for player: "+playerId);
  console.log(url);
    
  //if (INFOS['club'].clubId == SESSION['main_id'] ||
  //         INFOS['club'].clubId == SESSION['b_team']) {
  
  $.ajax
  ({
    type: "GET",
    url: url,
    dataType: 'json',
    error: function() {
      
    },
    success: function (res){
      
      if (res.data.status != null && res.data.status == "no player found") {
        if (INFOS['club'].clubId == SESSION['main_id'] ||
           INFOS['club'].clubId == SESSION['b_team']) {
            $(saveButton).attr("class","red_button");
        }
        $(saveButton).show();
      } else {

        //console.log(res);
        var skillArray = res.data.players[0].record.skills;  
        var infoArray = res.data.players[0].record.infos;      
        //console.log(skillArray);
        var tableData = table.getElementsByTagName("td");

        if (INFOS['club'].clubId == SESSION['main_id'] ||
           INFOS['club'].clubId == SESSION['b_team']) {
          console.log("OWN PLAYER!");
          $(saveButton).attr("class","red_button");
        }
        else
          console.log(infoArray['club']+" != "+SESSION['clubname']);



        if (tableData.length > 1) {
          for (var j = 0; j < skillNames.length; j += 1) {

            
            //var skillName = skillNames[j];
            var skillName = "";
            var skillValue = null;
            if (playerPosition != "Goalkeeper") {
              skillName = skillNames[j];    
              skillValue = skillArray[skillName];
            } else {
              skillName = skillNamesGoalKeeper[j]; 
              skillValue = skillArray[skillName];
            }
            
            if (skillName != "" && skillValue != null && skillValue != "undefined") {             
              
              var sml = document.createElement("span");
              $(sml).css("float","left");
              $(sml).css("font-size","9px");
              $(sml).css("text-align","center");


              if (skillName.indexOf("rr_") === -1 && skillName.indexOf("r2_") === -1) {
                $(sml).css("margin-left","-1px");        
              } else {
                $(sml).css("width","100%");
              }

              if (skillValue == 20 || skillValue == "20") {
                var starImg = document.createElement("img");
                $(starImg).attr("title", 20);
                $(starImg).attr("alt", 20);
                $(starImg).attr("src","/pics/star.png");
                $(sml).append(starImg);

              } else if (skillValue == 19 || skillValue == "19") {
                var starImg = document.createElement("img");
                $(starImg).attr("title", 19);
                $(starImg).attr("alt", 19);
                $(starImg).attr("src","/pics/star_silver.png");            
                $(sml).append(starImg);

              } else {

                $(sml).append("<i>"+skillValue+"</i>");

              }
            
            
              
              var skillClass = "";     
              
              var currentValue = 0;
              if (tableData[j].innerHTML.indexOf("star.png") > 0) {
                currentValue = 20;
              }
              else if (tableData[j].innerHTML.indexOf("star_silver.png") > 0) {
                currentValue = 19;
              }
              else if (tableData[j].textContent.length != 0) {
                currentValue = parseFloat(tableData[j].textContent);
              }
              
              var lastValue = parseFloat(skillValue);
              var wrapEle = $('<div></div>');
              //wrapEle.css("border","1px solid #fff");
              wrapEle.css("display","inline-block");
              //wrapEle.css("font-size","14px");
              //wrapEle.css("font-weight","bold");
              wrapEle.attr("title", lastValue);
              
              if (currentValue == 19 || currentValue == 20) {
                var img = $(tableData[j]).find('img')[0];
                if (img)
                  $(img).attr("title", lastValue);
              }

              if (currentValue > lastValue) {
                wrapEle.css("background","linear-gradient(135deg, #007700 0%, #007000 44%, #005500 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");       
                $(tableData[j]).wrapInner( wrapEle );    
              } else if (currentValue < lastValue) {
                wrapEle.css("background","linear-gradient(135deg, #990000 0%, #8f0222 44%, #6d0019 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)");
                $(tableData[j]).wrapInner( wrapEle );    
              } else {
                $(tableData[j]).attr("title", lastValue);
              }
              
            }

          }

          $('#player_last_save').html(res.data.players[0].date);
          if (res.data.players[0].saveRequired === true) {
            if (res.data.players[0].saveCurrentSi != INFOS['skill index'].si) {
              console.log("SI CHANGED - SAVE NEEDED");
              console.log("old si: "+infoArray['skill index'].si+"/ new si: "+INFOS['skill index'].si)
              $(saveButton).show();
            }
            //$(saveButton).show();
          }        
        }  


        //Infos
        var tableData2 = table2.getElementsByTagName("td");
        if (tableData2.length > 1) {
          for (var j = 1; j < tableData2.length; j += 1) {              

            var infoName = infoNames[j];
            if (infoName != "club" && infoName != "clubId" && infoName != "height/weight" && infoName != "age") {
              
              var lastValue2 = infoArray[infoName];
              var wrapEle2 = $('<div></div>');
              wrapEle2.css("display","inline-block");
              
              if (typeof lastValue2 === "object" || typeof lastValue2 === "array") {
                lastValue2 = JSON.stringify(lastValue2);
              } 
              wrapEle2.attr("title", lastValue2 );
              
              $(tableData2[j]).wrapInner( wrapEle2 ); 
              
             /*var sml = document.createElement("span");
              $(sml).css("font-size","9px");
              $(sml).css("width","100%");
              $(sml).css("float","left");           
     
              if (typeof lastValue === "object" || typeof lastValue === "array") {
                $(sml).html("<i>"+JSON.stringify(lastValue)+"</i>");
              } else {
                $(sml).html("<i>"+lastValue+"</i>");
              }

              $(tableData2[j]).append($(sml));*/

            }

          }    
        } 
        
        
        //Scouts
        var scoutReportsDiv = $('div#player_scout_new');
        var reports = document.getScoutReports(scoutReportsDiv);
        console.log("Reports");
        console.log(reports);
        
        //test graph
        document.loadPlayerGraphs(table2);
        
        
        
        
        
      }
    }
  });
  //} //end own only
  
}

/*
  PlayerMenu
*/

var dongleColors = {
  "red": "#f40",
  "yellow": "#FFFF00",
  "green": "#cf0",
}

document.isPlayerBuyable = function() {
  var price = -1;
  var tprice = $('.large.transfer_bid span.coin').html();
  if (tprice) {
      tprice = tprice.replace(",","").replace(",","").replace(",","");
      console.log("tprice: " + tprice);
      if (tprice > 0)
          price = tprice;
  }
  return price;
}

document.savePSSetting = function(key,value) {
    console.log("Stored setting: "+key+" - "+value)
    window.localStorage.setItem('PlayerSaver_settings_'+key, value);
};

document.getPSSetting = function(key) {
  if (window.localStorage.getItem('PlayerSaver_settings_'+key) != null && 
      window.localStorage.getItem('PlayerSaver_settings_'+key) != "undefined") {
     return window.localStorage.getItem('PlayerSaver_settings_'+key);
  } else {
     //console.log("Setting for "+key+" not found");
     return "";
  }
};

document.updateMenu = function() {
  //console.log("update ps menu"); 
  
  
  
  
  
  
  
  //dongle updates
  
  if (INFOS['club'].clubId == SESSION['main_id'] ||
      INFOS['club'].clubId == SESSION['b_team']) {
    //return;
  }
  console.log("player price: " + document.isPlayerBuyable());
  var playerPrice = document.isPlayerBuyable();
  var showDongle = true;
  $('#PlayerSaverMenu_dongle').css("background",dongleColors.green);
  if (INFOS.routine.routine.replace(".",",") < document.getPSSetting('filter_minRoutine')) {
    showDongle = false;
  }
  var recs;
  if (INFOS.recommandation.indexOf("/") != -1) {
    recs = INFOS.recommandation.split("/");
  } else {
    recs = [INFOS.recommandation];
  }
  if (recs.length == 1) {
    if (recs[0].replace(".",",") < document.getPSSetting('filter_minRRec')) {
       showDongle = false;
    }
  } else {
    if (recs[0].replace(".",",") < document.getPSSetting('filter_minRRec') &&
       recs[1].replace(".",",") < document.getPSSetting('filter_minRRec')) {
       showDongle = false;
    }
  }
  
  var r2s;
  if (INFOS.routine.r2.indexOf("/") != -1) {
    r2s = INFOS.routine.r2.split("/");
  } else {
    r2s = [INFOS.routine.r2];
  }
  if (r2s.length == 1) {
    if (r2s[0].replace(".",",") < document.getPSSetting('filter_minR2')) {
       showDongle = false;
    }
  } else {
    if (r2s[0].replace(".",",") < document.getPSSetting('filter_minR2') &&
       r2s[1].replace(".",",") < document.getPSSetting('filter_minR2')) {
       showDongle = false;
    }
  }
  
  var ages;
  ages = INFOS['age'].replace("Years","").replace("Months","");
  ages = ages.split(" ");
  var playerAge = parseFloat(ages[0]) + (parseFloat(ages[2]) / 12);
  if (playerAge > document.getPSSetting('filter_maxAge')) {
    showDongle = false;
  }
  
  if (playerPrice > document.getPSSetting('filter_minR2')) {
    showDongle = false;
  } else if (playerPrice == -1) {
    $('#PlayerSaverMenu_dongle').css("background",dongleColors.yellow);
  } 
  
  if (showDongle) {    
    $('#PlayerSaverMenu_dongle').show();
  } else {
    $('#PlayerSaverMenu_dongle').hide();
  }
};

document.getSettingsDiv = function() {
  var settingsDiv = $('<div id="menu_div_settings" style="margin-top:10px"></div>');
  
  //setting showBids
  var divShowBids = $("<div style=''><label style='float: left; min-width: 100px;'>Show Bids Box</label></div>");
  var inputShowBids = $("<input type='checkbox' value='1' style='margin-left:20px'/>");  
  var valShowBids = document.getPSSetting("setting_showBids");
  if (valShowBids == 1)
    $(inputShowBids).prop("checked",true);
  $(inputShowBids).change(function() {
    if ($(inputShowBids).prop("checked") == true)
      document.savePSSetting("setting_showBids",$(this).val());
    else 
      document.savePSSetting("setting_showBids",0);
  });
  $(divShowBids).append(inputShowBids);  
  $(settingsDiv).append(divShowBids);
  
  //setting openAll
  var divOpenAll = $("<div style=''><label style='float: left; min-width: 100px;'>Show OpenAll</label></div>");
  var inputOpenAll = $("<input type='checkbox' value='1' style='margin-left:20px'/>");  
  var valOpenAll = document.getPSSetting("setting_showOpenAll");
  if (valOpenAll == 1)
    $(inputOpenAll).prop("checked",true);
  $(inputOpenAll).change(function() {
    if ($(inputOpenAll).prop("checked") == true)
      document.savePSSetting("setting_showOpenAll",$(this).val());
    else 
      document.savePSSetting("setting_showOpenAll",0);
  });
  $(divOpenAll).append(inputOpenAll);  
  $(settingsDiv).append(divOpenAll);
  
  //setting MaxOpenAll
  var divMaxOpenAll = $("<div><label style='float: left; min-width: 100px;'>MaxOpenAll</label></div>");
  var inputMaxOpenAll = $("<input tpye='text' style='padding: 3px;width: 60px;'/>");  
  var valMaxOpenAll = document.getPSSetting("setting_maxOpenAll");
  if (valMaxOpenAll < 10)
    valMaxOpenAll = 10;
  $(inputMaxOpenAll).val(valMaxOpenAll);
  $(inputMaxOpenAll).change(function() {
    document.savePSSetting("setting_maxOpenAll",$(this).val());
  });
  $(divMaxOpenAll).append(inputMaxOpenAll);  
  $(settingsDiv).append(divMaxOpenAll);
  
  
  
  return settingsDiv;
}

document.getFilterDiv = function() {
  
  var filterDiv = $('<div id="menu_div_filter"></div>');
  
  //filter maxAge
  var divMaxAge = $("<div><label>MaxAge</label></div>");
  var inputMaxAge = $("<input tpye='text'/>");  
  var valMaxAge = document.getPSSetting("filter_maxAge");
  $(inputMaxAge).val(valMaxAge);
  $(inputMaxAge).change(function() {
    document.savePSSetting("filter_maxAge",$(this).val());
  });
  $(divMaxAge).append(inputMaxAge);  
  $(filterDiv).append(divMaxAge);
  
  //filter maxPrice
  var divMaxPrice = $("<div><label>MaxPrice</label></div>");
  var inputMaxPrice = $("<input tpye='text'/>");  
  var valMaxPrice = document.getPSSetting("filter_maxPrice");
  $(inputMaxPrice).val(valMaxPrice);
  $(inputMaxPrice).change(function() {
    document.savePSSetting("filter_maxPrice",$(this).val());
  });
  $(divMaxPrice).append(inputMaxPrice);  
  $(filterDiv).append(divMaxPrice);

  //filter minRoutine
  var divMinRoutine = $("<div><label>MinRoutine</label></div>");
  var inputMinRoutine = $("<input tpye='text'/>");  
  var valMinRoutine = document.getPSSetting("filter_minRoutine");
  $(inputMinRoutine).val(valMinRoutine);
  $(inputMinRoutine).change(function() {
    document.savePSSetting("filter_minRoutine",$(this).val());
  });
  $(divMinRoutine).append(inputMinRoutine);  
  $(filterDiv).append(divMinRoutine);
  
  //filter minRRec
  var divMinRRec = $("<div><label>MinRRec</label></div>");
  var inputMinRRec = $("<input tpye='text'/>");  
  var valMinRRec = document.getPSSetting("filter_minRRec");
  $(inputMinRRec).val(valMinRRec);
  $(inputMinRRec).change(function() {
    document.savePSSetting("filter_minRRec",$(this).val());
  });
  $(divMinRRec).append(inputMinRRec);  
  $(filterDiv).append(divMinRRec);
  
  //filter minR2
  var divMinR2 = $("<div><label>MinR2</label></div>");
  var inputMinR2 = $("<input tpye='text'/>");  
  var valMinR2 = document.getPSSetting("filter_minR2");
  $(inputMinR2).val(valMinR2);
  $(inputMinR2).change(function() {
    document.savePSSetting("filter_minR2",$(this).val());
  });
  $(divMinR2).append(inputMinR2);  
  $(filterDiv).append(divMinR2);
  
  return filterDiv;
};


var menuOpen = false;
if (true) {
  $("head").append('<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">');
  $("head").append('<style>#menu_div_filter{margin-top:10px}#menu_div_filter label{min-width:100px;float:left}#menu_div_filter > div{width:100%;}#menu_div_filter input{width:60px;padding:3px;}</style>');
  
  var menuContainer = $("<div></div>").css("position","fixed").css("top","0").css("right","0");
  $(menuContainer).css("max-width","200px");
  
  var menu = $("<div></div>");
  $(menu).css("width","30px").css("height","30px");
  $(menu).css("background","#333");
  $(menu).css("overflow","hidden");  
 
  var menuHeader = $('<div style="width:100%;float:left;"><small id="menu_div_title" style="margin:15px 10px;display:none;">PlayerSaver 2.1</small><i class="fa fa-bars" style="font-size:17px;line-height:30px;padding-right:6px;float:right;"></i></div>');
  $(menuHeader).css("cursor","pointer");  
  $(menuHeader).click(function() {
      if (!menuOpen){        
        $(menu).css("width","200px").css("height","300px");
        $('#menu_div_container,#menu_div_title').show();
        menuOpen = true;
      } else {
        window.setTimeout(function() {
           $('#menu_div_container,#menu_div_title').hide();
           $(menu).css("width","30px").css("height","30px");          
           menuOpen = false;
        },200);
      }
  });
  
  $(menu).html(menuHeader);
  var menuDiv = $("<div style='width:100%; display:none;' id='menu_div_container'></div>");
  $(menuDiv).css("padding","10px");
  
  var textApiKey = $("<input type='text'/>").css("width","80%").css("padding","3px");
  $(textApiKey).val(document.getApiKey());
  $(menuDiv).append(textApiKey);
  
  var filterDiv = document.getFilterDiv();  
  $(menuDiv).append(filterDiv); 
  
  var settingsDiv = document.getSettingsDiv();
  $(menuDiv).append(settingsDiv); 
  
  $(menu).append(menuDiv); 
  $(menuContainer).append(menu);
  
  if (location.href.indexOf("/players/") != -1) {  
    var menuDongle = $("<div id='PlayerSaverMenu_dongle'></div>").css("max-width","200px").css("height","30px");
    $(menuDongle).css("background",dongleColors.green);  
    $(menuDongle).css("display","none"); 
    $(menuContainer).append(menuDongle);
  } 
  
  
  $("body").append(menuContainer);
}




/*
  FINANCES
*/

if (location.href.indexOf("/finances") != -1){
  //var financeUrl = "http://trophy.kraxnor.at/web/api/";
  var financeUrl = serviceUrl;//"http://localhost/tm/web/app_dev.php/api/";
  var financeSaveButton = $("<a></a>");
  var financeAvailableGraphTypes = ['balance','total','income','expanses','staff'];
  var FINANCES = {};
  
  /* Savebutton for Finances Screen */  
  
  //jQuery(document).ready(function($) {
  window.setTimeout(function() {
    //console.log("finances");
    
    var financesTable = $("table#finances");
    document.getFinances(financesTable);  
    
    var financeContainer = $("<div></div>");
    var financeLabel = $("<label></label>");
    //label
    $(financeLabel).attr("style","margin-right:10px;");  
    $(financeLabel).html("<small id='finance_last_save'></small>");  
    $(financeContainer).append(financeLabel);
    //console.log("created label");
    
    //button
    $(financeSaveButton).attr("type","button");   
    $(financeSaveButton).attr("style","display:none;float:right;text-align:center;");  
    $(financeSaveButton).click(function() {        
         document.saveFinanceRecord(SESSION["id"], FINANCES);
    });    
    $(financeSaveButton).html("Save to db");
    $(financeContainer).append(financeSaveButton);
    //console.log("created savebutton");

    //append container
    $('div.main_center > div.column2_a > div.box > div.box_body').append(financeContainer);
    
  
    console.log("finances ready for: "+SESSION["id"]);   
    
    //get last record to check if save is required
    document.getFinanceRecord(SESSION["id"], function(res) {
      console.log("Received FinanceRecord");
      //console.log(res);
      //console.log(FINANCES);
      //console.log(res['Current Balance'] +" - "+ FINANCES['Current Balance'].replace(",","") );
      var curB = FINANCES['Current Balance'].replace(",","").replace(",","").replace(",","");
      if (res == null || res === false || res['Current Balance'] != curB) {
        $(financeSaveButton).show();
      }
      
    });
    
    //graph
    document.loadFinanceGraphs(financesTable);
    
  },1000);  
  //});  
  
  document.getFinanceRecord = function(clubId, callbackrec) {  
    
    var url = financeUrl + "clubs/"+clubId+"/lastfinancerecord"+"?apikey="+document.getApiKey();
    $.ajax
    ({
      type: "GET",
      url: url,
      dataType: 'json',
      success: function (res) {
        if (res.data.status != null && res.data.status == 'no record found') {                    
          console.log("Error loading lastrecord for club " + clubId + " to cache: " + res.data.status);
          if (callbackrec)
            callbackrec(false);
          else
            console.log("no callback given");
        } else {
          console.log("Loaded lastrecord for club " + clubId + " to cache");
          if (callbackrec)
            callbackrec(res.data.financeRecord.record);
        }
      },
    });
    
  };   
  
  var financeNames = ["Attendance","TV Money","Sponsors","Merchandise","Food","Staff/Youth/Transfers","Wages","Maintenance","Building Costs","Interest","Total","Current Balance"];
  document.getFinances = function(table) {
    if (table == null || table == "undefined") {
      console.log("Finance parse error: "+table);
      return [];
    }     
    
    var financeData = $(table).find("td");//.getElementsByTagName("td");
    if (financeData == null || financeData == "undefined") {
      console.log("Finance parse error: "+financeData);
      return [];
    }
    
    console.log("parsing finanace data");
    
    if (financeData.length > 1) {
       
        for (var j = 0, i = 0; j < financeData.length; j += 2, i += 1) {

          var skillName = financeNames[i]; 
          FINANCES[skillName] = financeData[j].textContent;
            
        }
    } 
    
    var tableCont = $('div.main_center > div.column2_a > div.box > div.box_body');
    var balCont = $(tableCont).find('span.coin_big')[0];
    
    FINANCES['Current Balance'] = balCont.textContent;   
    FINANCES['Fan Count'] = 0;   
    
    console.log("parsed finanace data");
    //console.log(FINANCES);

    return FINANCES;
  };
  
  document.saveFinanceRecord = function(clubId, srecord) {    
    console.log("saving finance record");
    console.log(srecord);
    
    $.ajax
    ({
      type: "GET",
      url: "/_test_t",
      success: function (html){
         var start = html.indexOf('<hr />Array\n(')+('<hr />Array\n(').length;        
         var end = html.indexOf(')\n<hr />');       
         var data = html.substring(start,end);         
         var start = data.indexOf('[after] => ')+('[after] => ').length;
         var fans = data.substring(start);
         console.log("got fans:" + fans);
        
        
       
          if (srecord != null) {
            srecord['Fan Count'] = fans;
            $.ajax
            ({
              type: "POST",
              url: financeUrl + "clubs/"+clubId+"/financerecords"+"?apikey="+document.getApiKey(),
              dataType: 'json',
              data: {"financerecord": srecord } ,
              success: function (res){
                console.log(res);

                financeSaveButton.setAttribute("disabled","disabled");
                $(financeSaveButton).html("Saved");
                window.setTimeout(function(){
                  $(financeSaveButton).hide();
                  window.location.reload();
                },1000);
              }
            });
          }
      }
    });
    
    
  };
  
  
  document.callFinanceGraphsAjax = function(clubId, graphType, callback) {

    var url = financeUrl + "clubs/"+clubId+"/graphs/"+graphType+"?apikey="+document.getApiKey();
    $.ajax
    ({
      type: "GET",
      url: url,
      dataType: 'json',
      error: function() {

      },
      success: function (res){
        callback(res);       
      },
    });       
  };
  
  document.loadFinanceGraphs = function(table2) {
    //create graphs

    var divTab = $('<div></div>').css("padding","5px");
    var divContainer1 = $('<div></div>').attr("id","chart75").css("width","100%").css("height","300px").attr("class","graph jqplot-target");

    var menuDiv = $('<div></div>').attr("class","tabs_new");

    $(financeAvailableGraphTypes).each(function(i,e) {

      var div = $('<div onclick="" id="" class="playerSaver_graph_tab" data-graph-type="'+e+'"><div style="text-transform:uppercase;">'+e+'</div></div>');
      if (i == 0) {
        $(div).addClass("active_tab");                  
      }
      div.click(function() {
        document.callFinanceGraphsAjax(SESSION['id'],$(this).attr("data-graph-type"),function(res) {  
          var data = res.data.financeGraph.graph;
          var min = res.data.financeGraph.graphMin;
          if (data.length > 0)    
            document.makeFinanceGraph(divContainer1.attr("id"), data, min);
        });

        $('.playerSaver_graph_tab').each(function() {
          $(this).removeClass("active_tab");
        });
        $(this).addClass("active_tab");
      });

      menuDiv.append(div);

    }); 

    //$(table2).parent().append(menuDiv);
    //$(table2).parent().append(divContainer1);
    var tableCont = $('div.main_center > div.column2_a > div.box > div.box_body'); 
    
    $(divTab).append(menuDiv);
    $(divTab).append(divContainer1);

    document.callFinanceGraphsAjax(SESSION['id'],financeAvailableGraphTypes[0],function(res) {
    
      var data = res.data.financeGraph.graph;
      var min = res.data.financeGraph.graphMin;
      
      if (data.length > 0) {
        
        
        tableCont.append(divTab);        
        
        document.makeFinanceGraph(divContainer1.attr("id"), data, min);
      }
      else
        console.log("Error loading graph: no data");
    });

  };

 
  document.makeFinanceGraph = function(target, plots, min) {  
  
    var ystyle='%.0f';
    var colors =  ["#ffffff","gold","#0055ff","#00cc66","#aa0066","#00ffff",];

    $("#"+target).html("");

    if(plots[0].length==0)
    {
     $("#"+target).html("No data available yet");
     return;
    }

    var yMin = 0;
    if (Math.abs(min) > 1)
      yMin = Math.round(min,2) - 1.01;
    else 
      yMin = Math.round(min,2) - 0.01;


    var plot1 = $.jqplot (target, plots,{
      grid:{
        background:"transparent",
        borderColor:"#6c9922",
        gridLineColor:"#6c9922",
        shadow:false
      },
      axes: {
        xaxis: {
          tickOptions:{formatString:'%.00f'},
          pad: 0,
          min: 1,
          max: plots[0].length+1,
          label: "",
          tickInterval:1
        },
        yaxis: {
          /*tickOptions:{formatString:'%.00f'},
          pad: 0,
          min:ymin,
          max:ymax,
          tickOptions:{formatString:'%.00f'},
          pad: 0,
          tickInterval: 0.01*/
          min: yMin,
        },

      },
      series:[{
         lineWidth:3,
         markerOptions: {size:2,style:"circle"}
      }],
      seriesDefaults:{
        showMarker:true,
        lineWidth:3,
        markerOptions: {size:4,style:"circle"},
        pointLabels:{ show:true,"location": "n",edgeTolerance:-20,}
      },
      seriesColors:colors
    });
    // add Labels
    $("#"+target+" .jqplot-point-label").hide();
    $("#"+target+" .jqplot-point-label").css("background","#333");
    $("#"+target+" .jqplot-point-label").css("padding","3px");
    previousPoint = null;
    $('#'+target).bind('jqplotDataMouseOver', function (ev, seriesIndex, pointIndex, data) {
      var labels = $(this).find('.jqplot-point-label');
      labels.hide();
      // var point = labels.length-pointIndex;
      $(this).find(".jqplot-point-"+pointIndex).show();
    });

  };
  
  
}

//openall button
if (document.getPSSetting("setting_showOpenAll") == 1) {
  if (location.href.indexOf("/transfer/") != -1) {

      var maxOpenCount = parseInt(document.getPSSetting("setting_maxOpenAll"));
      if (maxOpenCount < 10)
        maxOpenCount = 10;
      if (maxOpenCount > 100)
        maxOpenCount = 100;
      var openAllButton;  
      var toOpenPlayers;
      var startIndex = 0;

      function openPlayersIndexed(start, count) {      

        for(i = start; i < start + count; i++ ) {
          if (i < toOpenPlayers.length) {
           console.log(toOpenPlayers[i]);
           window.open(toOpenPlayers[i]);
          }
        }
        startIndex = start + count;

        if (startIndex >= toOpenPlayers.length)
          startIndex = 0;


      }


      console.log("init transfer button");
      openAllButton = $("<button class='button button_icon'>Open "+startIndex+"-"+(startIndex+maxOpenCount)+"</button>");
      var buttonDiv = $("<div style='text-align:center'></div>");
      $(buttonDiv).html(openAllButton);
      var resetButton = $("<button class='button button_icon' style='margin-left:5px;display:none'>x</button>");
      $(resetButton).click(function() {
              startIndex = 0;
              $(this).hide();
              $(openAllButton).html("Open "+startIndex+"-"+(startIndex+maxOpenCount)+"");
      });
      $(buttonDiv).append(resetButton);

      $(openAllButton).click(function() {
        //retrieve all player urls

        if (startIndex == 0) {
          var players = $('#transfer_list table tr div.player_name a');    
          var entries = [];

          $(players).each(function() {

            if (!$(this).closest("tr").hasClass("watched-player") && !$(this).closest("tr").hasClass("watched-player-shortlist")) {
              var url = $(this).attr("href");  
              entries.push(url);
            }         

          });

          toOpenPlayers = entries;     
          $(resetButton).show();   

        }

        openPlayersIndexed(startIndex,maxOpenCount);
        $(this).html("Open "+startIndex+"-"+(startIndex+maxOpenCount)+"");

      });    



      $('#filters').prepend(buttonDiv);  
  }
}




//transferhistory
if (location.href.indexOf("/players/") != -1) {
  
    console.log("init transfer history");
  
    //get all transferrecords
     var url = serviceUrl + "players/"+playerId+"/transfers"+"?apikey="+document.getApiKey();
      $.ajax
      ({
        type: "GET",
        url: url,
        dataType: 'json',
        error: function() {

        },
        success: function (res){
          
          //var result = JSON.parse(res);
          var result = res.data;
          
          //console.log(res);
          var cont = $('.column3_a');
          
          var box = $('<div class="box" style="margin-top:20px"></div>');
          $(box).append('<div class="box_head"><h2 class="std">Transfer Records<br/><small><i>PlayerSaver</i></small></h2></div>');
          var boxContent = $('<div class="box_body"></div>');
          $(boxContent).append('<div class="box_shadow"></div>');        
          
          var boxBody = $('<div class="std align_center transfer_box"></div>');        
          
          var table = $('<table></table>');
          var th = $('<tr></tr>');         
          $(th).append("<th>age</th>");
          $(th).append("<th>rec/Si</th>");
          $(th).append("<th>min/Max</th>");
          $(th).append("<th>end</th>");
          $(th).append("<th>info</th>");
          $(table).append(th);
          $(result.records).each(function(index, record) {
            console.log(record);
            var tr = $("<tr></tr>");           
            $(tr).append("<td>"+record.age+"</td>");
            $(tr).append("<td>"+record.rec+" (<small>"+record.si+"</small>)</td>");
            $(tr).append("<td><b>"+(record.minPrice/1000000).toFixed(2)+"M</b> (<small>"+(record.maxPrice/1000000).toFixed(2)+"M</small>)</td>");
            $(tr).append("<td><small>"+record.endTime+"</small></td>");
            
            
            
            $(tr).append("<td><a title='note: "+record.note+"\nbold: "+record.bold+"\nblue: "+record.boldAndBlue+"\nexpiry: "+record.expiry+"' onclick='return false;'>o</a></td>");
            
            $(table).append(tr);            
          });
          
          $(boxBody).append(table);
          $(boxContent).append(boxBody);
          
          $(box).append(boxContent);
          $(box).append('<div class="box_footer"><div></div></div>');
          
          $(cont).append(box);          
          
        },
      });             
      
  
    $('#transferlist_button').on("click",function() {
      
      console.log("transfer button clicked");
      
      var checkInterval = setInterval(function() {
        
        var button = $('#modal').find('#transfer_submit_button')[0];
        var okButton = $('#modal').find('span.button')[0];
        
        if ((button != null && button != "undefined") ||
           (okButton != null && okButton != "undefined")) {
          
          clearInterval(checkInterval);
          console.log("Transfer window opened");
          
          if (button != null && button != "undefined") {
            
           
            
             //console.log(button);
          
              var newButton = $("<button>Test Transfer</button>");
              //$("#modal .inner").append(newButton);

              //$(button).off();            
              $(button).click(function() {

              //$(newButton).click(function() {

                var minprice = $('#min_price').val();
                var maxprice = $('#max_price').val();
                var cleanMax = maxprice.split('.')[0];                
                
                var expiry = "";
                
                if ($('#transfer_expiry1').prop("checked") === true) {
                  
                  expiry = "5days";
                  
                } else if ($('#transfer_expiry2').prop("checked") === true) {
                  
                  expiry = $('#exp_time1').val();
                  
                } else if ($('#transfer_expiry3').prop("checked") === true) {
                  
                  expiry = $('#exp_days').val()+"#"+$('#exp_time2').val();
                  
                }

                var note = $('#transfer_textarea').val();

                var entry = {
                  "playerId": playerId,
                  "minPrice": minprice,
                  "maxPrice": cleanMax,
                  "expiry": expiry,
                  "note": note,        
                  "bold": 0,
                  "boldAndBlue": 0,
                };

                console.log(entry);                
               
                var url = serviceUrl + "players/"+playerId+"/transfers"+"?apikey="+document.getApiKey();
                $.ajax
                ({
                  type: "POST",
                  url: url,
                  dataType: 'json',
                  data: {"record": entry },
                  error: function() {

                  },
                  success: function (res){
                    console.log(res);
                  },
                });             

              });    
              
              
          }         
          
        } else {
          console.log("Checking for window opened");
        }
      },300);        

    });
  
}

//bids box
if (document.getPSSetting("setting_showBids") == 1) {
  if (/*location.href.indexOf("/players/") != -1 || */location.href.indexOf("/home/") != -1) {

    console.log("init bid box");

    var url = "/bids/";
    $.ajax
    ({
      type: "GET",
      url: url,
      error: function() {

      },
      success: function (res){
        var bidsPage = $(res);
        var box = $(bidsPage).find(".column2_a div.box");        
        if (box != null && box != "undefined") {

          if (location.href.indexOf("/players/") != -1) {
            //console.log(box);
            var cont = $('.column3_a');
            $(cont).append(box);
            console.log("inserted bids box");
          }
          if (location.href.indexOf("/home") != -1) {
            var ele = $('.column2_a .box:first-child');
            $(ele).append(box);
            console.log("inserted bids box");            
          }


        }

      }
    }); 

  }
}






/**
*
* PlayerSeller
*
*/
function zeroPad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function formatDate(date) {

    var cDate = date.getDate();
    var cMonth = date.getMonth();
    var cYear = date.getFullYear();

    var cHour = date.getHours();
    var cMin = date.getMinutes();
    var cSec = date.getSeconds();
    
    
    return zeroPad(cDate,2)+"."+zeroPad(cMonth+1,2)+"."+cYear;
}

var seasonStartFirst = new Date("2015/11/30 00:00");
var seasonDates = [
    {
        "s":44,
        "start":seasonStartFirst.getTime(),
        "half":seasonStartFirst.getTime() + (6 * 7 * 24 * 60 * 60 * 1000),
        "end":seasonStartFirst.getTime() + (12 * 7 * 24 * 60 * 60 * 1000),
    }
];

for(i = 45; i < 65; i++) {
    var lastEnd = new Date(seasonDates[i - 45].end);
    seasonDates.push({
        "s":i,
        "start":lastEnd.getTime(),
        "half":lastEnd.getTime() + (6 * 7 * 24 * 60 * 60 * 1000),
        "end":lastEnd.getTime() + (12 * 7 * 24 * 60 * 60 * 1000),
    });
}

//console.log(seasonDates);
function getCurrentSeasonHalf() {
    var now = new Date();
    for(i = 0; i < seasonDates.length; i++) {
        if (seasonDates[i].start < now.getTime() && now.getTime() <= seasonDates[i].end)
            return new Date(seasonDates[i].half);
    }
    return null;
}
function getCurrentSeasonEnd() {
    var now = new Date();
    for(i = 0; i < seasonDates.length; i++) {
        if (seasonDates[i].start < now.getTime() && now.getTime() <= seasonDates[i].end)
            return new Date(seasonDates[i].end);
    }
    return null;
}
//console.log("Seasonhalftime: "+getCurrentSeasonHalf());
//console.log("Seasonendtime: "+getCurrentSeasonEnd());

var cacheName = "playerseller_tooltip_cache_3";
var tooltipCache = {
    players: {},
    lastSave: new Date().getTime(),
};
if (window.localStorage.getItem(cacheName)) {
    tooltipCache = JSON.parse(window.localStorage.getItem(cacheName));
    console.log("Tooltip cache: "+tooltipCache.lastSave);
    console.log(tooltipCache);
    //todo: check if cache requires reset
    var now = new Date();
    var cacheDiff = now.getTime() - parseInt(tooltipCache.lastSave);    
    //console.log(cacheDiff);
    if (cacheDiff > 0.5 * 60 * 60 * 1000) {
        console.log("cache reset required");
        window.localStorage.removeItem(cacheName);        
    }
    
} else {
    window.localStorage.setItem(cacheName,JSON.stringify(tooltipCache));
}

//player-site options box
if (location.href.indexOf("/players/") != -1) {
    
    function detectScoutPage() {
        var res = new Object();
        res.detected = false;
        res.avgPotential = 0;
        res.reportCount = 0;
        
        var scoutTab = $('#player_scout_new');
        if ($(scoutTab).is(':visible')) {
            res.detected = true;
            //get scout reports
            var reports = $('#player_scout_new > div');
            $(reports).each(function() {
                
                var rows = $(this).children("div");
                var pot = rows[3];
                pot = $(pot).text();
                
                var temp = pot.split("(");
                //console.log(temp); 
                if (temp != null && temp.length > 1) {
                    //console.log("---");
                    pot = temp[1].replace(")","");                                  
                    //console.log(pot);                        
                    res.avgPotential += parseInt(pot); 
                    res.reportCount++;
                }
            });   
            res.avgPotential = res.avgPotential / res.reportCount;
        }        
        return res;
    }
    
    
    jQuery(document).ready(function() {
        window.setTimeout(function() {

            if (INFOS['club'].clubId == SESSION['main_id'] ||
                INFOS['club'].clubId == SESSION['b_team']) {
                
                var scoutr = detectScoutPage();
                console.log("scout detection");
                console.log(scoutr);


                var cont = $('.column3_a');

                var box = $('<div class="box" style="margin-top:20px"></div>');
                $(box).append('<div class="box_head"><h2 class="std">Player Seller<br/><small><i>PlayerSaver</i></small></h2></div>');
                var boxContent = $('<div class="box_body"></div>');
                $(boxContent).append('<div class="box_shadow"></div>');        

                var boxBody = $('<div class="std align_center transfer_box"></div>');        

                var form = $('<form></form>');
                var select = $('<select id="ps_player_box_type_select_'+playerId+'"></select>');
    
                $(select).append('<option value="sell_3_0">sell 3.0 stars</option>');
                $(select).append('<option value="sell_3_5">sell 3.5 stars</option>');
                $(select).append('<option value="sell_4_0">sell 4.0 stars</option>');
                $(select).append('<option value="sell_4_5">sell 4.5 stars</option>');
                $(select).append('<option value="sell_5_0">sell 5.0 stars</option>');                
                
                $(select).append('<option value="talent">talent</option>');
                /*$(select).append('<option value="talent_16">talent 16</option>');
                $(select).append('<option value="talent_17">talent 17</option>');
                $(select).append('<option value="talent_18">talent 18</option>');
                $(select).append('<option value="talent_19">talent 19</option>');
                $(select).append('<option value="talent_20">talent 20</option>');*/
                
                $(select).append('<option value="first_16">first 16</option>');
                $(select).append('<option value="reserve_16">reserve 16</option>');
                
                $(select).append('<option value="sold">sold</option>');
                $(select).append('<option value="remove">remove</option>');
                $(form).append("<label>Type: </label>");
                
                
                
                
                
                $(form).append(select);
                
               
                
                var button = $('<button id="ps_player_seller_save_button">save</button>');
                $(button).click(function() {
                    var sbtn = this;
                    $(sbtn).html("save");
                    var url = serviceUrl + "players/"+playerId+"/selldatas/type/updates/"+$(select).val()+"?apikey="+document.getApiKey();
                    //console.log(url);
                    $.ajax
                    ({
                        type: "GET",
                        url: url,
                        dataType: 'json',
                        error: function() {
                            $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                        },
                        success: function (res){
                            //console.log(res);
                            
                            //check round
                            var fr = $('#ps_player_box_type_frnd_'+playerId);
                            var sr = $('#ps_player_box_type_srnd_'+playerId);
                            var nr = $('#ps_player_box_type_nrnd_'+playerId);
                            var rurl = "";
                            if ($(fr).prop("checked")) {
                                rurl = serviceUrl + "players/"+playerId+"/selldatas/round/updates/1?apikey="+document.getApiKey();                    
                            } else if ($(sr).prop("checked")) {
                                rurl = serviceUrl + "players/"+playerId+"/selldatas/round/updates/2?apikey="+document.getApiKey();   
                            } else if ($(nr).prop("checked")) {
                                rurl = serviceUrl + "players/"+playerId+"/selldatas/round/updates/0?apikey="+document.getApiKey();                         
                            }
                            console.log(rurl);  
                            if (rurl != "") {
                                $.ajax
                                ({
                                    type: "GET",
                                    url: rurl,
                                    dataType: 'json',
                                    error: function() {
                                        $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                    },
                                    success: function (res){
                                        //console.log(res);  
                                        
                                        var pot = $('#ps_player_box_potential_'+playerId);
                                        if ($(pot).prop("disabled") == false && $(pot).val() > 0) {
                                            var potStr = (""+$(pot).val()).replace(".",",");
                                            var purl = serviceUrl + "players/"+playerId+"/selldatas/potential/updates/"+potStr+"?apikey="+document.getApiKey();
                                            
                                            console.log(purl);
                                            $.ajax
                                            ({
                                                type: "GET",
                                                url: purl,
                                                dataType: 'json',
                                                error: function() {
                                                    $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                },
                                                success: function (res){
                                                    console.log(res);
                                                    $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');     
                                                },
                                            });
                                        }
                                        
                                        
                                        var tbutton = $('.transfer_box').find('span.button.disabled')[0];
                                        if (tbutton != null) {
                                            //console.log($(tbutton).attr("tooltip"));
                                            var ddate_str = $(tbutton).attr("tooltip");
                                            ddate_str = ddate_str.replace("This player locked for transfer until ","");
                                            var ddate = new Date(ddate_str);
                                            ddate_str = Math.floor(ddate.getTime() /1000);
                                            /*        
                         console.log(ddate); */

                                            var turl = serviceUrl + "players/"+playerId+"/selldatas/transferDate/updates/"+ddate_str+"?apikey="+document.getApiKey();
                                            console.log(turl);
                                            $.ajax
                                            ({
                                                type: "GET",
                                                url: turl,
                                                dataType: 'json',
                                                error: function() {
                                                    $(sbtn).prepend('<img src="/pics/small_red_x.png">&nbsp;');
                                                },
                                                success: function (res){
                                                    console.log(res);
                                                    $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');     
                                                },
                                            });

                                        }  else {
                                            $(sbtn).prepend('<img src="/pics/mini_green_check.png">&nbsp;');   
                                        }
                                                                           
                                    },
                                });
                            }
                        },
                    });   
                    
                    return false;
                });
                $(form).append(button);
                
                $(form).append("<br/>");
                var nrnd = $('<input type="radio" name="ps_player_box_type_rnd_'+playerId+'[]" id="ps_player_box_type_nrnd_'+playerId+'" checked/>');
                $(form).append(nrnd);
                $(form).append("<label>no</label>");
                
                var frnd = $('<input type="radio" name="ps_player_box_type_rnd_'+playerId+'[]" id="ps_player_box_type_frnd_'+playerId+'" style="margin-left:10px"/>');
                $(form).append(frnd);
                $(form).append("<label>1st</label>");
                
                var srnd = $('<input type="radio" name="ps_player_box_type_rnd_'+playerId+'[]" id="ps_player_box_type_srnd_'+playerId+'" style="margin-left:10px"/>');
                $(form).append(srnd);
                $(form).append("<label>2st</label>");
                
                
                $(form).append("<br/>");
                var pot = $('<input type="text" name="ps_player_box_potential_'+playerId+'" id="ps_player_box_potential_'+playerId+'" style="width:50px;margin-right:10px"><label>Avg. Pot</label>');
                if (scoutr.detected == false)
                    $(pot).prop("disabled",true);
                else {
                    $(pot).val(scoutr.avgPotential.toFixed(2));
                }
                $(form).append(pot);

                $(boxBody).append(form);
                $(boxContent).append(boxBody);

                $(box).append(boxContent);
                $(box).append('<div class="box_footer"><div></div></div>');

                $(cont).append(box);                
            }

        },200);
    });    
}


var ps_menuOpen = false;
var ps_menuOpen_width = "420px";
var ps_menuClosed_width = "320px";
var ps_menuClosed_height = "55px";
//player-seller dongle
if (location.href.indexOf("/players/") != -1 || location.href.indexOf("/home") != -1) {
    
    var resTotals = {
                wages: 0,
                onList: 0,
                fRnd: 0,
                sRnd: 0,
            };
    
    function updatePsRow(resO,p) {
        //console.log(p);
        $('#ps_player_row_'+resO.playerId+" .ps_player_rec").html(p.recommendation);

        $('#ps_player_row_'+resO.playerId+" .ps_player_age").html(p.age+"."+p.months);
        //if (p.country != "at")
        $('#ps_player_row_'+resO.playerId+" .ps_player_name").prepend(p.flag);

        var row = $('#ps_player_row_'+resO.playerId);

        if ((resO.sellData.type == "sell_3_0" && p.rec_sort >= 3.0 && p.rec_sort < 3.5) ||
            (resO.sellData.type == "sell_3_5" && p.rec_sort >= 3.5 && p.rec_sort < 4.0) ||
            (resO.sellData.type == "sell_4_0" && p.rec_sort >= 4.0 && p.rec_sort < 4.5) ||
            (resO.sellData.type == "sell_5_0" && p.rec_sort >= 5) ||
            (resO.sellData.type == "sell_4_5" && p.rec_sort >= 4.5 && p.rec_sort < 5.0)) {



            if (resO.sellData.transferDate && resO.sellData.transferDate != null && resO.sellData.transferDate != "undefined") {
                if (new Date().getTime() < parseInt(resO.sellData.transferDate)*1000) {
                    $('#ps_player_row_'+resO.playerId+" td").css("background","#696969");
                    $(row).addClass("ps_player_row_orange");

                    var timage = $('<img src="/pics/small_red_x.png" title="sell '+formatDate(new Date(parseInt(resO.sellData.transferDate)*1000))+'">');
                    $('#ps_player_row_'+resO.playerId+' .ps_player_status').append(timage);
                } else {
                    $('#ps_player_row_'+resO.playerId+" td").css("background","#2e8b57");
                    $(row).addClass("ps_player_row_green");
                    //$(row).remove();
                    //$(tbody).prepend(row);
                }
            } else {
                $('#ps_player_row_'+resO.playerId+" td").css("background","#2e8b57");
                $(row).addClass("ps_player_row_green");
                //$(row).remove();
                //$(tbody).prepend(row);
            }



        } else {
            $('#ps_player_row_'+resO.playerId+" td").css("background","darkred");
            $(row).addClass("ps_player_row_red");
            //$(row).remove();
            //$(tbody).append(row);
        }
        //check transfer status
        var onList = false;
        var hasEndTime = false;
        if (resO.transferRecords && resO.transferRecords.length > 0) {

            var onListEnd = null;
            var lastTRecord = null;
            $(resO.transferRecords).each(function(i,t) {                                                  
                var stringToParse = t.endTime;
                var dateParts     = stringToParse.match(/(\d{2})\.(\d{2})\.(\d{4})\s+(\d{2}):(\d{2})/);
                dateParts[2] -= 1; //months are zero-based
                dateParts = dateParts.slice(1);
                //console.log(dateParts);

                var endTime = new Date(dateParts[2], dateParts[1], dateParts[0], dateParts[3], dateParts[4], 0,0);
                lastTRecord = endTime;
                if (new Date().getTime() < endTime.getTime())  {
                    onList = true;
                    onListEnd = endTime;
                    hasEndTime = true;
                }
            });
            if (onList) {
                var timage = $('<img src="/pics/auction_hammer_small.png" title="until '+onListEnd.toLocaleString()+'">');
                //$('#ps_player_row_'+resO.playerId+" .ps_player_name").append(timage);
                $('#ps_player_row_'+resO.playerId+' .ps_player_status').append(timage);
                resTotals.onList++;
            } else {
                if (new Date().getTime() < lastTRecord.getTime() + (5 * 24 * 60 * 60 * 1000))  {
                    $('#ps_player_row_'+resO.playerId+' .ps_player_status').append("+");
                }
            }
        }    

        //if (!onList) {
            if (resO.sellData.round && resO.sellData.round != null && resO.sellData.round != "undefined" && parseInt(resO.sellData.round) > 0) {

                $('#ps_player_row_'+resO.playerId+' .ps_player_status').append("<span style='background:#333;color:#fff;padding:1px 3px'>"+resO.sellData.round+"</span>");

                if (parseInt(resO.sellData.round) == 1) {
                    $('#ps_player_box_type_frnd_'+resO.playerId).prop("checked",true);
                    resTotals.fRnd++;
                } else if (parseInt(resO.sellData.round) == 2) {
                    $('#ps_player_box_type_srnd_'+resO.playerId).prop("checked",true);
                    resTotals.sRnd++;
                } else {
                    $('#ps_player_box_type_nrnd_'+resO.playerId).prop("checked",true);
                }
            } else {
                $('#ps_player_box_type_nrnd_'+resO.playerId).prop("checked",true);
            }
        
        
            
        //}

        var pwage = $(p.wage);
        var pwt = $(pwage).text();                                    
        pwt = pwt.replace(",","").replace(",","").replace(",",""),
        //console.log(pwt);
        resTotals.wages += parseInt(pwt);


    }

    function sortPsRowsCompare(a,b) {
        var rra = $(a).find('td:nth-child(3)').text();
        var rrb = $(b).find('td:nth-child(3)').text();
        //console.log(rra);
        rra = parseFloat(rra);
        rrb = parseFloat(rrb);
        if (rra < rrb) {
            return 1;
        }
        if (rra > rrb) {
            return -1;
        }
        return 0;
    }
    
    function updatePsTable(tbody) {
        var greens = $(tbody).find("tr.ps_player_row_green");
        var oranges = $(tbody).find("tr.ps_player_row_orange");
        var reds = $(tbody).find("tr.ps_player_row_red");                        

        $('#ps_menu_status_35cnt').html("<b>"+$('.ps_player_type_sell_3_5').length+"</b>"+"<br/>");

        $('#ps_menu_status_35cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_3_5').length+"</span");
        $('#ps_menu_status_35cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_3_5').length+"</span");
        $('#ps_menu_status_35cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_3_5').length+"</span");

        $('#ps_menu_status_40cnt').html("<b>"+$('.ps_player_type_sell_4_0').length+"</b>"+"<br/>");
        $('#ps_menu_status_40cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_4_0').length+"</span");
        $('#ps_menu_status_40cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_4_0').length+"</span");
        $('#ps_menu_status_40cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_4_0').length+"</span");

        $('#ps_menu_status_45cnt').html("<b>"+$('.ps_player_type_sell_4_5').length+"</b>"+"<br/>");
        $('#ps_menu_status_45cnt').append("<span style='color:yellowgreen'>"+$('tr.ps_player_row_green .ps_player_type_sell_4_5').length+"</span");
        $('#ps_menu_status_45cnt').append("-<span style='color:#ccc'>"+$('tr.ps_player_row_orange .ps_player_type_sell_4_5').length+"</span");
        $('#ps_menu_status_45cnt').append("-<span style='color:tomato'>"+$('tr.ps_player_row_red .ps_player_type_sell_4_5').length+"</span");

        $('#ps_menu_status_wages').html("<b>"+(resTotals.wages/(1000*1000)).toFixed(2)+"m"+"</b>");
        $('#ps_menu_status_onlist').html("<b>"+resTotals.onList+"</b>");

        $('#ps_menu_status_frnd').html("<b>"+resTotals.fRnd+"</b>");                       
        $('#ps_menu_status_srnd').html("<b>"+resTotals.sRnd+"</b>");


        $(tbody).html("");
        /*greens.sort(function(a, b) {
            return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
        });*/

		greens = $(greens).sort(sortPsRowsCompare);

        $(greens).each(function() {
            if ($(this).hasClass('ps_player_row_sell_5_0'))
                $(tbody).append(this);
        });
        $(greens).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_5'))
                $(tbody).append(this);
        });
        $(greens).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_0'))
                $(tbody).append(this);
        });
        $(greens).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_5'))
                $(tbody).append(this);
        });
        $(greens).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_0'))
                $(tbody).append(this);
        });

        oranges = $(oranges).sort(sortPsRowsCompare);

        $(oranges).each(function() {
            if ($(this).hasClass('ps_player_row_sell_5_0'))
                $(tbody).append(this);
        });
        $(oranges).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_5'))
                $(tbody).append(this);
        });
        $(oranges).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_0'))
                $(tbody).append(this);
        });
        $(oranges).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_5'))
                $(tbody).append(this);
        });
        $(oranges).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_0'))
                $(tbody).append(this);
        });

        reds = $(reds).sort(sortPsRowsCompare);

        $(reds).each(function() {
            if ($(this).hasClass('ps_player_row_sell_5_0'))
                $(tbody).append(this);
        });
        $(reds).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_5'))
                $(tbody).append(this);
        });
        $(reds).each(function() {
            if ($(this).hasClass('ps_player_row_sell_4_0'))
                $(tbody).append(this);
        });
        $(reds).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_5'))
                $(tbody).append(this);
        });
        $(reds).each(function() {
            if ($(this).hasClass('ps_player_row_sell_3_0'))
                $(tbody).append(this);
        });
    }
    
    
    
    jQuery(document).ready(function() {
        
        
        
        window.setTimeout(function() {
            if (location.href.indexOf("/home") != -1 || INFOS['club'].clubId == SESSION['main_id'] ||
                INFOS['club'].clubId == SESSION['b_team'] ) {
            
            var ps_menuContainer = $("<div></div>").css("position","fixed").css("top","0").css("left","0").css("z-index","10");
            
           
            $(ps_menuContainer).css("max-width",ps_menuOpen_width);
            $(ps_menuContainer).css("font-size","13px");
            $(ps_menuContainer).css("line-height","140%");

            var ps_menu = $("<div></div>");
            $(ps_menu).css("width",ps_menuClosed_width).css("height",ps_menuClosed_height);
            $(ps_menu).css("background","rgb(51, 51, 51)");
            $(ps_menu).css("overflow","hidden");  
            
             $(window).resize(function() {
                var w = $(window).width();
                if (w < 1200) {
                    $(ps_menuContainer).css("top","auto");
                    $(ps_menuContainer).css("bottom","0");
                } else {
                    $(ps_menuContainer).css("top","0");
                    $(ps_menuContainer).css("bottom","auto");
                }
            });
                
            if ($(window).width() < 1200) {
                $(ps_menuContainer).css("top","auto");
                $(ps_menuContainer).css("bottom","0");
            }

            var menuStatus = $("<div id='ps_menu_div_status'></div>");
            
            var ps_menuHeader = $('<div style="width:100%;float:left;padding-left:3px;"><small id="ps_menu_div_title" style="margin:15px 10px;display:none;">PlayerSeller 2.1</small></div>');
            $(ps_menuHeader).append(menuStatus);
            $(ps_menuHeader).css("cursor","pointer");  
            $(ps_menuHeader).click(function() {
                if (!ps_menuOpen){        
                    $(ps_menu).css("width","100%").css("min-height","200px");
                    $(ps_menu).css("height","auto"); 
                    $('#ps_menu_div_container'/*,#ps_menu_div_title'*/).show();
                    $(ps_menu).css("padding","10px");  
                    ps_menuOpen = true;
                } else {
                    window.setTimeout(function() {
                        $('#ps_menu_div_container'/*,#ps_menu_div_title'*/).hide();
                        $(ps_menu).css("width",ps_menuClosed_width).css("height",ps_menuClosed_height);   
                        $(ps_menu).css("min-height",ps_menuClosed_height); 
                        //$(ps_menu).css("overflow","hidden");  
                        $(ps_menu).css("padding","0px");  
                        ps_menuOpen = false;
                    },200);
                }
            });
            
            var stTable = $('<table></table>');
            $(menuStatus).css("float","right");
            $(menuStatus).css("padding","3px 10px");
            $(menuStatus).css("width","95%");
            $(stTable).css("width","45%");
            $(stTable).css("line-height","1.1em");
            $(stTable).css("float","right");
            $(stTable).append('<tr align="center"><td style="border-left:1px solid white;border-right:1px solid white;color:gold"><b>3.5</b></td><td style="color:gold;border-right:1px solid white;"><b>4.0</b></td><td style="color:gold"><b>4.5</b></td></tr>');  
            $(stTable).append('<tr align="center"><td style="border-left:1px solid white;border-right:1px solid white;"><span id="ps_menu_status_35cnt">0</span></td><td style="border-right:1px solid white;"><span id="ps_menu_status_40cnt">0</span></td><td><span id="ps_menu_status_45cnt">0</span></td></tr>'); 
            
            
            var stTable2 = $('<table></table>');
            $(stTable2).css("width","50%");
            $(stTable2).css("float","left");
            $(stTable2).css("font-size","1.0em");
            $(stTable2).css("line-height","1.1em");
            $(stTable2).append("<tr><td>wgs: </td><td><span id='ps_menu_status_wages'>0</span></td><td>oTL: </td><td><span id='ps_menu_status_onlist'>0</span></td></tr>");
             $(stTable2).append("<tr><td>1Rnd: </td><td><span id='ps_menu_status_frnd'>0</span></td><td>2Rnd: </td><td><span id='ps_menu_status_srnd'>0</span></td></tr>");           
            //$(stTable2).append("<tr><td>: </td><td><span id=''>0</span></td></tr>");
            
            var shalf = getCurrentSeasonHalf();            
            var now = new Date();
            if (now.getTime() >= shalf.getTime())
                shalf = getCurrentSeasonEnd();
                
            var diffHalf = new Date(shalf.getTime()-now.getTime());
            var diffHalfDays = Math.ceil(diffHalf/(24 * 60 * 60 * 1000));
            shalf.setDate(shalf.getDate()-5);
            $(stTable2).append("<tr><td>rem: </td><td colspan='3'><span id='ps_menu_status_seasonhalf'><b>"+diffHalfDays+"d - "+formatDate(shalf)+"</b></span></td></tr>");
            
            $(menuStatus).append(stTable2);
            $(menuStatus).append(stTable); 
            

            var ps_menuDiv = $("<div style='width:100%; display:none;' id='ps_menu_div_container'></div>");
            $(ps_menu).html(ps_menuDiv); 
            $(ps_menu).append(ps_menuHeader);
            


           
            $(ps_menuContainer).append(ps_menu);

            if (location.href.indexOf("/players/") != -1) {  
                var ps_menuDongle = $("<div id='ps_PlayerSaverMenu_dongle'></div>").css("max-width",ps_menuOpen_width).css("height",ps_menuClosed_height);
                $(ps_menuDongle).css("background",dongleColors.green);  
                $(ps_menuDongle).css("display","none"); 
                $(ps_menuContainer).append(ps_menuDongle);
            } 


            $("body").append(ps_menuContainer);


            //fill data
            var urls = [serviceUrl + "clubs/"+SESSION['main_id']+"/selldata"+"?apikey="+document.getApiKey(),
                        serviceUrl + "clubs/"+SESSION['b_team']+"/selldata"+"?apikey="+document.getApiKey()];

            //console.log(url);
            var table = $('<table id="playerseller_overview_table" cellpadding="0" cellspacing="0"></table>');      
            var thead = $('<thead></thead>');
            $(table).append(thead);
           

            var th = $('<tr align="center"></tr>');
            $(th).append('<th>T</th>');    
            $(th).append('<th>Rec</th>');
            $(th).append('<th>RR</th>');
            $(th).append('<th>Player</th>');
            $(th).append('<th>Age</th>');            
            $(th).append('<th>St.</th>');
            
            $(thead).append(th);
            $(table).append(thead);

            var tbody = $('<tbody></tbody>');
            $(table).append(tbody);

            
            $(urls).each(function(ui, url) {                    
                $.ajax
                ({
                    type: "GET",
                    url: url,
                    dataType: 'json',
                    error: function() {

                    },
                    success: function (res){
                        //console.log(res);      
                        
                        var resCntMax = res.data.length;
                        var resCnt = 0;

                        $(res.data).each(function() {
                            var resO = this;
                            //console.log(resO);
                            
                            //update seller box
                            if (playerId == resO.playerId) {
                                //select saved option
                                $('#ps_player_box_type_select_'+resO.playerId).val(resO.sellData.type);
                                //mark as already saved
                                $('#ps_player_seller_save_button').prepend('<img src="/pics/mini_green_check.png">&nbsp;');
                                
                                var pot = $('#ps_player_box_potential_'+resO.playerId);
                                if (resO.sellData.potential && resO.sellData.potential != null && resO.sellData.potential != "undefined" && parseInt(resO.sellData.potential) > 0) {
                                           
                                    if ($(pot).prop("disabled") == true && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                        $(pot).val(resO.sellData.potential);
                                    }
                                    if ($(pot).prop("disabled") == false && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                        $(pot).next("label").after("<br/><small><i>value changed, push save button</i></small>")
                                    }
                                } else {
                                    if ($(pot).prop("disabled") == false && parseFloat($(pot).val()) != parseFloat(resO.sellData.potential)) {
                                        $(pot).next("label").after("<br/><small><i>value changed, push save button</i></small>")
                                    } else {
                                        $(pot).next("label").after("<br/><small><i>*go to scout tab and reload page<br/> to update avg pot.</i></small>")
                                    }
                                }
                            }
                            
                            if (resO.sellData.type && 
                                (resO.sellData.type == "sell_3_0" || resO.sellData.type == "sell_3_5" || resO.sellData.type == "sell_4_0" || resO.sellData.type == "sell_4_5" || resO.sellData.type == "sell_5_0")
                               ) {                               

                               

                                var tr = $('<tr id="ps_player_row_'+resO.playerId+'" class="ps_player_row_'+resO.sellData.type+'"></tr>');
                                var rec = 0;

                                $(tr).append('<td class="ps_player_type_'+resO.sellData.type+'">'+resO.sellData.type.replace("sell_","")+'</td>'); 
                                $(tr).append('<td><span class="ps_player_rec"></span></td>');
                                if (resO.rec.indexOf("/") != -1) {
                                    var recs = resO.rec.split("/");
                                    if (recs[0] > recs[1]) {
                                        $(tr).append('<td>'+recs[0]+'</td>');
                                    } else {
                                        $(tr).append('<td>'+recs[1]+'</td>');
                                    }
                                } else
                                    $(tr).append('<td>'+resO.rec+'</td>');
                                
                                var plNameStr = (this.pos ? '['+this.pos+'] ' : '' )+this.name;
                                if (plNameStr.length > 24) {
                                    plNameStr = (this.pos ? '['+this.pos+'] ' : '' )+this.name.substring(0,16);
                                }
                                $(tr).append('<td><div><span class="ps_player_name"><a target="_blank" href="/players/'+resO.playerId+'/'+this.name+'/" class="normal" player_link="'+resO.playerId+'">'+plNameStr+' (ti:'+resO.tiGain+')</a></span></td>');
                                $(tr).append('<td><span class="ps_player_age"></span></td>');    
                                
                                
                                
                                //$(tr).append('<td>'+(resO.recDiff/resO.saves).toFixed(2)+'</td>');
                                $(tr).append('<td><span class="ps_player_status"></span></td>');
                                $(tbody).append(tr);        
                                activate_player_links($('#playerseller_overview_table a'));

                                if (!tooltipCache.players[resO.playerId]) {
                                    $.post("/ajax/tooltip.ajax.php",{"player_id":resO.playerId, minigame:""},function(data){
                                        if(data!=null) {
                                            console.log("loaded tooltip for "+resO.playerId+" from server");
                                            data = JSON.parse(data);
                                            var p=data["player"];
                                            //console.log(p);

                                            tooltipCache.players[resO.playerId] = p;
                                            tooltipCache.lastSave = new Date().getTime();
                                            
                                            updatePsRow(resO,p);
                                            
                                            resCnt++;
                                            if (resCnt >= resCntMax) {
                                                console.log("delay update table");                                                
                                                window.setTimeout(function() {
                                                    updatePsTable(tbody);
                                                },300);
                                            }
                                            

                                            window.localStorage.setItem(cacheName,JSON.stringify(tooltipCache));             
                                            if (p.club_id != SESSION['main_id'] && p.club_id != SESSION['b_team']) {
                                                console.log("todo: update players selldata.type to 'sold'");
                                                var url = serviceUrl + "players/"+resO.playerId+"/selldatas/type/updates/sold?apikey="+document.getApiKey();
                                                console.log(url);
                                                $.ajax
                                                ({
                                                    type: "GET",
                                                    url: url,
                                                    dataType: 'json',
                                                    error: function() {

                                                    },
                                                    success: function (res){
                                                        console.log(res);
                                                    },
                                                });           


                                            }
                                        }
                                    });                                        
                                } else {
                                    //console.log("loaded tooltip for "+playerId+" from cache");
                                    var p = tooltipCache.players[resO.playerId];
                                    updatePsRow(resO,p);
                                    
                                    resCnt++;
                                    if (resCnt >= resCntMax) {
                                        console.log("update table");
                                        updatePsTable(tbody);
                                    }
                                }                                  
                            } else if (resO.sellData.type && resO.sellData.type != "sold" && resO.sellData.type != "remove") {
                                //count for none sell players
                                resCnt++;
                                if (resCnt >= resCntMax) {
                                    console.log("update table");
                                    updatePsTable(tbody);
                                }
                            }

                        }); 
                        
                       
                        
                        
                    },
                });
            });
            
            $(ps_menuDiv).append(table);    
            
            console.log("info:");
            //console.log(player_history_data);
            //$.post("/ajax/players_get_info.ajax.php",{"player_id":player_id, "type":"scout","scout_id":null,"show_non_pro_graphs":show_non_pro_graphs},function(data){
            
            //    console.log(data);
            
            //});

            return false;
            } //club only
        },300);
        
    });

}





